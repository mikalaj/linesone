﻿using MaSoft.Framework;
using System;
using System.Threading.Tasks;

namespace MaSoft.Client
{
    class IdentityClientApp : Program
    {
        static IdentityClientApp()
        {
            Current = new IdentityClientApp("MaSoft", "IdentityClient");
        }

        public IdentityClientApp(string company, string application)
            : base(company, application)
        {
        }

        static async Task Main(string[] args)
        {
            await Current.RunAsync(args);
        }

        protected override async Task OnRunAsync(string[] args)
        {
            var server = "https://identity.ma-shadow.tech/";
            if (args.Length > 0)
                server = args[0];

            var client = MaSoft.Framework.Identity.Module.CreateIdentityClient();
            try
            {
                var token = await client.GetTokenAsync(server, Identity);
                Console.WriteLine($"Token: {token.Value}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to get token. {e.Message}");
            }
        }
    }
}
