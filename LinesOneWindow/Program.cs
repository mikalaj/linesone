﻿using System;
using System.Diagnostics;
using System.IO;

namespace MaSoft.Lines.Client
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
            {
                //Process process = null;
                game.IsMobile = false;

                game.Window.TextInput += (o, e) =>
                {
                    game.DoChar(e.Key, e.Character);
                };

                game.ShowKeyboard += () =>
                {
                    //string progFiles = @"C:\Program Files\Common Files\Microsoft Shared\ink";
                    //string keyboardPath = Path.Combine(progFiles, "TabTip.exe");
                    //process = Process.Start(keyboardPath);
                    // process = Process.Start(new ProcessStartInfo(((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
                };

                game.HideKeyboard += () =>
                {
                    //process?.CloseMainWindow();
                    //process?.Dispose();
                    //process = null;
                };

                game.MainLoop();
            }
        }
    }
}
