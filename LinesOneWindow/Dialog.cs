﻿using Microsoft.Xna.Framework;
using System.Text;

namespace Lines
{
    class Dialog
    {

        public void invalidate()
        {
            has_sub = false;
        }

        public void on_key(InputKeyEventArgs e)
        {
            if (e.Key != 0)
            {
                text.Append((char)(int)e.Key);
                invalid = true;
            }
            else if (e.Key == Microsoft.Xna.Framework.Input.Keys.Back)
            {
                if (text.Length != 0)
                {
                    text.Length = text.Length - 1;
                    invalid = true;
                }
            }
        }

        public void draw(IRenderLib c, IAssetsLib a, bool changed)
        {
            if (!(invalid || changed))
                return;

            c.draw_quad(x, y, width, height, new Color(127, 200, 100, 50));
            var w = a.get_font().MeasureString(text);

            c.draw_text(x + width / 2 - w.X / 2, y + height / 2 - w.Y, 48, a.get_font(), new Color(255, 0, 127, 0), text.ToString());
            invalid = false;
        }

        public void set_text(string value)
        {
            text = new StringBuilder(value);
        }

        StringBuilder text = new StringBuilder("Hello World");
        bool has_sub = false;
        int x = 220;
        int y = 300;
        int width = 400;
        int height = 300;
        bool invalid = true;
    }
}
