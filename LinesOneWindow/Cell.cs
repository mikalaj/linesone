﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lines
{
    

    class Cell
    {
        public bool draw(int x, int y, IRenderLib c, IAssetsLib a, bool force, TimeSpan dt)
        {
            var cell_size = LinesGame.cell_size;
            var margin = LinesGame.margin;
            var bounce_delay = LinesGame.bounce_delay;            

            for (int i = 0; i < cell_size; ++i)
            {
                c.draw_line(x, y + i, x + cell_size, y + i, new Color(125, 125, 125, 255));
            }

            c.draw_line(x, y, x + cell_size, y, Color.Black);
            c.draw_line(x + cell_size, y, x + cell_size, y + cell_size, Color.Black);
            c.draw_line(x + cell_size, y + cell_size, x, y + cell_size, Color.Black);
            c.draw_line(x, y + cell_size, x, y, Color.Black);

            if (_ball != BallType.no)
            {
                if (selected)
                {
                    _delay -= (int)dt.TotalMilliseconds;
                    if (_delay < 0)
                    {
                        dy += dir;
                        if (dy > margin - 2 || dy < -(margin - 2))
                        {
                            dir = -dir;
                        }
                        _delay = bounce_delay;
                    }
                }
                else
                {
                    dy = 0;
                }

                var tile = a.get_ball(_ball);
                c.draw_tile(x + margin, y + margin + dy, tile);
            }
            else if (_preview_ball != BallType.no)
            {
                var tile = a.get_small_ball(_preview_ball);
                c.draw_tile(x + (int)(LinesGame.ball_size / 2 * 0.5f) + 8, y + (int)(LinesGame.ball_size / 2 * 0.5f) + 8, 0.5f, tile);
            }            
            
            return true;
        }

        public void select(bool flag)
        {
            selected = flag;
            _delay = LinesGame.bounce_delay;
        }

        public void set_ball(BallType b)
        {
            _ball = b;
        }

        public BallType ball() { return _ball; }
        public BallType preview_ball() { return _preview_ball; }

        public void set_preview_ball(BallType b)
        {
            _preview_ball = b;
        }

        public Point c;
        int dir = 1;
        int dy = 0;
        public int search_rev =0 ;
        public int search_step = 0 ;
        // bool need_draw =true ;
        bool selected =false ;
        BallType _ball =  BallType.no;
        BallType _preview_ball = BallType.no;
        int _delay = LinesGame.bounce_delay;
    }
}