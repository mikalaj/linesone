﻿using Microsoft.Xna.Framework.Graphics;
using System;

namespace Lines
{
    class LinesGame
    {
        public const int field_width = 9;
        public const int field_height = 9;
        public const int ball_size = 64;
        public const int margin = 8;
        public const int cell_size = 2 * margin + ball_size;
        public const int min_clean = 5;
        public const int bounce_delay = 10;
    }

}
