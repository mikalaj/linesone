﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lines
{
    class Text
    {
        public bool draw(IRenderLib c, IAssetsLib a)
        {
            bool r = _need_draw;
            if (_need_draw)
            {
                var size = a.get_font().MeasureString(_text);

                if (_rect.Width < size.X)
                {
                    _rect.Width = (int)size.X;
                }

                if (_rect.Height < size.Y)
                {
                    _rect.Height = (int)size.Y;
                }

                //c.draw_rect(_rect.X, _rect.Y, _rect.Width, _rect.Height, _color);
                c.draw_text(_rect.X + size.X + _rect.Width / 2 - size.X / 2, _rect.Y + size.Y + _rect.Height / 2 - size.Y / 2, _font_size, a.get_font(), _color, _text);
                _need_draw = false;
            }
            return r;
        }

        public bool is_point_inside(int x, int y)
        {
            return x > _rect.X && x < _rect.X + _rect.Width && y > _rect.Y && y < _rect.Y + _rect.Height;
        }

        public void set_position(int x, int y)
        {
            _rect = new Rectangle(x, y, _rect.Width, _rect.Height);
            _need_draw = true;
        }

        public void set_rect(int x, int y, int width, int height)
        {
            _rect = new Rectangle(x, y, width, height);
            _need_draw = true;
        }

        public void set_font_size(int size)
        {
            _font_size = size;
            _need_draw = true;
        }

        public void set_text_color(Color c)
        {
            _color = c;
            _need_draw = true;
        }

        public void set_text(string value)
        {
            _text = value;
            _need_draw = true;
        }

        public void invalidate()
        {
            _need_draw = true;
        }

        int _font_size = 48;
        Color _color = Color.Black;
        Rectangle _rect;
        string _text;
        bool _need_draw = true;
    }
}