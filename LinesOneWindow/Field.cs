﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lines
{
    class Field
    {
        enum state_t
        {
            idle,
            ball_selected,
            ball_move,
            clean,
            spawn,
            end,
            enter_name,
            play_again,
            wait_yes_no,
        };

        int score = 0;

        public Field()
        {
            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    _field[y * LinesGame.field_width + x] = new Cell();
                }
            }

            for (int i = 0; i < next_balls.Length; ++i)
            {
                next_balls[i] = new BallCell();
            }

            LoadRecord();
            restart();
        }

        void SaveRecord()
        {
            var path = GetRecordFile();
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var file = Path.Combine(path, "record.dat");

            File.WriteAllText(file, score.ToString());
        }

        public string GetRecordFile()
        {
            return "lines";
        }

        void LoadRecord()
        {
            var path = GetRecordFile();

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var file = Path.Combine(path, "record.dat");

            if (File.Exists(file))
            {
                var text = File.ReadAllText(file);
                best_score = int.Parse(text);
            }
        }

        BallType next_ball()
        {
            return (BallType)_rnd.Next(Enum.GetValues(typeof(BallType)).Length - 1);
        }

        int free_cells = 0;

        Point GetNextPoint()
        {
            int x = _rnd.Next() % LinesGame.field_width;
            int y = _rnd.Next() % LinesGame.field_height;

            return new Point { X = x, Y = y };
        }

        Cell find_random_free_cell()
        {
            int count = 100;
            int spawned = 0;

            for (int i = 0; i < count; ++i)
            {
                var p = GetNextPoint();
                int x = p.X;
                int y = p.Y;
                if (at(x, y).ball() != BallType.no || at(x, y).preview_ball() != BallType.no)
                {
                    continue;
                }

                return at(x, y);
            }

            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    if (at(x, y).ball() == BallType.no && at(x, y).preview_ball() == BallType.no)
                    {
                        return at(x, y);
                    }
                }
            }

            throw new System.Exception("Should not try to find cell if no free cells");
        }

        void spawn()
        {
            int count = 100;
            int spawned = 0;

            for (int i = 0; i < 3 && free_cells != 0; ++i)
            {

                if (next_balls[i].Key == BallType.no)
                    continue;

                target_cell = at(next_balls[i].Value.X, next_balls[i].Value.Y);
                if (target_cell.ball() == BallType.no)
                {
                    target_cell.set_ball(next_balls[i].Key);
                    target_cell.set_preview_ball(BallType.no);
                }
                else
                {
                    target_cell.set_preview_ball(BallType.no);
                    target_cell = find_random_free_cell();
                    target_cell.set_ball(next_balls[i].Key);
                    target_cell.set_preview_ball(BallType.no);
                }

                free_cells--;

                next_balls[i].Key = BallType.no;
            }

            for (int i = 0; i < 3; ++i)
            {
                var c = at(next_balls[i].Value);
                if (c.ball() == BallType.no)
                    continue;
                clean_field(c);
            }

            int next_free_cells = free_cells;
            for (int i = 0; i < 3 && next_free_cells != 0; ++i)
            {
                var c = find_random_free_cell();
                c.set_preview_ball(next_balls[i].Key = next_ball());
                next_balls[i].Value = c.c;
                --next_free_cells;
            }
        }

        void draw_next(IRenderLib c, IAssetsLib a)
        {
            const int cs = LinesGame.cell_size;
            int w = cs * 3;
            int off_x = c.Width / 2 - w / 2;
            int off_y = 10;


            for (int i = 0; i < 3; ++i)
            {
                c.draw_rect(off_x + i * cs, off_y, cs, cs, new Color(255, 0, 0, 0));
                if (next_balls[i].Key != BallType.no)
                {
                    int b_off_x = off_x + i * cs + cs / 2 - 8;
                    int b_off_y = off_y + cs / 2 - 8;
                    c.draw_tile(b_off_x, b_off_y, 0.5f, a.get_small_ball(next_balls[i].Key));
                }
            }
        }

        bool draw_field(IRenderLib c, IAssetsLib a, TimeSpan dt)
        {
            var field_width = LinesGame.field_width;
            var field_height = LinesGame.field_height;
            var cell_size = LinesGame.cell_size;


            c.draw_quad(0, 0, x_off, c.Height, new Color(200, 200, 200, 255));
            c.draw_quad(x_off, 0, field_width * cell_size, y_off, new Color(200, 200, 200, 255));
            c.draw_quad(x_off + field_width * cell_size, 0, c.Width - x_off + field_width * cell_size, c.Height, new Color(200, 200, 200, 255));
            c.draw_quad(0, y_off + field_height * cell_size, c.Width, c.Height - y_off - field_height * cell_size, new Color(200, 200, 200, 255));

            for (int y = 0; y < field_height; ++y)
            {
                for (int x = 0; x < field_width; ++x)
                {
                    _field[y * field_width + x].draw(x_off + x * cell_size, y_off + y * cell_size, c, a, true, dt);
                }
            }

            draw_next(c, a);

            DrawChampionScore(c, a);
            DrawChallengerScore(c, a);

            return true;
        }

        void DrawChampionScore(IRenderLib c, IAssetsLib a)
        {
            const int cs = LinesGame.cell_size;
            int w = cs * 3;
            int right = c.Width / 2 - w / 2;
            var text_score = best_score.ToString();
            string text_label = "Champion";

            int bottom = 10 + LinesGame.cell_size;
            c.draw_quad(0, 15, right, 90, new Color(200, 200, 200, 240));
            c.draw_text_hcenter(0, 15, right, 48, a.get_font(), new Color(0, 127, 0, 255), text_score);
            c.draw_text_hcenter(0, 65, right, 24, a.get_font(), new Color(0, 127, 0, 255), "Champion");
        }

        void DrawChallengerScore(IRenderLib c, IAssetsLib a)
        {
            const int cs = LinesGame.cell_size;
            int w = cs * 3;
            int left = c.Width / 2 + w / 2;
            int width = c.Width - left;
            var text_score = score.ToString();

            int bottom = 10 + LinesGame.cell_size;
            c.draw_quad(left + 1, 15, width, 90, new Color(200, 200, 200, 240));
            c.draw_text_hcenter(left, 15, width, 48, a.get_font(), new Color(0, 127, 0, 255), text_score);
            c.draw_text_hcenter(left, 65, width, 24, a.get_font(), new Color(0, 127, 0, 255), "Challenger");
        }

        public bool draw(IRenderLib c, IAssetsLib a, TimeSpan dt)
        {
            bool flag = false;
            switch (state)
            {
                case state_t.ball_move:
                    {
                        var b = cur_cell.ball();
                        cur_cell.set_ball(target_cell.ball());
                        target_cell.set_ball(b);
                        cur_cell.select(false);
                        state = state_t.clean;
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a);
                        //flag |= _exit.draw(c, a);
                    }
                    break;

                case state_t.clean:
                    {
                        if (clean_field(target_cell))
                        {
                            state = state_t.idle;
                            //_restart.invalidate();
                            //_exit.invalidate();
                        }
                        else
                            state = state_t.spawn;
                        target_cell = null;
                        cur_cell = null;
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a);
                        //flag |= _exit.draw(c, a);
                    }
                    break;
                case state_t.spawn:
                    {
                        spawn();
                        if (free_cells == 0)
                            state = state_t.end;
                        else
                            state = state_t.idle;
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a);
                        //flag |= _exit.draw(c, a);
                    }
                    break;
                case state_t.idle:
                    {
                        flag |= draw_field(c, a, dt);
                        // flag |= _restart.draw(c, a);
                        // flag |= _exit.draw(c, a);
                    }
                    break;
                case state_t.end:
                    {
                        if (score > best_score)
                        {
                            SaveRecord();
                            best_score = score;
                        }
                        state = state_t.play_again;
                    }
                    break;
                case state_t.play_again:
                    {
                        DrawGameOver(c, a);
                        DrawPlayAgain(c, a);

                        //_yes_button.set_position(window_width / 3, window_height / 2);
                        //_yes_button.set_text("Yes");

                        //_no_button.set_position(window_width / 3 * 2, window_height / 2);
                        //_no_button.set_text("No");

                        //_yes_button.draw(c, a);
                        //_no_button.draw(c, a);
                        state = state_t.wait_yes_no;
                        flag |= true;
                    }
                    break;
                case state_t.wait_yes_no:
                    //flag |= _yes_button.draw(c, a);
                    //flag |= _no_button.draw(c, a);
                    break;
                default:
                    flag |= draw_field(c, a, dt);
                    //flag |= _restart.draw(c, a);
                    //flag |= _exit.draw(c, a);
                    break;
            }

            return flag;
        }

        void DrawGameOver(IRenderLib c, IAssetsLib a)
        {
            var size = a.get_font().MeasureString("Game Over");

            c.draw_quad(0, 0, window_width, window_height, new Color(240, 50, 50, 50));
            c.draw_text(window_width / 2 - size.X / 2, window_height / 2 - size.Y - 200, 48, a.get_font(), new Color(255, 255, 0, 0), "Game Over");
        }

        void DrawPlayAgain(IRenderLib c, IAssetsLib a)
        {
            var size = a.get_font().MeasureString("Play Again?");
            c.draw_text(window_width / 2 - size.X / 2, window_height / 2 - size.Y - 100, 48, a.get_font(), new Color(255, 255, 0, 0), "Play Again?");
        }

        void restart()
        {
            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    _field[y * LinesGame.field_width + x].c = new Point(x, y);
                    _field[y * LinesGame.field_width + x].set_ball(BallType.no);
                    _field[y * LinesGame.field_width + x].set_preview_ball(BallType.no);
                }
            }

            for (int i = 0; i < 3; ++i)
            {
                next_balls[i].Key = next_ball();
                next_balls[i].Value = GetNextPoint();
            }

            free_cells = LinesGame.field_width * LinesGame.field_height;
            spawn();
            state = state_t.idle;
            score = 0;
        }

        void exit_game()
        {
            _done = true;
        }

        int count = 0;

        public void on_click(MouseState e)
        {

            // console::get().set_position(0, 5);
            // out_cout(count << ": " << e.to_string() << "            ");

            count++;

            //if (_restart.is_point_inside(e.X, e.Y))
            //{
            //    restart();
            //    return;
            //}

            //if (_exit.is_point_inside(e.X, e.Y))
            //{
            //    exit_game();
            //    return;
            //}


            if (state == state_t.wait_yes_no)
            {
                //if (_yes_button.is_point_inside(e.X, e.Y))
                //{
                //    restart();
                //}

                //if (_no_button.is_point_inside(e.X, e.Y))
                //{
                //    exit_game();
                //}
            }
            else
            {
                int x = e.X - x_off;
                int y = e.Y - y_off;

                int cx = x / LinesGame.cell_size;
                int cy = y / LinesGame.cell_size;

                if (cx >= 0 && cx < LinesGame.field_width && cy >= 0 && cy < LinesGame.field_height)
                {
                    // out_cout("Cell[" << cx << "][" << cy << "] was clicked" << "                     ");

                    switch (state)
                    {
                        case state_t.idle:
                            {
                                var cell = at(cx, cy);
                                if (cell.ball() != BallType.no)
                                {
                                    cur_cell = cell;
                                    cur_cell.select(true);
                                    state = state_t.ball_selected;
                                }
                            }
                            break;
                        case state_t.ball_selected:
                            {
                                var cell = at(cx, cy);
                                if (cell.ball() == BallType.no)
                                {
                                    target_cell = cell;
                                    if (find_path())
                                    {
                                        state = state_t.ball_move;
                                    }
                                    else
                                    {
                                        target_cell = null;
                                    }
                                }
                                else
                                {
                                    cur_cell.select(false);
                                    cur_cell = cell;
                                    cur_cell.select(true);
                                }
                            }
                            break;

                    }
                }
            }
        }

        public void on_resize(int width, int height)
        {
            window_width = width;
            window_height = height;
            var fw = LinesGame.field_height * LinesGame.cell_size;
            var fh = LinesGame.field_height * LinesGame.cell_size;
            x_off = (window_width - fw) / 2;
            y_off = 120;
            //_restart.set_rect(0, window_height - 64, window_width / 2, 64);
            //_restart.set_text("Restart");
            //_exit.set_rect(window_width / 2, window_height - 64, window_width / 2, 64);
            //_exit.set_text("Exit");
        }


        public void on_mouse(int X, int Y)
        {
            switch (state)
            {
                case state_t.ball_selected:
                case state_t.idle:
                    {
                        //if (_restart.is_point_inside(X, Y))
                        //{
                        //    _restart.set_text_color(new Color(255, 255, 255, 255));
                        //}
                        //else
                        //{
                        //    _restart.set_text_color(new Color(255, 0, 0, 0));
                        //}

                        //if (_exit.is_point_inside(X, Y))
                        //{
                        //    _exit.set_text_color(new Color(255, 255, 255, 255));
                        //}
                        //else
                        //{
                        //    _exit.set_text_color(new Color(255, 0, 0, 0));
                        //}
                    }
                    break;
                case state_t.wait_yes_no:
                    {
                        //if (_yes_button.is_point_inside(X, Y))
                        //{
                        //    _yes_button.set_text_color(new Color(255, 255, 255, 255));
                        //}
                        //else
                        //{
                        //    _yes_button.set_text_color(new Color(255, 0, 0, 0));
                        //}

                        //if (_no_button.is_point_inside(X, Y))
                        //{
                        //    _no_button.set_text_color(new Color(255, 255, 255, 255));
                        //}
                        //else
                        //{
                        //    _no_button.set_text_color(new Color(255, 0, 0, 0));
                        //}
                    }
                    break;
                default:
                    break;

            }
        }

        bool find_path()
        {
            ++search;

            Queue<Cell> s = new Queue<Cell>();
            cur_cell.search_rev = search;
            cur_cell.search_step = 0;
            s.Enqueue(cur_cell);

            while (s.Count != 0)
            {
                var cell = s.Peek();

                if (cell == target_cell)
                {
                    // out_cout("Path found with " << cell.search_step << " steps");
                    return true;
                }

                s.Dequeue();

                Cell[] b = new Cell[4];

                if (cell.c.X - 1 >= 0)
                    b[0] = at(cell.c.X - 1, cell.c.Y);
                else
                    b[0] = null;

                if (cell.c.X + 1 < LinesGame.field_width)
                    b[1] = at(cell.c.X + 1, cell.c.Y);
                else
                    b[1] = null;

                if (cell.c.Y - 1 >= 0)
                    b[2] = at(cell.c.X, cell.c.Y - 1);
                else
                    b[2] = null;

                if (cell.c.Y + 1 < LinesGame.field_height)
                    b[3] = at(cell.c.X, cell.c.Y + 1);
                else
                    b[3] = null;

                foreach (var n in b)
                {
                    if (n == null)
                        continue;

                    if (n.ball() == BallType.no && (n.search_rev != search || n.search_step > cell.search_step + 1))
                    {
                        n.search_rev = search;
                        n.search_step = cell.search_step + 1;
                        s.Enqueue(n);
                    }
                }
            }
            // out_cout("Path was not found");
            return false;
        }

        bool clean_field(Cell target)
        {
            // assert(target != null);
            // assert(target.ball() != ball_type_t.no);
            clean_id++;
            int count = 0;
            int bonus = -5;

            var b = target.ball();
            int cx = target.c.X;
            int cy = target.c.Y;
            bool result = false;

            int x1 = cx;
            while (true)
            {
                if (x1 > 0 && at(x1 - 1, cy).ball() == b)
                {
                    --x1;
                }
                else
                {
                    break;
                }
            }

            int x2 = cx;
            while (true)
            {
                if (x2 < LinesGame.field_width - 1 && at(x2 + 1, cy).ball() == b)
                {
                    ++x2;
                }
                else
                {
                    break;
                }
            }

            int y1 = cy;
            while (true)
            {
                if (y1 > 0 && at(cx, y1 - 1).ball() == b)
                {
                    --y1;
                }
                else
                {
                    break;
                }
            }

            int y2 = cy;
            while (true)
            {
                if (y2 < LinesGame.field_height - 1 && at(cx, y2 + 1).ball() == b)
                {
                    ++y2;
                }
                else
                {
                    break;
                }
            }

            int dx11 = cx;
            int dy11 = cy;
            while (true)
            {
                if (dx11 < LinesGame.field_width - 1 && dy11 < LinesGame.field_height - 1 && at(dx11 + 1, dy11 + 1).ball() == b)
                {
                    ++dx11;
                    ++dy11;
                }
                else
                {
                    break;
                }
            }

            int dx12 = cx;
            int dy12 = cy;
            while (true)
            {
                if (dx12 > 0 && dy12 > 0 && at(dx12 - 1, dy12 - 1).ball() == b)
                {
                    --dx12;
                    --dy12;
                }
                else
                {
                    break;
                }
            }

            int dx21 = cx;
            int dy21 = cy;
            while (true)
            {
                if (dx21 > 0 && dy21 < LinesGame.field_height - 1 && at(dx21 - 1, dy21 + 1).ball() == b)
                {
                    --dx21;
                    ++dy21;
                }
                else
                {
                    break;
                }
            }

            int dx22 = cx;
            int dy22 = cy;
            while (true)
            {
                if (dx22 < LinesGame.field_width - 1 && dy22 > 0 && at(dx22 + 1, dy22 - 1).ball() == b)
                {
                    ++dx22;
                    --dy22;
                }
                else
                {
                    break;
                }
            }

            if (x2 - x1 >= LinesGame.min_clean - 1)
            {
                while (x1 <= x2)
                {
                    at(x1, cy).set_ball(BallType.no);
                    ++x1;
                    free_cells++;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (y2 - y1 >= LinesGame.min_clean - 1)
            {
                while (y1 <= y2)
                {
                    at(cx, y1).set_ball(BallType.no);
                    ++y1;
                    free_cells++;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (dx11 - dx12 >= LinesGame.min_clean - 1)
            {
                while (dx12 <= dx11)
                {
                    at(dx12, dy12).set_ball(BallType.no);
                    free_cells++;
                    ++dx12;
                    ++dy12;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (dx22 - dx21 >= LinesGame.min_clean - 1)
            {
                while (dx21 <= dx22)
                {
                    at(dx21, dy21).set_ball(BallType.no);
                    free_cells++;
                    ++dx21;
                    --dy21;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            score += count;
            return result;
        }

        Cell at(Point p)
        {
            return _field[p.Y * LinesGame.field_width + p.X];
        }

        Cell at(int x, int y)
        {
            return _field[y * LinesGame.field_width + x];
        }

        bool done()
        {
            return _done;

        }

        class BallCell
        {
            public BallType Key;
            public Point Value;
        }

        int window_width = 0;
        int window_height = 0;
        int x_off = 0;
        int y_off = 0;
        Cell target_cell;
        Cell cur_cell;
        List<Point> path;
        int search = 0;
        int clean_id = 0;

        BallCell[] next_balls = new BallCell[3];

        state_t state = state_t.idle;

        Cell[] _field = new Cell[LinesGame.field_width * LinesGame.field_height];

        // bool need_draw = true;
        // bool need_draw_next = true;
        // bool need_draw_score = true;
        int best_score = 0;
        string best_player = "Unknown";
        //Text _yes_button;
        //Text _no_button;
        //Text _exit;
        //Text _restart;
        bool _done = false;
        Random _rnd = new Random();
    }
}
