﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Lines
{
    public delegate void OnResize(int width, int height);
    public delegate void OnMouseLeftButtonDown(MouseState state);
    public delegate void OnMouseLeftButtonUp(MouseState state);
    public delegate void OnMouseLeftClick(MouseState state);
    public delegate void OnMouseMove(MouseState state, int dx, int dy);

    interface IRenderLib
    {
        int Width { get; }
        int Height { get; }

        void draw_line(int x1, int y1, int x2, int y2, Color p2);
        void draw_tile(int x, int y, Texture2D tile);
        void draw_tile(int x, int y, float scale, Texture2D tile);
        void draw_text(float v1, float v2, int font_size, SpriteFont spriteFont, Color color, string text);
        void draw_quad(int x, int y, int width, int height, Color color);
        void draw_rect(int v, int off_y, int cs1, int cs2, Color color);
        void draw_text_hcenter(int v1, int v2, int right, int v3, SpriteFont spriteFont, Color color, string text_score);
        
        event OnResize OnResize;
        event OnMouseLeftButtonDown OnMouseLeftButtonDown;
        event OnMouseMove OnMouseMove;
        event OnMouseLeftButtonUp OnMouseLeftButtonUp;
        event OnMouseLeftClick OnMouseLeftClick;

        void Resize(int width, int height);
    }
}