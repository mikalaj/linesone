﻿using Microsoft.Xna.Framework.Graphics;

namespace Lines
{
    interface IAssetsLib
    {
        Texture2D get_ball(BallType ball);
        Texture2D get_small_ball(BallType preview_ball);
        SpriteFont get_font();
    }
}