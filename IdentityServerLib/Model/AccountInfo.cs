﻿using System.Collections.Generic;

namespace MaSoft.Framework.Identity
{
    public class AccountInfo
    {
        public List<string> Roles { get; set; } = new List<string>();

        public static AccountInfo Default = new AccountInfo { Roles = new List<string> { WellKnownRoles.Guest } };
    }
}
