﻿namespace MaSoft.Framework.Identity
{
    public static class WellKnownRoles
    {
        public const string Guest = "guest";
        public const string User = "user";
        public const string Admin = "admin";
    }
}
