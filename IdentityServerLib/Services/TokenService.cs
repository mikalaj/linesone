﻿#if !NETSTANDARD
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.IdentityModel.Tokens;
using MaSoft;
using MaSoft.Framework;
using Protocol;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using MaSoft.Framework.Certificates;
using MaSoft.Framework.Grpc;
using NLog;
using MaSoft.Framework.Crypto;

namespace MaSoft.Framework.Identity
{      
    class TokenService : Protocol.TokenService.TokenServiceBase
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public IIdentityServerInfo Service { get; set; }
        public IAccountProvider AccountProvider { get; set; }

        public TokenService(IIdentityServerInfo service, IAccountProvider account)
        {
            Service = service;
            AccountProvider = account;
        }

        public override Task<ProtoConnectResponse> Connect(Empty request, ServerCallContext context)
        {
            return Task.Run(() =>
            {
                var value = Service.Certificate.ExportWithoutPrivateKeyToPEM();
                return new ProtoConnectResponse
                {
                    Certificate = value
                };
            });
        }

        public override Task<ProtoGetTokenResponse> GetToken(ProtoGetTokenRequest request, ServerCallContext context)
        {
            return Task.Run(() =>
            {

                if (!(Service.Certificate.PrivateKey is RSA rsa))
                    throw new Exception("Expected RSA key exchange");

                var clientKey = rsa.Decrypt(request.EncryptedKey.EncryptedKey.ToArray(), RSAEncryptionPadding.Pkcs1);
                var clientIV = rsa.Decrypt(request.EncryptedKey.EncryptedIV.ToArray(), RSAEncryptionPadding.Pkcs1);

                var clientPublicKey = RSA.Create();

                if (request.PublicKeyCase == ProtoGetTokenRequest.PublicKeyOneofCase.EncryptedRsaClientPublicKey)
                {
                    var clientPublicKeyBlob = Encryption.DecryptBytesAes(request.EncryptedRsaClientPublicKey.ToArray(), clientKey, clientIV);
                    clientPublicKey.ImportRSAPublicKey(clientPublicKeyBlob, out int _);
                } else if (request.PublicKeyCase == ProtoGetTokenRequest.PublicKeyOneofCase.EncryptedRsaPublicParameters)
                {
                    var serializedParameters = Encryption.DecryptBytesAes(request.EncryptedRsaPublicParameters.ToArray(), clientKey, clientIV);
                    var parameters = (RsaKeyParameters)System.Text.Json.JsonSerializer.Deserialize(serializedParameters, typeof(RsaKeyParameters));
                    clientPublicKey.ImportParameters(parameters.ToRsaParameters());
                }

                using (Aes myAes = Aes.Create())
                {
                    var newkey = myAes.Key;
                    var newIV = myAes.IV;


                    var identity = AccountProvider.GetIdentity(clientPublicKey.ExportRSAPublicKey());

                    Log.Info($"Server certificate {Service.Certificate.ExportWithoutPrivateKeyToPEM()}");

                    var now = DateTime.UtcNow;
                    var jwt = new JwtSecurityToken(
                            issuer: Service.Issuer,
                            audience: Service.Audience,
                            notBefore: now,
                            claims: identity.Claims,
                            expires: now.Add(Service.LifeTime),
                            signingCredentials: new SigningCredentials(new X509SecurityKey(Service.Certificate), SecurityAlgorithms.RsaSha512Signature));

                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);


                    var message = System.Text.Json.JsonSerializer.Serialize(new Token { Value = encodedJwt });

                    var encryptedToken = Encryption.EncryptStringAes(message, newkey, myAes.IV);
                    var encryptedKey = clientPublicKey.Encrypt(newkey, RSAEncryptionPadding.Pkcs1);
                    var encryptedIV = clientPublicKey.Encrypt(newIV, RSAEncryptionPadding.Pkcs1);

                    return new ProtoGetTokenResponse
                    {
                        EncryptedKey = new ProtoAesKey
                        {
                            EncryptedKey = ByteString.CopyFrom(encryptedKey),
                            EncryptedIV = ByteString.CopyFrom(encryptedIV)
                        },
                        EncryptedToken = ByteString.CopyFrom(encryptedToken)
                    };
                }
            });
        }
    }
}
#endif