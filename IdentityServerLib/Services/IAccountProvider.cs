﻿using System.Security.Claims;

namespace MaSoft.Framework.Identity
{
    public interface IAccountProvider
    {
        ClaimsIdentity GetIdentity(byte[] publicKey);
    }
}
