﻿using NLog;
using NLog.Fluent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace MaSoft.Framework.Identity
{
    class FileAccountProvider : IAccountProvider
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();
        public string Location { get; set; }

        public HashAlgorithm Hasher { get; set; } = HashAlgorithm.Create(HashAlgorithmName.SHA512.Name);

        public FileAccountProvider(string location)
        {
            Location = Path.Combine(location, "accounts");
            if (!Directory.Exists(Location))
            {
                Directory.CreateDirectory(Location);
            }
        }

        public ClaimsIdentity GetIdentity(byte[] publicKey)
        {
            var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, BitConverter.ToString(publicKey)),
                    };

            foreach (var role in GetAccountInfo(publicKey).Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role, ClaimValueTypes.String));
            }

            //claims.Add(new Claim(ClaimTypes.Role, Roles.Admin, ClaimValueTypes.String));

            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                       ClaimsIdentity.DefaultRoleClaimType);
        }

        private string GetAccountStorageLocation(byte[] publicKey)
        {
            var filename = BitConverter.ToString(Hasher.ComputeHash(publicKey));
            return Path.Combine(Location, filename + ".json");
        }


        private AccountInfo GetAccountInfo(byte[] publicKey)
        {
            var path = GetAccountStorageLocation(publicKey);
            if (File.Exists(path))
            {
                return System.Text.Json.JsonSerializer.Deserialize<AccountInfo>(File.ReadAllText(path));
            }

            Log.Info($"Account not found {path}. Create guest.");

            File.WriteAllText(path, System.Text.Json.JsonSerializer.Serialize(AccountInfo.Default));

            return AccountInfo.Default;
        }
    }
}
