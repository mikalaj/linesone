﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace MaSoft.Framework.Identity
{
    public interface IIdentityServerInfo
    {
        X509Certificate2 Certificate { get; set; }
        string Issuer { get; set; }
        string Audience { get; set; }
        TimeSpan LifeTime { get; set; }
    }
}
