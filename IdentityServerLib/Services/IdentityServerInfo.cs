﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace MaSoft.Framework.Identity
{
    internal class IdentityServerInfo : IIdentityServerInfo
    {
        public X509Certificate2 Certificate { get; set; }

        public string Issuer { get; set; } = "MyAuthServer";

        public string Audience { get; set; } = "MyAuthClient";

        public TimeSpan LifeTime { get; set; } = TimeSpan.FromSeconds(24 * 60 * 60);
    }    
}
