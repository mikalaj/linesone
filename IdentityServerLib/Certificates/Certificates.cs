﻿using PeNet.Asn1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;

namespace Common.Certificates
{
    class CertificateAuthoritySettings
    {
        public byte[] NextSerialNumber { get; set; }
        public byte[] Certificate { get; set; }
        public byte[] PublicKey { get; set; }
        public byte[] PrivateKey { get; set; }
    }

    internal class CertificateAuthority    
    {
        private static string id_kp_clientAuth = "1.3.6.1.5.5.7.3.2";
        private static string id_kp_serverAuth = "1.3.6.1.5.5.7.3.1";

        private string SettingsPath { get => Path.Combine(Location, "settings.json"); }
        private string Location { get; }
        private string LocationClient { get => Path.Combine(Location, id_kp_clientAuth); }
        private string LocationServer { get => Path.Combine(Location, id_kp_serverAuth); }
        private X509Certificate2 Certificate { get; set; }
        private ECDsa KeyPair { get; set; }
        private byte[] NextSerial { get; set; }

        private CertificateAuthority(string storageLocation)
        {
            Location = Path.Combine(storageLocation, "ca");

            if (!Directory.Exists(Location))
                Directory.CreateDirectory(Location);
        }

        internal CertificateAuthority(string storageLocation, string name)
            : this(storageLocation)
        {
            Init(name);
        }

        void Init(string name)
        {
            _nextSerialNumber = new BigInteger(new byte[20]);
            _nextSerialNumber = BigInteger.Add(_nextSerialNumber, BigInteger.One);

            CreateCACertificate(name);

            Save();
        }

        public static CertificateAuthority GetOrCreate(string path, string name)
        {
            var auth = new CertificateAuthority(path);
            
            if (!auth.Load() || auth.Certificate.Issuer != name)
            {
                auth.Init(name);
            }

            return auth;
        }

        void Save()
        {
            var settings = new CertificateAuthoritySettings
            {
                NextSerialNumber = _nextSerialNumber.ToByteArray(),
                Certificate = Certificate.RawData,
                PrivateKey = KeyPair.ExportECPrivateKey(),
                PublicKey = KeyPair.ExportSubjectPublicKeyInfo()
            };

            File.WriteAllText(SettingsPath, JsonSerializer.Serialize(settings, new JsonSerializerOptions { WriteIndented = true }));
        }

        bool Load()
        {

            if (!File.Exists(SettingsPath))
                return false;

            try
            {
                var settings = JsonSerializer.Deserialize<CertificateAuthoritySettings>(File.ReadAllText(SettingsPath));
                _nextSerialNumber = new BigInteger(settings.NextSerialNumber);
                Certificate = new X509Certificate2(settings.Certificate);
                KeyPair = ECDsa.Create();
                KeyPair.ImportECPrivateKey(settings.PrivateKey, out int read);
                KeyPair.ImportSubjectPublicKeyInfo(settings.PublicKey, out read);
                return true;
            } 
            catch (Exception e)
            {
                return false;
            }
        }

        void CreateCACertificate(string name)
        {
            KeyPair = ECDsa.Create(); // generate asymmetric key pair            
            var req = new CertificateRequest($"cn={name}", KeyPair, HashAlgorithmName.SHA512);
            req.CertificateExtensions.Add(new X509BasicConstraintsExtension(true, false, 5, true));
            req.CertificateExtensions.Add(new X509KeyUsageExtension(X509KeyUsageFlags.KeyCertSign | X509KeyUsageFlags.CrlSign, true));
            Certificate = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(5));
        }

        public X509Certificate2 GetClientCertificate(string name, bool createIfNoExist)
        {
            var ecdsa = ECDsa.Create(); // generate asymmetric key pair                        
            var req = new CertificateRequest($"cn={name}", ecdsa, HashAlgorithmName.SHA256);
            req.CertificateExtensions.Add(new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, false));
            var myOid = new Oid("1.3.6.1.4.1.1.4.1");
            var v = new OidCollection();
            v.Add(new Oid("1.3.6.1.5.5.7.3.2"));
            v.Add(myOid);
            req.CertificateExtensions.Add(new X509EnhancedKeyUsageExtension(v, false));
            req.CertificateExtensions.Add(new X509Extension(myOid, System.Text.UTF8Encoding.UTF8.GetBytes("This is my data"), false));

            var  bytes = req.CreateSigningRequest();

            var node = Asn1Node.ReadNode(bytes);
            

            var data = new AsnEncodedData(bytes);
            return null;
            //return req.Create(ca, DateTime.Now, DateTime.Now.AddYears(5), CreateSerialNumber());
        }


        private BigInteger _nextSerialNumber;        
    }


    internal class Certificates
    {

    }
}
