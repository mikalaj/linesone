﻿using Grpc.Core;
using Grpc.Net.Client;
using MaSoft.Framework.Grpc;
using MaSoft.Framework.Identity.Clients;
using System;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace MaSoft.Framework.Identity
{    
    public abstract class GrpcClient<T> where T : class
    {        
        protected ChannelBase _channel;
        private T _client;

        private Token _token;
        private string _server = "";
        private RSA _identity;
        private string _identityServer;
        private object _cs = new object();
        private IIdentityClient _tokenService;
        private bool _restart;

        protected GrpcClient()
        {
            _tokenService = Framework.Identity.Module.CreateIdentityClient();
        }

        protected async Task<T> GetClientAsync()
        {
            if (_client == null || _restart)
            {
                _channel?.ShutdownAsync();
                if (_channel is IDisposable d)
                {
                    d.Dispose();
                }

                _token = await _tokenService.GetTokenAsync(_identityServer, _identity);

                await ConnectAsync(_server, _token);

                _restart = false;
            }

            return _client;
        }

        protected void Restart()
        {
            _restart = true;
        }

        public Task SetConnectionInfoAsync(string scoreServer, string identityServer, RSA identity)
        {
            _server = scoreServer;
            _identity = identity;
            _identityServer = identityServer;
            return Task.CompletedTask;
        }

        public async Task<Token> GetTokenAsync(string identityServer, RSA identity)
        {            
            return await _tokenService.GetTokenAsync(identityServer, identity);
        }

        public Task ConnectAsync(string scoreServer, Token token)
        {
            if (_channel is IDisposable d)
                d.Dispose();

            var useGrpcNet = Program.Current.Config.Get("UseGrpcNet", false);

            if (useGrpcNet)
            {
                _channel = CreateAuthenticatedChannel2(scoreServer, token);
            }
            else
            {
                _channel = CreateAuthenticatedChannel(scoreServer, token);
            }

            _client = (T)Activator.CreateInstance(typeof(T), _channel);

            return Task.CompletedTask;
        }
        

        protected static ChannelBase CreateAuthenticatedChannel(string address, Token token)
        {
            var credentials = CallCredentials.FromInterceptor((context, metadata) =>
            {
                if (!string.IsNullOrEmpty(token?.Value))
                {
                    metadata.Add("Authorization", $"Bearer {token.Value}");
                }
                return Task.CompletedTask;
            });

            if (address.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {
                address = address.Substring("https://".Length);
                var channel = new Channel(address, ChannelCredentials.Create(new SslCredentials(), credentials));
                // SslCredentials is used here because this channel is using TLS.
                // CallCredentials can't be used with ChannelCredentials.Insecure on non-TLS channels.
                //var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                //{
                //    HttpClient = CreateHttpClient(),
                //    Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
                //});

                return channel;
            }
            //else
            //{
            //    // AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            //    //var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
            //    //{
            //    //    HttpClient = CreateHttpClient2()
            //    //});

            //    return channel;
            //}

            throw new Exception("Not implemented");
        }

        protected static GrpcChannel CreateAuthenticatedChannel2(string address, Token token)
        {
            var credentials = CallCredentials.FromInterceptor((context, metadata) =>
            {
                if (!string.IsNullOrEmpty(token?.Value))
                {
                    metadata.Add("Authorization", $"Bearer {token.Value}");
                }
                return Task.CompletedTask;
            });

            if (address.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {

                // SslCredentials is used here because this channel is using TLS.
                // CallCredentials can't be used with ChannelCredentials.Insecure on non-TLS channels.
                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient(),
                    Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
                });

                return channel;
            }
            else
            {
                // AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient2()
                });

                return channel;
            }
        }

        private static HttpClient CreateHttpClient()
        {
            //var httpClientHandler = new HttpTwo.Http2MessageHandler();
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.CheckCertificateRevocationList = false;
            httpClientHandler.ClientCertificateOptions = ClientCertificateOption.Manual;
            httpClientHandler.ServerCertificateCustomValidationCallback = ValidateCertificate;
            httpClientHandler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;

            httpClientHandler.UseProxy = true;

            var client = new HttpClient(httpClientHandler)
            {
                
            };
            
            return client;
        }

        private static HttpClient CreateHttpClient2()
        {
            var httpClientHandler = new HttpClientHandler();
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\ca.pem"));
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\1.3.6.1.5.5.7.3.1\localhost.pem"));
            return new HttpClient(httpClientHandler);
        }

        private static bool ValidateCertificate(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3, SslPolicyErrors arg4)
        {            
            return true;
        }

    }
}
