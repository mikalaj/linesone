﻿using MaSoft.Framework.Grpc;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MaSoft.Framework.Identity.Clients
{
    public interface IIdentityClient
    {
        Task<Token> GetTokenAsync(string server, RSA id);
    }
}
