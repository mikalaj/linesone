﻿using Google.Protobuf;
using Grpc.Core;
using Grpc.Net.Client;
using MaSoft.Framework.Certificates;
using MaSoft.Framework.Crypto;
using MaSoft.Framework.Grpc;
using Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MaSoft.Framework.Identity.Clients
{
    class IdentityClient : IIdentityClient
    {
        private HttpClient CreateHttpClient()
        {
            // var httpClientHandler = new HttpTwo.Http2MessageHandler();
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.CheckCertificateRevocationList = false;
            httpClientHandler.ClientCertificateOptions = ClientCertificateOption.Manual;
            httpClientHandler.ServerCertificateCustomValidationCallback = ValidateCertificate;
            httpClientHandler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;
            httpClientHandler.UseProxy = true;
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\ca.pem"));
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\1.3.6.1.5.5.7.3.1\localhost.pem"));
            return new HttpClient(httpClientHandler);
        }

        private bool ValidateCertificate(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3, SslPolicyErrors arg4)
        {            
            return true;
        }

        protected static ChannelBase CreateChannel(string address)
        {           
            if (address.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {
                address = address.Substring("https://".Length);
                var channel = new Channel(address, new SslCredentials());
                return channel;
            }
           
            throw new Exception("Not implemented");
        }

        protected ChannelBase CreateChannel2(string address)
        {
            if (address.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {

                // SslCredentials is used here because this channel is using TLS.
                // CallCredentials can't be used with ChannelCredentials.Insecure on non-TLS channels.
                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient(),
                    Credentials = new SslCredentials()
                });

                return channel;
            }
            else
            {
                // AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient()
                });

                return channel;
            }
        }

        public async Task<Token> GetTokenAsync(string server, RSA id)
        {
            var channel = CreateChannel(server);

            try
            {

                var client = new Protocol.TokenService.TokenServiceClient(channel);

                var serverInfo = await client.ConnectAsync(new Google.Protobuf.WellKnownTypes.Empty());

                var serverCertificate = CertificateExtension.ImportFromPem(serverInfo.Certificate);
                var serverPublicKey = serverCertificate.GetRSAPublicKey();

                var request = new ProtoGetTokenRequest();

                using (var aes = Aes.Create())
                {
                    var key = aes.Key;
                    var IV = aes.IV;

                    request.EncryptedKey = new ProtoAesKey
                    {
                        EncryptedKey = ByteString.CopyFrom(serverPublicKey.Encrypt(key, RSAEncryptionPadding.Pkcs1)),
                        EncryptedIV = ByteString.CopyFrom(serverPublicKey.Encrypt(IV, RSAEncryptionPadding.Pkcs1))
                    };

                    Program.Current.Identity.ExportParameters(true);

                    try
                    {
                        var clientPublicKeyPKCS = id.ExportRSAPublicKey();
                        request.EncryptedRsaClientPublicKey = ByteString.CopyFrom(Encryption.EncryptBytesAes(clientPublicKeyPKCS, key, IV));
                    }
                    catch
                    {                        
                        var parameters = id.ExportParameters(false).ToRsaKeyParameters();
                        var serializedParameters = System.Text.Json.JsonSerializer.Serialize(parameters);
                        request.EncryptedRsaPublicParameters = ByteString.CopyFrom(Encryption.EncryptStringAes(serializedParameters, key, IV));
                    }
                }

                var response = await client.GetTokenAsync(request);

                var newKey = id.Decrypt(response.EncryptedKey.EncryptedKey.ToByteArray(), RSAEncryptionPadding.Pkcs1);
                var newIV = id.Decrypt(response.EncryptedKey.EncryptedIV.ToByteArray(), RSAEncryptionPadding.Pkcs1);

                var tokenJson = Encryption.DecryptStringAes(response.EncryptedToken.ToByteArray(), newKey, newIV);

                return System.Text.Json.JsonSerializer.Deserialize<Token>(tokenJson);
            }
            finally
            {
                if (channel is IDisposable d)
                    d.Dispose();
            }
        }
    }
}
