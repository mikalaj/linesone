﻿using MaSoft.Framework.Identity.Clients;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using System;

namespace MaSoft.Framework.Identity
{
    public static class Module
    {
        public static IIdentityClient CreateIdentityClient()
        {
            return new IdentityClient();
        }

#if !NETSTANDARD
        public static IAccountProvider CreateAccountProvider(AccountProviderType type, string connectionString)
        {
            switch (type)
            {
                case AccountProviderType.FileProvider:
                    return new FileAccountProvider(connectionString);
                default:
                    throw new NotImplementedException($"Account provider of type {type} is not implemented.");
            }
        }

        public static IIdentityServerInfo CreateServerInfo()
        {
            return new IdentityServerInfo();
        }

        public static void RegisterIdentityServer(IEndpointRouteBuilder ep)
        {
            ep.MapGrpcService<TokenService>();
        }
#endif

    }
}