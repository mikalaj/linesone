﻿#if !NET5_0

using MaSoft.Framework;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaSoft.Framework.Graphics
{
    public class ProgramAdapter : Program
    {        

        public ProgramAdapter(string company, string application, bool isService)
            : base(company, application, isService)
        {
            Current = this;
        }

        protected override Task OnRunAsync(string[] args)
        {
            return Task.CompletedTask;
        }
        
    }

    public class MaGame<T> : Game where T : class, IProgram 
    {
        struct Resizing
        {
            public bool Pending;
            public int Width;
            public int Height;
        }        

        protected GraphicsDeviceManager GraphicsManager { get; }
        Resizing resizing = new Resizing();
        public UI.Ui Ui { get; }

        public T Program { get; }

        public MaGame(string company, string app)
            : this(company, app, null)
        {

        }

        public MaGame(string company, string app, UI.Ui ui)
            : this(Activator.CreateInstance(typeof(T), company, app, false) as T)
        {
            GraphicsManager = new GraphicsDeviceManager(this);
            GraphicsManager.SynchronizeWithVerticalRetrace = true;
            Ui = ui;

            Window.ClientSizeChanged += Window_ClientSizeChanged;
            Window.OrientationChanged += Window_OrientationChanged;
        }

        private void Window_OrientationChanged(object sender, EventArgs e)
        {
            resizing.Pending = true; // Resize is pending
            resizing.Width = Window.ClientBounds.Width;
            resizing.Height = Window.ClientBounds.Height;
        }


        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            resizing.Pending = true; // Resize is pending
            resizing.Width = Window.ClientBounds.Width;
            resizing.Height = Window.ClientBounds.Height;
        }

        protected override void Initialize()
        {
            base.Initialize();

            Ui.Init(this);
        }

        public MaGame(IProgram p)
        {
            Program = p as T;
            var task = Program.RunAsync(new string[0]);
            task.Wait();
        }

        MouseState _lastState;
        bool _leftDown = false;
        bool _rightDown = false;

        Keys[] _pressedKeys = new Keys[256];
        Keys[] _prevKeys = new Keys[256];
        int _prevCount;
        bool[] _keyState = new bool[256];

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            if (resizing.Pending)
            {                
                GraphicsManager.PreferredBackBufferWidth = resizing.Width;
                GraphicsManager.PreferredBackBufferHeight = resizing.Height;
                GraphicsManager.ApplyChanges();
                resizing.Pending = false;                
                Resize(resizing.Width, resizing.Height);
                // Render.Resize(Window.ClientBounds.Width, Window.ClientBounds.Height);                
            }

            MouseState state;

            if (TouchPanel.GetCapabilities().IsConnected)
            {
                var tc = TouchPanel.GetState();
                if (tc.Count == 1)
                {
                    if (tc[0].State == TouchLocationState.Pressed)
                    {
                        MouseLeftClick(new MouseState((int)tc[0].Position.X, (int)tc[0].Position.Y, 0, ButtonState.Pressed, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released));
                    }
                }
            }
            else
            {
                state = Mouse.GetState();


                if (state.LeftButton == ButtonState.Pressed && _lastState.LeftButton == ButtonState.Released)
                {
                    _leftDown = true;
                    OnMouseLeftButtonDown(state);
                }

                if (state.RightButton == ButtonState.Pressed && _lastState.RightButton == ButtonState.Released)
                {
                    _rightDown = true;
                    OnMouseRightButtonDown(state);
                }

                if (state.LeftButton == ButtonState.Released && _lastState.LeftButton == ButtonState.Pressed)
                {
                    if (_leftDown)
                    {
                        _leftDown = false;
                        MouseLeftClick(state);
                    }

                    OnMouseLeftButtonUp(state);
                }

                if (state.RightButton == ButtonState.Released && _lastState.RightButton == ButtonState.Pressed)
                {
                    if (_rightDown)
                    {
                        _rightDown = false;
                        MouseRightClick(state);
                    }

                    OnMouseRightButtonUp(state);
                }

                if (state.X != _lastState.X || state.Y != _lastState.Y)
                {
                    if (Math.Abs(state.X - _lastState.X) >= 20 || Math.Abs(state.Y - _lastState.Y) >= 20)
                    {
                        _leftDown = false;
                    }
                    OnMouseMove(state, state.X - _lastState.X, state.Y - _lastState.Y);
                }

                if (state.ScrollWheelValue != _lastState.ScrollWheelValue)
                {
                    OnMouseScroll(state.X, state.Y, -(state.ScrollWheelValue - _lastState.ScrollWheelValue) / 120);
                }

                _lastState = state;
            }

            // TODO: Add your update logic here
            var kstate = Keyboard.GetState();
            var count = kstate.GetPressedKeyCount();
            if (count > _pressedKeys.Length)
            {
                _pressedKeys = new Keys[count];
            }

            kstate.GetPressedKeys(_pressedKeys);

            foreach (var key in _pressedKeys.Take(count))
            {
                if (!_prevKeys.Take(_prevCount).Contains(key))
                {
                    OnKeyDown(key);
                }
            }

            foreach (var prevKey in _prevKeys.Take(_prevCount))
            {
                if (!_pressedKeys.Take(count).Contains(prevKey))
                {
                    OnKeyUp(prevKey);
                }
            }

            {
                var tmp = _pressedKeys;
                _pressedKeys = _prevKeys;
                _prevKeys = tmp;
            }

            {
                _prevCount = count;
            } 

            base.Update(gameTime);

        }

        private void OnMouseScroll(int x, int y, int delta)
        {
            Ui?.UiMouseScroll(x, y, delta);
        }

        protected virtual void OnMouseMove(MouseState state, int dx, int dy)
        {
            Ui?.UiMouseMove(state, dx, dy);
        }

        protected virtual void OnMouseLeftButtonDown(MouseState state)
        {
            Ui?.UiMouseLeftButtonDown(state);
        }

        protected virtual void OnMouseLeftButtonUp(MouseState state)
        {
            Ui?.UiMouseLeftButtonUp(state);
        }

        private void MouseLeftClick(MouseState state)
        {
            if (Ui == null || !Ui.UiMouseLeftClick(state))
                OnMouseLeftClick(state);
        }

        protected virtual void OnMouseLeftClick(MouseState state)
        {
            
        }

        protected virtual void OnMouseRightButtonDown(MouseState state)
        {
            Ui?.UiMouseRightButtonDown(state);
        }

        protected virtual void OnMouseRightButtonUp(MouseState state)
        {
            Ui?.UiMouseRightButtonUp(state);
        }

        private void MouseRightClick(MouseState state)
        {
            if (Ui == null || !Ui.UiMouseRightClick(state))
                OnMouseRightClick(state);
        }

        protected virtual void OnMouseRightClick(MouseState state)
        {

        }

        protected virtual void OnKeyDown(Keys key)
        {
            Ui?.UiKeyDown(key);
        }

        protected virtual void OnKeyUp(Keys prevKey)
        {
            Ui?.UiKeyUp(prevKey);
        }

        private void Resize(int width, int height)
        {
            OnResize(width, height);
        }

        protected virtual void OnResize(int width, int height)
        {
            Ui?.UiResize(width, height);
        }

        public void MainLoop()
        {
            Run();
        }
    }
}

#endif
