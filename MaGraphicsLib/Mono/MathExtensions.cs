﻿#if !NET5_0
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaGraphicsLib.Mono
{
    public static class MathExtensions
    {
        public static Vector3 ToMono(this System.Numerics.Vector3 v)
        {
            return new Vector3 { X = v.X, Y = v.Y, Z = v.Z };
        }

        public static Color ToMono(this System.Drawing.Color c)
        {
            return new Color { R = c.R, G = c.G, B = c.B, A = c.A };
        }

        public static System.Numerics.Vector3 ToNumerics(this Vector3 v)
        {
            return new System.Numerics.Vector3 { X = v.X, Y = v.Y, Z = v.Z };
        }

        public static Quaternion ToMono(this System.Numerics.Quaternion q)
        {
            return new Quaternion { W = q.W, X = q.X, Y = q.Y, Z = q.Z };
        }

        public static System.Numerics.Quaternion ToNumerics(this Quaternion q)
        {
            return new System.Numerics.Quaternion { W = q.W, X = q.X, Y = q.Y, Z = q.Z };
        }

        public static Matrix ToMono(this System.Numerics.Matrix4x4 m)
        {
            return new Matrix
            {
                M11 = m.M11,
                M12 = m.M12,
                M13 = m.M13,
                M14 = m.M14,

                M21 = m.M21,
                M22 = m.M22,
                M23 = m.M23,
                M24 = m.M24,

                M31 = m.M31,
                M32 = m.M32,
                M33 = m.M33,
                M34 = m.M34,

                M41 = m.M41,
                M42 = m.M42,
                M43 = m.M43,
                M44 = m.M44,
            };
        }
    }
}
#endif