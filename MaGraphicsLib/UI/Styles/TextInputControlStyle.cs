﻿#if !NET5_0

using Microsoft.Xna.Framework;

namespace MaSoft.Framework.Graphics.UI
{
    public class TextInputControlStyle : ControlStyle
    {
        public Color TextColor = Color.White;
    }
}

#endif