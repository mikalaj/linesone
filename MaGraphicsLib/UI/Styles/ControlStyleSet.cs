﻿#if !NET5_0


namespace MaSoft.Framework.Graphics.UI
{
    public class ControlStyleSet
    {
        public ButtonControlStyle ButtonStyle { get; set; } = new ButtonControlStyle();
        public IconControlStyle IconStyle { get; set; } = new IconControlStyle();
        public PanelControlStyle PanelStyle { get; set; } = new PanelControlStyle();
        public TextControlStyle TextStyle { get; internal set; } = new TextControlStyle();
        public TextInputControlStyle TextIntputStyle { get; internal set; } = new TextInputControlStyle();
        public ItemsViewStyle ItemsViewStyle { get; internal set; } = new ItemsViewStyle();
    }
}

#endif