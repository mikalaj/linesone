﻿#if !NET5_0


using Microsoft.Xna.Framework;

namespace MaSoft.Framework.Graphics.UI
{
    public class InteractiveControlStyle : ControlStyle
    {
        public Color _noFocus = new Color(200, 200, 200, 255);
        public Color _Focused = new Color(220, 220, 220, 255);
        public Color _Pressed = new Color(170, 170, 170, 255);
    }
}

#endif