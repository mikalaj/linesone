﻿#if !NET5_0


using Microsoft.Xna.Framework;

namespace MaSoft.Framework.Graphics.UI
{
    public class ItemsViewStyle : InteractiveControlStyle
    {
        public Color TextColor = Color.White;
        public Color SelectionFill = Color.DarkBlue;
        public Color SelectionFrame = Color.LightSteelBlue;
    }
}

#endif