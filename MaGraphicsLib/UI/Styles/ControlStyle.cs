﻿#if !NET5_0


using Microsoft.Xna.Framework;

namespace MaSoft.Framework.Graphics.UI
{
    public class ControlStyle
    {
        public Color BackgroundColor = Color.DarkGray;
        public Color FrameColor = Color.Black;
    }
}

#endif