﻿#if !NET5_0

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MaSoft.Framework.Graphics.UI
{

    public class TextControl : Control, IFontRequired
    {
        public TextControl()
            : base(Ui.StyleSet.TextStyle)
        {
        }

        public TextControl(TextControlStyle style)
            : base(style)
        {
        }

        public new TextControlStyle Style { get => base.Style as TextControlStyle; }

        protected override bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            //c.draw_tile(_rect.X, _rect.Y, a.GetRestartIcon());
            //c.draw_rect(_rect.X, _rect.Y, _rect.Width, _rect.Height, _color);
            if (Font != null)
            {
                c.DrawTextLeft(rect.X, rect.Y,  _font_size, Font, _color, _text);
            }
            return false;
        }

        public SpriteFont Font { get; set; }
        

        public void set_font_size(int size)
        {
            _font_size = size;
            Invalidate();
        }

        public void set_text_color(Color c)
        {
            _color = c;
            Invalidate();
        }

        public string Text
        {
            get => _text;
            set
            {
                if (_text == value)
                    return;
                _text = value;
                Invalidate();
            }
        }        

        int _font_size = 48;
        Color _color = new Color(200, 200, 200, 255);
        Rectangle _rect;
        string _text = string.Empty;        
    }
}

#endif