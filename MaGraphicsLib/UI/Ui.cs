﻿#if !NET5_0

using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaSoft.Framework.Graphics.UI
{
    public abstract class Ui
    {
        private List<Control> _controls = new List<Control> { };
        private Control _focused = null;
        private bool _needClear = false;

        public Ui()
        {
            Current = this;
        }

        public IEnumerable<Control> FindChildren(Func<Control,bool> filter)
        {
            foreach(var c in _controls)
            {
                if (filter?.Invoke(c) == true)
                {
                    yield return c;
                }

                foreach(var v in c.FindChildren(filter))
                {
                    if (filter?.Invoke(v) == true)
                    {
                        yield return v;
                    }
                }
            }
        }

        public static Ui Current { get; private set; }

        public static void ApplyStyleSet(ControlStyleSet styleSet)
        {
            StyleSet = styleSet;
            foreach (var control in Current._controls)
            {
                control.ApplyStyleSet(StyleSet);
            }
        }

        public static ControlStyleSet StyleSet { get; private set; } = new ControlStyleSet();

        public static Func<string, string, string, bool, Task<string>> ShowTextInput { get; set; }
        public static bool IsMobile { get; set; }

        public static string SupportedCharacters { get; set; } = string.Empty;

        protected abstract void ShowKeyboard();
        protected abstract void HideKeyboard();

        public void AddControl(Control control)
        {
            _controls.Add(control);
        }

        public void RemoveControl(Control control)
        {
            _controls.Remove(control);
        }

        protected abstract void OnInit(object app);

        public void Init(object app)
        {
            OnInit(app);
        }

        public void Invalidate()
        {
            foreach (var c in _controls)
            {
                c.Invalidate();
            }
        }

        public void UiDraw(IRenderLib render, TimeSpan dt)
        {
            render.Sprite.Begin();
            if (_needClear)
            {                
                _needClear = false;
            }

            if (IsMobile && _focused is TextInput input)
            {
                input.draw(render, dt);
            }
            else
            {
                foreach (var c in _controls)
                {
                    if (!c.Visible)
                        continue;

                    c.draw(render, dt);
                }
            }
            render.Sprite.End();
        }
        public void UiMouseLeftButtonDown(MouseState state)
        {
            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    c.DoMouseLeftButtonDown(state.X, state.Y);
                    return;
                }
            }
        }

        public void UiMouseLeftButtonUp(MouseState state)
        {
            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    c.DoMouseLeftButtonUp(state.X, state.Y);
                    return;
                }
            }
        }

        public bool UiMouseLeftClick(MouseState state)
        {
            bool clicked = false;

            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    clicked = true;

                    c.DoClick(state.X, state.Y);

                    if (_focused != null && _focused != c)
                    {
                        _focused.DoUnfocus();
                        _focused = null;
                        _needClear = true;
                    }

                    if (c.Focusable && !c.IsFocused)
                    {
                        _focused = c;
                        c.DoFocus();

                        if (c is TextInput input)
                        {
                            if (IsMobile)
                            {
                                var task = ShowTextInput(input.Title, input.Description, input.Text, input.IsPassword);
                                task.ContinueWith(a =>
                                {
                                    var text = a.Result;
                                    if (text != null)
                                    {
                                        input.Text = text;                                        
                                    }
                                    input.DoUnfocus();
                                    _focused = null;
                                    _needClear = true;
                                });
                            }
                            else
                            {
                                ShowKeyboard();
                            }
                        }
                        else
                        {
                            HideKeyboard();
                        }
                        _needClear = true;
                    }

                    return clicked;
                }
            }

            if (_focused != null)
            {
                _focused.DoUnfocus();
                _focused = null;
                HideKeyboard();
                _needClear = true;
            }

            return clicked;
        }

        public void UiMouseRightButtonDown(MouseState state)
        {
            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    c.DoMouseRightButtonDown(state.X, state.Y);
                    return;
                }
            }
        }

        public void UiMouseRightButtonUp(MouseState state)
        {
            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    c.DoMouseRightButtonUp(state.X, state.Y);
                    return;
                }
            }
        }

        public bool UiMouseRightClick(MouseState state)
        {
            bool clicked = false;

            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(state.X, state.Y))
                {
                    clicked = true;

                    c.DoRightClick(state.X, state.Y);

                    if (_focused != null && _focused != c)
                    {
                        _focused.DoUnfocus();
                        _focused = null;
                        _needClear = true;
                    }

                    if (c.Focusable && !c.IsFocused)
                    {
                        _focused = c;
                        c.DoFocus();
                        
                        _needClear = true;
                    }

                    return clicked;
                }
            }

            if (_focused != null)
            {
                _focused.DoUnfocus();
                _focused = null;
                HideKeyboard();
                _needClear = true;
            }

            return clicked;
        }

        public void UiKeyDown(Keys key)
        {
            _focused?.DoKeyDown(key);
        }

        public void UiKeyUp(Keys key)
        {
            _focused?.DoKeyUp(key);
        }

        public void UiChar(Keys key, char character)
        {
            _focused?.DoCharInput(character);
        }

        public void UiMouseMove(MouseState state, int dx, int dy)
        {
            var X = state.X;
            var Y = state.Y;

            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.Dragging)
                {
                    c.DoMouseMove(X, Y);
                }
                else
                {
                    if (c.IsPointInside(X, Y))
                    {
                        if (!c.IsMouseOver)
                        {
                            c.DoMouseEnter(X, Y);
                        }

                        c.DoMouseMove(X, Y);
                    }
                    else
                    {
                        if (c.IsMouseOver)
                        {
                            c.DoMouseLeave(X, Y);
                        }
                    }
                }
            }
        }

        public virtual void UiResize(int width, int height)
        {

        }

        internal void UiMouseScroll(int x, int y, int delta)
        {
            var X = x;
            var Y = y;

            foreach (var c in _controls)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(X, Y))
                {
                    c.DoScroll(X, Y, delta);
                }
            }
        }
    }
}

#endif