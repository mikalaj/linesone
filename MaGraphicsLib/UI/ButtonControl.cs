﻿#if !NET5_0

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MaSoft.Framework.Graphics.UI
{
    public class ButtonControl : Control, IFontRequired
    {
        public ButtonControl()
            : this(Ui.StyleSet.ButtonStyle)
        {
        }

        public ButtonControl(ButtonControlStyle style)
            : base(style)
        {
            VerticalSizePolicy = SizePolicyType.Fixed;
            HorizontalSizePolicy = SizePolicyType.Expand;
        }

        public new ButtonControlStyle Style { get => base.Style as ButtonControlStyle; }

        protected override bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            c.DrawQuad(rect, Style.BackgroundColor);
            if (Font != null)
            {
                c.draw_text_hcenter(rect.X, rect.Y + rect.Height / 2, rect.Width, _font_size, Font, Style.TextColor, _text);
            }
            c.DrawRectangle(rect, Style.FrameColor);
            return false;
        }

        public SpriteFont Font { get; set; }


        public void set_font_size(int size)
        {
            _font_size = size;
            Invalidate();
        }

        public void set_text_color(Color c)
        {
            _color = c;
            Invalidate();
        }

        public string Text
        {
            get => _text;
            set
            {
                if (_text == value)
                    return;
                _text = value;
                Invalidate();
            }
        }

        int _font_size = 48;
        Color _color = new Color(200, 200, 200, 255);        
        string _text = string.Empty;
    }
}

#endif