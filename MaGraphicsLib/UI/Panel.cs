﻿#if !NET5_0


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MaSoft.Framework.Graphics.UI
{

    public class Panel : Control
    {
        public Panel()
            : this(Ui.StyleSet.PanelStyle)
        {
        }

        public Panel(PanelControlStyle style)
            : base(style)
        {
            OnVisibilityChanged += (o, e) => { Layout(); };
            OnGeometryChanged += (o, e) => { Layout(); };
            OnSizePolicyChanged += (o, e) => { Layout(); };
        }

        public List<Control> Children { get; } = new List<Control> { };

        private Control _focused = null;
        private bool _needClear = false;
        private Layout _layout;

        public new PanelControlStyle Style { get => base.Style as PanelControlStyle; }

        public void SetLayout(Layout layout)
        {
            _layout = layout;
            Layout();
        }

        public override SizePolicyType VerticalSizePolicy
        {
            get
            {
                if (Children.Any(o => o.Visible && o.VerticalSizePolicy == SizePolicyType.Expand))
                    return SizePolicyType.Expand;
                return base.VerticalSizePolicy;
            }

            protected set
            {
                base.VerticalSizePolicy = value;
            }
        }

        public override SizePolicyType HorizontalSizePolicy
        {
            get
            {
                if (Children.Any(o => o.Visible && o.HorizontalSizePolicy == SizePolicyType.Expand))
                    return SizePolicyType.Expand;
                return base.HorizontalSizePolicy;
            }

            protected set
            {
                base.HorizontalSizePolicy = value;
            }
        }

        public override IEnumerable<Control> FindChildren(Func<Control, bool> filter)
        {
            foreach (var c in Children)
            {
                if (filter?.Invoke(c) == true)
                    yield return c;

                foreach(var v in c.FindChildren(filter))
                {
                    yield return v;
                }
            }
        }

        public void AddControl(Control control)
        {
            if (control == null)
                return;

            control.OnVisibilityChanged += Control_OnVisibilityChanged;
            control.OnGeometryChanged += Control_OnGeometryChanged;
            control.OnSizePolicyChanged += Control_OnSizePolicyChanged;
            Children.Add(control);
            Layout();
        }

        private void Control_OnSizePolicyChanged(object sender, SizePolicyChangedArgs args)
        {
            Layout();
        }

        private void Control_OnGeometryChanged(object sender, GeometryChangedArgs args)
        {
            Layout();
        }

        private void Control_OnVisibilityChanged(object sender, VisibilityChangedArgs args)
        {
            Layout();
        }

        public void RemoveControl(Control control)
        {
            Children.Remove(control);
            control.OnVisibilityChanged -= Control_OnVisibilityChanged;
            control.OnGeometryChanged -= Control_OnGeometryChanged;
            control.OnSizePolicyChanged -= Control_OnSizePolicyChanged;
            Layout();    
        }

        private void Layout()
        {
            int width = Width;
            int height = Height;
            _layout?.Apply(ref width, ref height, Children);
            Resize(Position.X, Position.Y, width, height);
        }

        protected override bool OnDraw(IRenderLib render, Rectangle rect, TimeSpan dt)
        {
            var flag = base.OnDraw(render, rect, dt);

            render.DrawQuad(rect, Color.Red);

            if (_needClear)
            {
                _needClear = false;
            }

            if (Ui.IsMobile && _focused is TextInput input)
            {
                flag |= input.draw(render, dt);
            }
            else
            {
                foreach (var c in Children)
                {
                    if (!c.Visible)
                        continue;

                    flag |= c.draw(render, dt, rect.Location);
                }
            }

            return flag;
        }

        protected override void OnInvalidate()
        {
            base.OnInvalidate();

            foreach(var c in Children)
            {
                c.Invalidate();
            }
        }

        protected override void OnMouseLeftButtonDown(int x, int y)
        {
            base.OnMouseLeftButtonDown(x, y);

            foreach (var c in Children)
            {
                if (!c.Visible)
                    continue;

                var localX = x - Position.X;
                var localY = y - Position.Y;

                if (c.IsPointInside(localX, localY))
                {
                    c.DoMouseLeftButtonDown(localX, localY);
                    IsMouseLeftDown = false;
                    return;
                }
            }
        }

        protected override void OnMouseLeftButtonUp(int x, int y)
        {
            base.OnMouseLeftButtonUp(x, y);

            foreach (var c in Children)
            {
                if (!c.Visible)
                    continue;

                var localX = x - Position.X;
                var localY = y - Position.Y;

                if (c.IsPointInside(localX, localY))
                {
                    c.DoMouseLeftButtonUp(localX, localY);
                    return;
                }
            }
        }        

        protected override void OnClick(int x, int y)
        {
            base.OnClick(x, y);

            foreach (var c in Children.Where(o => o.Visible))
            {
                if (!c.Visible)
                    continue;

                var localX = x - Position.X;
                var localY = y - Position.Y;

                if (c.IsPointInside(localX, localY))
                {
                    c.DoClick(localX, localY);

                    if (_focused != null && _focused != c)
                    {
                        _focused.DoUnfocus();
                        _focused = null;
                        _needClear = true;
                    }

                    if (c.Focusable && !c.IsFocused)
                    {
                        _focused = c;
                        c.DoFocus();

                        if (c is TextInput input)
                        {
                            if (Ui.IsMobile)
                            {
                                var task = Ui.ShowTextInput(input.Title, input.Description, input.Text, input.IsPassword);
                                task.ContinueWith(a =>
                                {
                                    var text = a.Result;
                                    if (text != null)
                                    {
                                        input.Text = text;
                                    }
                                    input.DoUnfocus();
                                    _focused = null;
                                    _needClear = true;
                                });
                            }
                            else
                            {
                                //Ui.ShowKeyboard();
                            }
                        }
                        else
                        {
                            //Ui.HideKeyboard();
                        }
                        _needClear = true;
                    }

                    return;
                }
            }

            if (_focused != null)
            {
                _focused.DoUnfocus();
                _focused = null;
                //HideKeyboard();
                _needClear = true;
            }
        }

        protected override void OnKeyDown(Keys key)
        {
            base.OnKeyDown(key);
            _focused?.DoKeyDown(key);
        }

        protected override void OnKeyUp(Keys key)
        {
            base.OnKeyUp(key);
            _focused?.DoKeyUp(key);
        }

        protected override void OnCharInput(char character)
        {
            base.OnCharInput(character);
            _focused?.DoCharInput(character);
        }

        protected override void OnResize()
        {
            base.OnResize();
            Layout();
        }

        protected override void OnScroll(int x, int y, int delta)
        {
            var localX = x - Position.X;
            var localY = y - Position.Y;

            foreach (var c in Children)
            {
                if (!c.Visible)
                    continue;

                if (c.IsPointInside(localX, localY))
                {

                    c.DoScroll(localX, localY, delta);
                }
            }

            base.OnMouseMove(x, y);
        }

        protected override void OnMouseMove(int x, int y)
        {            
            var localX = x - Position.X;
            var localY = y - Position.Y;

            foreach (var c in Children)
            {
                if (!c.Visible)
                    continue;
                if (c.Dragging)
                {
                    c.DoMouseMove(localX, localY);
                }
                else
                {
                    if (c.IsPointInside(localX, localY))
                    {
                        if (!c.IsMouseOver)
                        {
                            c.DoMouseEnter(localX, localY);
                        }

                        c.DoMouseMove(localX, localY);
                    }
                    else
                    {
                        if (c.IsMouseOver)
                        {
                            c.DoMouseLeave(localX, localY);
                        }
                    }
                }
            }            

            base.OnMouseMove(x, y);
        }
    }
}

#endif