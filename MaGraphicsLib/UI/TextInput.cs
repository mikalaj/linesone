﻿#if !NET5_0


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Text;

namespace MaSoft.Framework.Graphics.UI
{
    public delegate void TextInputEnteredDelegate(object o);

    public class TextInput : Control, IFontRequired
    {
        public TextInput()
            : base(Ui.StyleSet.TextIntputStyle)
        {
        }

        public TextInput(TextInputControlStyle style)
            : base(style)
        {
        }


        public new TextInputControlStyle Style { get => base.Style as TextInputControlStyle; }

        public event TextInputEnteredDelegate OnTextEntered;

        protected override bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            if (Font == null)
                return false;

            if (IsFocused && !Ui.IsMobile)
            {
                var x = rect.X;
                var y = rect.Y;
                c.DrawQuad(x, y, rect.Width, rect.Height, new Color(200, 200, 200, 255));
                c.DrawRectangle(x, y, rect.Width, rect.Height, Color.Black);
                c.DrawTextLeft(x, y, 14, Font, Color.Black, _drawText);

                if (_showCursor)
                {
                    c.DrawLine(x + (int)_cursorOffset, y, x + (int)_cursorOffset, y + rect.Height, 5, Color.Red);
                }                

                _cursorTimeOut -= dt;

                if (_cursorTimeOut.TotalSeconds < 0)
                {
                    _showCursor = !_showCursor;
                    _cursorTimeOut = TimeSpan.FromMilliseconds(500);
                }

                return true;
            }
            else
            {
                c.DrawQuad(rect.X, rect.Y, rect.Width, rect.Height, new Color(200, 200, 200, 255));
                c.DrawRectangle(rect.X, rect.Y, rect.Width, rect.Height, Color.Black);
                c.DrawTextLeft(rect.X, rect.Y, 10, Font, Color.Black, _drawText);
            }
            return false;
        }

        protected override void OnKeyDown(Keys key)
        {
            if (key == Keys.Left)
            {
                SetPosition(_cursorPosition - 1);
            }

            if (key == Keys.Right)
            {
                SetPosition(_cursorPosition + 1);
            }

            if (key == Keys.Back)
            {
                if (_cursorPosition != 0 && _text.Length != 0)
                {
                    _text.Remove(_cursorPosition - 1, 1);
                    PrevPosition();
                }
            }

            if (key == Keys.Delete)
            {
                if (_cursorPosition < _text.Length)
                {
                    _text.Remove(_cursorPosition, 1);
                }
            }

            UpdateVisibleText();

            Invalidate();
        }

        protected override void OnKeyUp(Keys key)
        {
            
        }

        protected override void OnCharInput(char character)
        {
            if (Char.IsControl(character))
                return;            

            if (!string.IsNullOrEmpty(Ui.SupportedCharacters))
            {
                if (!Ui.SupportedCharacters.Contains(character))
                    return;
            }

            if (_text.Length == _cursorPosition)
                _text.Append(character);
            else
                _text.Insert(_cursorPosition, character);

            NextPosition();

            Invalidate();
        }

        private void NextPosition()
        {
            SetPosition(_cursorPosition + 1);
        }

        private void PrevPosition()
        {
            SetPosition(_cursorPosition - 1);
        }

        private void UpdateVisibleText()
        {
            var text = _text.ToString();

            if (_textOffset > _cursorPosition)
                _textOffset = _cursorPosition;

            var size = Font.MeasureString(text.Substring(_textOffset, _cursorPosition - _textOffset));
            _cursorOffset = size.X;

            while(_cursorOffset > Width)
            {
                _textOffset++;
                size = Font.MeasureString(text.Substring(_textOffset, _cursorPosition - _textOffset));
                _cursorOffset = size.X;
            }

            _visibleSymbolsCount = _cursorPosition - _textOffset;
            while (size.X < Width && _visibleSymbolsCount + _textOffset < text.Length)
            {
                _visibleSymbolsCount++;
                size = Font.MeasureString(text.Substring(_textOffset, _visibleSymbolsCount));
            }

            _drawText = text.Substring(_textOffset, _visibleSymbolsCount);
        }

        private void SetPosition(int value)
        {
            _cursorPosition = Math.Min(_text.Length, Math.Max(0, value));            
        }

        protected override void OnFocus()
        {            
        }

        protected override void OnUnfocus()
        {
            _showCursor = false;
            _visibleSymbolsCount = 0;
            var text = _text.ToString();
            var size = Font.MeasureString(text.Substring(0, _visibleSymbolsCount));

            while (size.X < Width)
            {
                _visibleSymbolsCount++;
                if (_visibleSymbolsCount > text.Length)
                {
                    _visibleSymbolsCount = text.Length;
                    break;
                }

                size = Font.MeasureString(text.Substring(0, _visibleSymbolsCount));
            }

            _drawText = text.Substring(0, _visibleSymbolsCount);

            OnTextEntered?.Invoke(this);
        }

        public string Text
        {
            get => _text.ToString();
            set
            {
                _text = new StringBuilder(value);
                _cursorPosition = 0;
                _textOffset = 0;
                UpdateVisibleText();
                Invalidate();
            }
        }

        protected override void OnResize()
        {
            UpdateVisibleText();
        }

        protected override void OnScroll(int x, int y, int delta)
        {
            base.OnScroll(x, y, delta);            
        }

        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Default { get; set; } = string.Empty;
        public bool IsPassword { get; set; } = false;

        public SpriteFont Font { get; set; }

        private bool _showCursor = false;
        private TimeSpan _cursorTimeOut = TimeSpan.FromMilliseconds(500);
        private int _cursorPosition;
        private int _textOffset;
        private int _visibleSymbolsCount;
        private float _cursorOffset;
        private StringBuilder _text = new StringBuilder("Username");
        private string _drawText = "Username";
        private bool _need_clear;
    }
}

#endif