﻿#if !NET5_0


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace MaSoft.Framework.Graphics.UI
{
    public enum SizePolicyType
    {
        Minimize,
        Maximize,
        Expand,
        Fixed
    }

    public class VisibilityChangedArgs : EventArgs
    {
        public bool Visibile;
    }

    public class GeometryChangedArgs : EventArgs
    {
        public Rectangle Geometry;
    }

    public class SizePolicyChangedArgs : EventArgs
    {
        public SizePolicyType HorizontalSizePolicy;
        public SizePolicyType VerticalSizePolicy;
    }

    public delegate void ControlClickDelegate(object sender, EventArgs args);
    public delegate void VisibilityChangedDelegate(object sender, VisibilityChangedArgs args);
    public delegate void GeometryChangedDelegate(object sender, GeometryChangedArgs args);
    public delegate void SizePolicyChangedDelegate(object sender, SizePolicyChangedArgs args);

    public class Control
    {

        public Control(ControlStyle style)
        {
            Style = style;
        }

        SizePolicyType _verticalSizePolicy = SizePolicyType.Fixed;
        public virtual SizePolicyType VerticalSizePolicy
        {
            get => _verticalSizePolicy; 
            
            protected set
            {
                if (_verticalSizePolicy == value)
                    return;

                _verticalSizePolicy = value;
                OnSizePolicyChanged?.Invoke(this, new SizePolicyChangedArgs { HorizontalSizePolicy = HorizontalSizePolicy, VerticalSizePolicy = VerticalSizePolicy});
            }
        }

        SizePolicyType _horizontalSizePolicy = SizePolicyType.Fixed;
        public virtual SizePolicyType HorizontalSizePolicy
        {
            get => _horizontalSizePolicy;

            protected set
            {
                if (_horizontalSizePolicy == value)
                    return;

                _horizontalSizePolicy = value;
                OnSizePolicyChanged?.Invoke(this, new SizePolicyChangedArgs { HorizontalSizePolicy = HorizontalSizePolicy, VerticalSizePolicy = VerticalSizePolicy });
            }
        }

        public void Invalidate()
        {
            _need_draw = true;
            OnInvalidate();
        }

        public bool draw(IRenderLib c, TimeSpan dt)
        {
            return draw(c, dt, Point.Zero);
        }

        public bool draw(IRenderLib c, TimeSpan dt, Point parent)
        {
            if (!_visible)
                return false;            

            if (_need_draw)
            {
                _need_draw = OnDraw(c, new Rectangle(_rect.Location + parent, _rect.Size), dt);
            }

            return _need_draw;
        }

        protected virtual bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            return false;
        }

        protected virtual void OnInvalidate()
        {

        }

        public virtual int MinHeight { get => Height; }
        public virtual int MinWidth { get => Width; }

        public bool Visible
        {
            get => _visible;
            set
            {
                if (_visible == value)
                    return;
                _visible = value;

                if (_visible)
                    _need_draw = true;

                OnVisibilityChanged?.Invoke(this, new VisibilityChangedArgs { Visibile = _visible });
            }
        }

        public bool IsPointInside(int x, int y)
        {
            return x > _rect.X && x < _rect.X + _rect.Width && y > _rect.Y && y < _rect.Y + _rect.Height;
        }

        public void SetPosition(int x, int y)
        {
            Resize(x, y, _rect.Width, _rect.Height);            
        }

        public void Resize(int x, int y, int width, int height)
        {
            if (_rect.X != x || _rect.Y != y || _rect.Width != width || _rect.Height != height)
            {
                _rect.X = x;
                _rect.Y = y;
                _rect.Width = width;
                _rect.Height = height;
                OnGeometryChanged?.Invoke(this, new GeometryChangedArgs { Geometry = _rect });
                OnResize();
                Invalidate();
            }
        }

        protected virtual void OnResize()
        {

        }
        

        public void DoMouseLeftButtonUp(int x, int y)
        {
            if (Dragging)
            {
                Dragging = false;
                OnEndDrag(x, y);
            }

            State = States.upping;
            IsMouseLeftDown = false;
            OnMouseLeftButtonUp(x, y);
        }        

        protected virtual void OnMouseLeftButtonUp(int x, int y)
        {

        }

        public void DoMouseLeftButtonDown(int x, int y)
        {
            State = States.downing;
            IsMouseLeftDown = true;
            OnMouseLeftButtonDown(x, y);
        }

        protected virtual void OnMouseLeftButtonDown(int x, int y)
        {
        }

        public void DoMouseRightButtonUp(int x, int y)
        {
            if (Dragging)
            {
                Dragging = false;
                OnEndDrag(x, y);
            }

            State = States.upping;
            IsMouseRightDown = false;
            OnMouseRightButtonUp(x, y);
        }

        protected virtual void OnMouseRightButtonUp(int x, int y)
        {

        }

        public void DoMouseRightButtonDown(int x, int y)
        {
            State = States.downing;
            IsMouseRightDown = true;
            OnMouseRightButtonDown(x, y);
        }

        protected virtual void OnMouseRightButtonDown(int x, int y)
        {
        }

        public bool IsMouseOver { get; internal set; }
        protected bool IsMouseLeftDown { get; set; }
        protected bool IsMouseRightDown { get; set; }

        internal void DoMouseEnter(int x, int y)
        {
            if (!IsMouseOver)
            {
                State = States.focusing;
                IsMouseOver = true;
                Invalidate();
            }
            //set_text_color(new Color(220, 220, 220, 255));
        }

        internal void DoMouseLeave(int x, int y)
        {
            if (IsMouseOver)
            {
                if (State == States.downing)
                {
                    State = States.unfocusing;
                }
                else
                {
                    State = States.unfocusing;
                }

                IsMouseOver = false;
                Invalidate();
            }

            //set_text_color(new Color(200, 200, 200, 255));
        }
        
        int prev_x = 0;
        int prev_y = 0;

        protected virtual void OnBeginDrag(int x, int y)
        {
        }

        protected virtual void OnDragging()
        {
        }

        private void OnEndDrag(int x, int y)
        {
        }

        internal void DoMouseMove(int x, int y)
        {
            if (IsMouseLeftDown && AllowDrag)
            {
                if (Dragging)
                {                    
                    Resize(Position.X + (x - prev_x), Position.Y + (y - prev_y), Width, Height);
                    prev_x = x;
                    prev_y = y;
                    OnDragging();
                }
                else
                {
                    Dragging = true;
                    OnBeginDrag(x, y);
                    prev_x = x;
                    prev_y = y;
                }
            }
            else
            {
                OnMouseMove(x, y);
            }
        }

        protected virtual void OnMouseMove(int x, int y)
        {
        }

        public void DoClick(int x, int y)
        {
            OnClick(x, y);
            OnClicked?.Invoke(this, new EventArgs());
        }

        protected virtual void OnClick(int x, int y) 
        {
        }

        public void DoRightClick(int x, int y)
        {
            OnRightClick(x, y);
            OnRightClicked?.Invoke(this, new EventArgs());
        }

        protected virtual void OnRightClick(int x, int y)
        {
        }

        internal bool Focusable { get; set; } = true;
        public bool IsFocused => _focused;

        internal void DoFocus()
        {
            _focused = true;
            OnFocus();
            Invalidate();
        }

        protected virtual void OnFocus()
        {

        }

        public bool Dragging { get; protected set; }

        internal void DoUnfocus()
        {
            _focused = false;
            OnUnfocus();
            Invalidate();
        }

        protected virtual void OnUnfocus()
        {
        }

        internal void DoScroll(int x, int y, int delta)
        {
            OnScroll(x, y, delta);
        }

        protected virtual void OnScroll(int x, int y, int delta)
        {
        }

        internal void DoKeyDown(Keys key)
        {
            OnKeyDown(key);
        }

        protected virtual void OnKeyDown(Keys key)
        {

        }

        internal void DoKeyUp(Keys key)
        {
            OnKeyUp(key);
        }

        protected virtual void OnKeyUp(Keys key)
        {
        }

        internal void DoCharInput(char character)
        {
            OnCharInput(character);
        }

        protected virtual void OnCharInput(char character)
        {

        }

        public int Width
        {
            get => _rect.Width;
            set
            {
                Resize(_rect.X, _rect.Y, value, _rect.Height);
            }
        }

        public int Height
        {
            get => _rect.Height;
            set
            {
                Resize(_rect.X, _rect.Y, _rect.Width, value);
            }
        }        

        internal enum States
        {
            idle,
            focusing,
            unfocusing,
            downing,
            upping
        }

        public Point Position { get => _rect.Location; }

        internal States State { get; set; } = States.idle;
        public bool AllowDrag { get; set; }
        public ControlStyle Style { get; }        
        public string Name { get; set; }

        public virtual void ApplyStyleSet(ControlStyleSet set)
        {
        }

        public virtual IEnumerable<Control> FindChildren(Func<Control, bool> filter)
        {
            yield break;
        }

        private Rectangle _rect = new Rectangle(0,0, 64, 64);
        private bool _need_draw = true;
        private bool _visible = true;
        private bool _focusable = true;
        private bool _focused = false;        

        public event ControlClickDelegate OnClicked;
        public event ControlClickDelegate OnRightClicked;
        public event VisibilityChangedDelegate OnVisibilityChanged;
        public event GeometryChangedDelegate OnGeometryChanged;
        public event SizePolicyChangedDelegate OnSizePolicyChanged;
    }
}

#endif