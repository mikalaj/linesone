﻿#if !NET5_0

using Microsoft.Xna.Framework.Graphics;

namespace MaSoft.Framework.Graphics.UI
{
    public interface IFontRequired
    {
        public SpriteFont Font { get; set; }
    }
}

#endif