﻿#if !NET5_0


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace MaSoft.Framework.Graphics.UI
{    
    public class Item
    {
        public string Text { get; set; }
        public object Tag { get; set; }
    }

    public class ItemsView : Control, IFontRequired
    {
        public ItemsView(ItemsViewStyle style) : base(style)
        {
            VerticalSizePolicy = SizePolicyType.Expand;
            HorizontalSizePolicy = SizePolicyType.Expand;
        }

        public ItemsView()
            : this(Ui.StyleSet.ItemsViewStyle)
        {
        }

        public new ItemsViewStyle Style { get => base.Style as ItemsViewStyle; }

        protected override bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            c.DrawQuad(rect, Style.BackgroundColor);
            if (Font != null)
            {
                int y = 0;
                int x = 0;

                var dy = (int)ItemHeight;

                for (int i = _offset; i < _items.Count; ++i)
                {
                    if (y + dy > rect.Height)
                        break;

                    if (i == _selectedIndex)
                    {
                        c.DrawQuad(rect.X + x, rect.Y + y - 2, rect.Width, dy + 2, Style.SelectionFill);
                        c.DrawRectangle(rect.X + x, rect.Y + y - 2, rect.Width, dy + 2, Style.SelectionFrame);
                    }
                    c.DrawTextLeft(rect.X + x, rect.Y + y, 0, Font, Style.TextColor, _items[i].Text);
                    y += dy;
                }
            }
            c.DrawRectangle(rect, Style.FrameColor);
            return false;
        }

        private SpriteFont _font;
        public SpriteFont Font
        {
            get => _font; 
            set
            {
                _font = value;
                if (_font != null)
                {
                    ItemHeight = _font.MeasureString("M").Y;
                }
            }
        }

        public void RemoveAllItems()
        {
            _items.Clear();
            Invalidate();
        }
        
        public void AddItem(string text, object tag = null)
        {
            _items.Add(new Item { Text = text, Tag = tag });
            Invalidate();
        }

        public void RemoveItem(int index)
        {
            _items.RemoveAt(index);
            Invalidate();
        }

        public IReadOnlyCollection<Item> Items { get => _items.AsReadOnly(); }
        public int SelectedIndex
        {
            get => _selectedIndex; 
            set
            {
                if (_selectedIndex == value)
                    return;

                SetSelect(value);
            }
        }

        public Item SelectedItem
        {
            get
            {
                if (SelectedIndex >= 0 && SelectedIndex < Items.Count)
                {
                    return _items[SelectedIndex];
                }

                return null;
            }
        }

        public float ItemHeight { get; private set; }

        private void SetSelect(int value)
        {
            _selectedIndex = value;
            Invalidate();
        }

        protected override void OnClick(int x, int y)
        {
            base.OnClick(x, y);

            SetSelect(Math.Min(_items.Count - 1, _offset + y / (int)ItemHeight));
            Invalidate();
        }

        protected override void OnKeyDown(Keys key)
        {
            if (key == Keys.Up)
            {
                _offset = Math.Max(0, _offset - 1);
                Invalidate();
            } 
            else if (key == Keys.Down)
            {
                _offset = Math.Min(_items.Count, _offset + 1);
                Invalidate();
            }
        }

        protected override void OnScroll(int x, int y, int delta)
        {
            _offset = Math.Min(_items.Count, Math.Max(0, _offset + delta));
            Invalidate();
        }

        private List<Item> _items = new List<Item>();
        private int _selectedIndex = -1;
        private int _offset;
    }
}

#endif