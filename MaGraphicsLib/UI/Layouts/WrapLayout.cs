﻿#if !NET5_0


using System.Collections.Generic;
using System.Linq;

namespace MaSoft.Framework.Graphics.UI
{
    public sealed class WrapLayout : Layout
    {
        public override void Apply(ref int width, ref int height, IEnumerable<Control> items)
        {
            var offsetX = 0;
            var offsetY = 0;
            var maxHeight = 0;
            var firstInRow = true;

            foreach(var item in items.Where(o => o.Visible))
            {
                if (offsetX + Margin.Left + item.Width + Margin.Right <= width || firstInRow)
                {
                    item.SetPosition(offsetX + Margin.Left, offsetY + Margin.Top);
                    offsetX += Margin.Left + item.Width + Margin.Right;
                    if (maxHeight < Margin.Top + Margin.Bottom + item.Height)
                    {
                        maxHeight = Margin.Top + Margin.Bottom + item.Height;
                    }
                }
                else
                {
                    offsetY += maxHeight;
                    offsetX = 0;
                    item.SetPosition(offsetX + Margin.Left, offsetY + Margin.Top);
                    offsetX = Margin.Left + item.Width + Margin.Right;                    
                    maxHeight = Margin.Top + Margin.Bottom + item.Height;
                }

                firstInRow = false;
            }

            height = offsetY + maxHeight;
        }
    }
}

#endif