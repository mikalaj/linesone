﻿#if !NET5_0


using System.Collections.Generic;

namespace MaSoft.Framework.Graphics.UI
{
    public abstract class Layout
    {
        public Margin Margin { get; set; } = new Margin { Left = 5, Bottom = 0, Right = 0, Top = 5 };
        public abstract void Apply(ref int width, ref int height, IEnumerable<Control> items);
    }
}

#endif