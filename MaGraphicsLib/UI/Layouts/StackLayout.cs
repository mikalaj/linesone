﻿#if !NET5_0


using System.Collections.Generic;
using System.Linq;

namespace MaSoft.Framework.Graphics.UI
{
    public enum StackOrientation
    {
        Vertical,
        Horizontal
    }

    public sealed class StackLayout : Layout
    {
        private StackOrientation _orientation;

        public StackLayout(StackOrientation orientation)
        {
            _orientation = orientation;
            if (_orientation == StackOrientation.Vertical)
                Margin = new Margin { Left = 4, Right = 4, Top = 2, Bottom = 2 };
            else
                Margin = new Margin { Left = 2, Right = 2, Top = 4, Bottom = 4 };
        }

        private bool _recursion;

        public override void Apply(ref int width, ref int height, IEnumerable<Control> items)
        {
            if (_recursion)
                return;

            _recursion = true;

            if (_orientation == StackOrientation.Vertical)
                ApplyVertical(ref width, ref height, items);
            else
                ApplyHorizontal(ref width, ref height, items);

            _recursion = false;
        }

        private void ApplyHorizontal(ref int width, ref int height, IEnumerable<Control> items)
        {
            var offsetX = 0;                        

            foreach (var item in items.Where(o => o.Visible))
            {
                item.Resize(Margin.Left + offsetX, Margin.Top, item.Width, height - Margin.Top - Margin.Bottom);
                offsetX += item.Width + Margin.Left + Margin.Right;                
            }

            if (width < offsetX)
                width = offsetX;
            else
            {
                var diff = width - offsetX;
                var expandable = items.Where(o => o.Visible && o.HorizontalSizePolicy == SizePolicyType.Expand);
                var count = expandable.Count();
                if (count != 0)
                {
                    offsetX = 0;
                    var delta = diff / count;
                    foreach (var item in items.Where(o => o.Visible))
                    {
                        if (item.HorizontalSizePolicy == SizePolicyType.Expand)
                        {
                            item.Resize(Margin.Left + offsetX, Margin.Top, item.Width + delta, height - Margin.Top - Margin.Bottom);
                            offsetX += item.Width + Margin.Left + Margin.Right + delta;
                        }
                        else
                        {
                            item.Resize(Margin.Left + offsetX, Margin.Top, item.Width, height - Margin.Top - Margin.Bottom);
                            offsetX += item.Width + Margin.Left + Margin.Right;
                        }
                    }
                }
            }
        }
        
        private void ApplyVertical(ref int width, ref int height, IEnumerable<Control> items)
        {
            var offsetY = 0;

            foreach (var item in items.Where(o => o.Visible))
            {
                item.Resize(Margin.Left, offsetY + Margin.Top, width - Margin.Left - Margin.Right, item.Height);
                offsetY += item.Height + Margin.Top + Margin.Bottom;                
            }

            if (height < offsetY)
                height = offsetY;
            else if (height > offsetY)
            {
                var diff = height - offsetY;
                var expandable = items.Where(o => o.Visible && o.VerticalSizePolicy == SizePolicyType.Expand);
                var count = expandable.Count();
                if (count != 0)
                {
                    offsetY = 0;
                    var delta = diff / count;
                    foreach (var item in items.Where(o => o.Visible))
                    {
                        if (item.VerticalSizePolicy == SizePolicyType.Expand)
                        {
                            item.Resize(Margin.Left, offsetY + Margin.Top, width - Margin.Left - Margin.Right, item.Height + delta);
                            offsetY += item.Height + Margin.Top + Margin.Bottom + delta;
                        }
                        else
                        {
                            item.Resize(Margin.Left, offsetY + Margin.Top, width - Margin.Left - Margin.Right, item.Height);
                            offsetY += item.Height + Margin.Top + Margin.Bottom;
                        }
                    }
                }
            }
        }
    }
}

#endif