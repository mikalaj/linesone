﻿#if !NET5_0

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MaSoft.Framework.Graphics.UI
{
    public class IconControl : Control
    {        
        public IconControl()
            : base(Ui.StyleSet.IconStyle)
        {
        }

        public IconControl(IconControlStyle style)
            : base(style)
        {

        }

        protected override bool OnDraw(IRenderLib c, Rectangle rect, TimeSpan dt)
        {
            if (!Visible)
                return false;

            if (State == States.focusing)
            {
                _color = Color.Lerp(Style._noFocus, Style._Focused, _focusFactor);
                _focusFactor += (float)dt.TotalSeconds / (float)_focusDt.TotalSeconds;

                if (_focusFactor >= 1.0f)
                {
                    State = States.idle;
                }
            }

            if (State == States.unfocusing)
            {
                _color = Color.Lerp(Style._noFocus, Style._Focused, _focusFactor);
                _focusFactor -= (float)dt.TotalSeconds / (float)_focusDt.TotalSeconds;

                if (_focusFactor <= 0.0f)
                {
                    State = States.idle;
                }
            }

            if (State == States.downing)
            {
                _color = Color.Lerp(Style._Focused, Style._Pressed, _focusFactor);
                _focusFactor += (float)dt.TotalSeconds / (float)_downDt.TotalSeconds;

                if (_focusFactor <= 0.0f)
                {
                    State = States.idle;
                }
            }

            if (State == States.upping)
            {
                _color = Color.Lerp(Style._Focused, Style._Pressed, _focusFactor);
                _focusFactor -= (float)dt.TotalSeconds / (float)_downDt.TotalSeconds;

                if (_focusFactor <= 0.0f)
                {
                    State = States.idle;
                }
            }


            if (rect.Width != 0 && rect.Height != 0)
            {
                c.DrawQuad(rect.X, rect.Y, rect.Width, rect.Height, _color);                
                if (_icon != null)
                {
                    var scalex = rect.Width / (float)_icon.Width;
                    var scaley = rect.Height / (float)_icon.Height;
                    c.draw_tile(rect.X, rect.Y, scalex, scaley, _icon);
                }
                //c.draw_rect(_rect.X, _rect.Y, _rect.Width, _rect.Height, _color);
                // c.draw_text_hcenter(_rect.X, _rect.Y + _rect.Height / 2, _rect.Width,  _font_size, a.GetFont(FontType.Score), _color, _text);                
            }

            return State != States.idle;
        }

        public void SetIcon(Texture2D texture2D)
        {
            _icon = texture2D;
            Invalidate();
        }                
        

        public void set_font_size(int size)
        {
            _font_size = size;
            Invalidate();
        }

        public void set_text_color(Color c)
        {
            _color = c;
            Invalidate();
        }

        public void set_text(string value)
        {
            _text = value;
            Invalidate();
        }

        public new IconControlStyle Style { get => base.Style as IconControlStyle; }

        int _font_size = 48;                
        string _text = string.Empty;
        private Texture2D _icon;
        Color _color = new Color(200, 200, 200, 255);
        float _focusFactor = 0;
        
        private static readonly TimeSpan _focusDt = TimeSpan.FromMilliseconds(200);
        private static readonly TimeSpan _downDt = TimeSpan.FromMilliseconds(100);
    }
}

#endif