﻿#if !NET5_0

using MaSoft.Framework.Graphics;
using MaSoft.Framework.Graphics.UI;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace MaSoft.Framework.Graphics.UI
{
    public class UiImpl<T> : Ui where T : class
    {
        private T _app;
                        

        protected override void OnInit(object app)
        {
            _app = app as T;
            OnInit(_app);
        }

        protected virtual void OnInit(T app)
        {

        }
        
        protected virtual void ShowKeyboard(T app)
        {

        }

        protected virtual void HideKeyboard(T app)
        {

        }

        public override void UiResize(int width, int height)
        {
            base.UiResize(width, height);
            OnResize(_app, width, height);
        }

        protected virtual void OnResize(T app, int width, int height)
        {             
                
        }

        protected override void ShowKeyboard()
        {
            ShowKeyboard(_app);
        }

        protected override void HideKeyboard()
        {
            HideKeyboard(_app);
        }
    }
}

#endif