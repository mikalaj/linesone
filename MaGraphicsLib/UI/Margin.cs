﻿#if !NET5_0


namespace MaSoft.Framework.Graphics.UI
{
    public struct Margin
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    }
}

#endif