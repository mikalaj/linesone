﻿#if !NET5_0

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace MaSoft.Framework.Graphics
{
    public delegate void OnResize(int width, int height);
    public delegate void OnMouseLeftButtonDown(MouseState state);
    public delegate void OnMouseLeftButtonUp(MouseState state);
    public delegate void OnMouseLeftClick(MouseState state);
    public delegate void OnMouseMove(MouseState state, int dx, int dy);
    public delegate void OnKeyDown(Keys key);
    public delegate void OnKeyUp(Keys key);

    public interface IRenderLib
    {
        int Width { get; }
        int Height { get; }
        GraphicsDevice Device { get; }
        void DrawLine(int x1, int y1, int x2, int y2, Color p2);
        void DrawLine(int x1, int y1, int x2, int y2, int size, Color p2);
        void draw_tile(int x, int y, Texture2D tile);
        void draw_tile(int x, int y, float scale, Texture2D tile);
        void draw_tile(int x, int y, float scalex, float scaley, Texture2D tile);
        void DrawText(float v1, float v2, int font_size, SpriteFont spriteFont, Color color, string text);
        void DrawTextLeft(float x, float y, int font_size, SpriteFont spriteFont, Color color, string text);
        void DrawQuad(int x, int y, int width, int height, Color color);
        void DrawQuad(Rectangle rect, Color color);
        void DrawRectangle(int x, int y, int width, int height, Color color);
        void DrawRectangle(Rectangle rect, Color color);
        void draw_text_hcenter(int x, int y, int width, int size, SpriteFont spriteFont, Color color, string text_score);
        
        event OnResize OnResize;
        event OnMouseLeftButtonDown OnMouseLeftButtonDown;
        event OnMouseMove OnMouseMove;
        event OnMouseLeftButtonUp OnMouseLeftButtonUp;
        event OnMouseLeftClick OnMouseLeftClick;
        event OnKeyDown OnKeyDown;
        event OnKeyUp OnKeyUp;

        void Resize(int width, int height);
        void DrawSphere(Matrix matrix1, Matrix matrix2, Matrix matrix3, Color color);
        SpriteBatch Sprite { get; }
        RenderTarget2D BackBuffer { get; }

        void SwapBuffer();
        void Clear();
    }
}
#endif
