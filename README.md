# Lines One
Implementation of the Lines game using MonoGame framework with global score table. 
Score table implemented as a GRPC services hosted on the dedicated server. 
Every instance of the game is treated as a unique user and user is identified by the generated at first launch key pair.
Player can set its name though uniqueness of this name is not checked.


# Screens
## Windows game play screen
![](doc/windows_gameplay.png)

## Windows score screen
![](doc/windows_score.png)

## Windows settings screen
![](doc/windows_settings.png)

## Android game play screen
![](doc/android_gameplay.png)

## Android score screen
![](doc/android_score.png)

## Android settings screen
![](doc/android_settings.png)

## Android settings screen name enter
![](doc/android_settings_name.png)

