﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaSoft.Framework.MathEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace MaSoft.Framework.MathEx.Tests
{
    [TestClass()]
    public class ExtensionsTests
    {
        [TestMethod()]
        public void ClosestPtSegmentSegmentTest()
        {
            var p1 = new Vector3(10, 10, 0);
            var q1 = new Vector3(-10, -10, 0);
            var p2 = new Vector3(10, -10, 0);
            var q2 = new Vector3(-10, 10, 0);
            var dst = Extensions.ClosestPtSegmentSegment(p1, q1, p2, q2, out float s, out float y, out Vector3 c1, out Vector3 c2);
        }

        [TestMethod()]
        public void TouchSphereTest()
        {
            var c = new Vector3(2, 2, 0);
            var p = new Vector3(10, -1, 0);
            var r = 3;

            var t = Extensions.TouchSphere(c, new Vector3(0, 0, 1), p, r);

            Assert.AreEqual(2, t.Count());
        }

        [TestMethod()]
        public void TouchPointsTest()
        {
            var p = new List<Vector3>
            {
                new Vector3(1,1,0),
                new Vector3(2,2,0),
                new Vector3(3,-1,0),
                new Vector3(4,0,0)
            };

            var c = new Vector3(7, 1, 0);

            var t = Extensions.TouchPoints(p, c);
            Assert.AreEqual(2, t.Count());
        }
    }
}