﻿using MaSoft.Framework;
using System;
using System.Threading.Tasks;

namespace MaSoft.Client
{
    class ScoreClientApp : Program
    {
        static ScoreClientApp()
        {
            Current = new ScoreClientApp("MaSoft", "ScoreClient");
        }

        public ScoreClientApp(string company, string application)
            : base(company, application)
        {
        }

        static async Task Main(string[] args)
        {
            await Current.RunAsync(args);
        }

        protected override async Task OnRunAsync(string[] args)
        {
            var IdentityServer = "https://localhost:5002";
            var ScoreServer = "https://localhost:5003";
            if (args.Length > 0)
                IdentityServer = args[0];

            var client = MaSoft.Framework.Score.Module.CreateScoreClient();
            try
            {
                await client.SetConnectionInfoAsync(ScoreServer, IdentityServer, Identity);
                var items = await client.GetTopAsync(100);
                Console.WriteLine($"Token: {items.Count}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to get token. {e.Message}");
            }
        }
    }
}
