﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaSoft.Framework.Score;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MaSoft.Framework.Score.Tests
{
    [TestClass()]
    public class DbScoreTableTests
    {
        [TestMethod()]
        public void AddScoreTest()
        {
            var filename = @".\score.db";
            if (File.Exists(filename))
                File.Delete(filename);

            var db = new DbScoreTable(filename);

            db.AddScore("John", 20);
            db.AddScore("Smith", 15);
            db.AddScore("Alph", 100);

            var items = db.GetTop(100).ToList();

            Assert.AreEqual(3, items.Count);
            Assert.AreEqual("Alph", items[0].Name);
            Assert.AreEqual(100, items[0].Points);
        }

        [TestMethod()]
        public void DeleteScoreTest()
        {
            var filename = @".\score.db";
            if (File.Exists(filename))
                File.Delete(filename);

            var db = new DbScoreTable(filename);

            db.AddScore("John", 20);
            db.AddScore("Smith", 15);
            db.AddScore("Alph", 100);

            var items = db.GetTop(100).ToList();

            Assert.AreEqual(3, items.Count);
            Assert.AreEqual("Alph", items[0].Name);
            Assert.AreEqual(100, items[0].Points);

            db.DeleteScore(items[0].Id);

            items = db.GetTop(100).ToList();

            Assert.AreEqual(2, items.Count);
            Assert.AreEqual("John", items[0].Name);
            Assert.AreEqual(20, items[0].Points);
        }
    }
}