﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MaSoft.Framework
{
    public static class SettingNames
    {
        public static string ListenAddress = "ListenAddress";
        public static string ListenAddressDefault = "*";
        public static string ListenPort = "ListenPort";
        public static int ListenPortDefault = 5002;
        public static string ServerCertificate = "ServerCertificate";
        public static string ServerPrivateCertificate = "ServerPrivateKey";
        public static string ServerPublicCertificate = "ServerPublicKey";
        public static string ProgramPublicKey = "ProgramPublicKey";
        public static string ProgramPrivateKey = "ProgramPrivateKey";
        public static string ProgramPrivateKeyParameters = "ProgramPrivateKeyParameters";
    }    

    public class Settings
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public Settings(string location)
        {
            Path = location;
            LoadSettings();
        }

        public string Path { get; }
        public void LoadSettings()
        {
            try
            {
                if (!File.Exists(Path))
                    return;

                Config = System.Text.Json.JsonSerializer.Deserialize<Storage>(File.ReadAllText(Path));
                
            }
            catch (Exception e)
            {
                Log.Error($"Failed to load settings. {e.Message}");
            }
        }

        public void Save()
        {
            try
            {
                var dir = System.IO.Path.GetDirectoryName(Path);

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                File.WriteAllText(Path, System.Text.Json.JsonSerializer.Serialize(Config, typeof(Storage), new System.Text.Json.JsonSerializerOptions { WriteIndented = true }));
                IsChanged = false;

            }
            catch (Exception e)
            {
                Log.Error($"Failed to save settings. {e.Message}");
            }
        }

        public class LogSettings
        {
            public Dictionary<string, string> LogLevel { get; set; } = new Dictionary<string, string>();
        }

        Storage Config { get; set; } = new Storage();

        public class Storage
        {
            public Dictionary<string, object> Options { get; set; } = new Dictionary<string, object>();

            public LogSettings Logging { get; set; } = new LogSettings();

            public LogLevel GetLogLevel(string name)
            {
                if (Logging.LogLevel.TryGetValue(name, out string str))
                {
                    var value = (Microsoft.Extensions.Logging.LogLevel)Enum.Parse(typeof(Microsoft.Extensions.Logging.LogLevel), str);
                    return value.ToLogLevel();
                }

                Logging.LogLevel[name] = Microsoft.Extensions.Logging.LogLevel.Information.ToString();
                return Microsoft.Extensions.Logging.LogLevel.Information.ToLogLevel();
            }

            public void SetLogLevel(string name, LogLevel value)
            {
                Logging.LogLevel[name] = value.ToLogLevel().ToString();
            }

            private object GetObject(string name, object value)
            {
                if (Options.TryGetValue(name, out object v))
                {
                    var e = (JsonElement)v;
                    switch (e.ValueKind)
                    {
                        case JsonValueKind.Undefined:
                            return null;
                        case JsonValueKind.Object:
                            return e;
                        case JsonValueKind.Array:
                            return e;
                        case JsonValueKind.String:
                            return e.GetString();
                        case JsonValueKind.Number:
                            return e.GetInt32();
                        case JsonValueKind.True:
                            return e.GetBoolean();
                        case JsonValueKind.False:
                            return e.GetBoolean();
                        case JsonValueKind.Null:
                            return null;
                        default:
                            break;
                    }
                }

                Options[name] = value;
                return value;
            }

            public void Set(string name, int value)
            {
                Options[name] = value;
            }

            public void Set(string name, string value)
            {
                Options[name] = value;
            }

            public void Set(string name, bool value)
            {
                Options[name] = value;
            }

            public int GetInt(string name, int def = default)
            {
                return (int)GetObject(name, def);
            }

            public bool GetFlag(string name, bool def = false)
            {
                return (bool)GetObject(name, def);
            }

            public string Get(string name, string def = "")
            {
                return (string)GetObject(name, def);
            }
        }

        public void Set(string name, int value)
        {
            Config.Set(name, value);
            IsChanged = true;
        }

        public void Set(string name, string value)
        {
            Config.Set(name, value);
            IsChanged = true;
        }

        public void Set(string name, bool value)
        {
            Config.Set(name, value);
            IsChanged = true;
        }

        public void Set(string name, LogLevel value)
        {
            Config.SetLogLevel(name, value);
            IsChanged = true;
        }

        public T Get<T>(string name, T def = default)
        {
            if (typeof(T) == typeof(int))
            {
                return (T)(object)Config.GetInt(name, (int)(object)def);
            }
            else if (typeof(T) == typeof(bool))
            {
                return (T)(object)Config.GetFlag(name, (bool)(object)def);
            }
            else if (typeof(T) == typeof(string))
            {
                return (T)(object)Config.Get(name, (string)(object)def);
            }
            else if (typeof(T) == typeof(LogLevel))
            {
                return (T)(object)Config.GetLogLevel(name);
            }

            return def;
        }        

        public bool IsChanged { get; set; }
    }
}
