﻿using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MaSoft.Framework
{
    public interface IProgram
    {
        string ApplicationName { get; set; }
        string Company { get; set; }
        Settings Config { get; }
        RSA Identity { get; set; }
        bool IsService { get; }

        string GetApplicationDataFolder();
        string GetLocation();
        string GetSettingsPath();
        string GetSettingsPath(bool isService);
        string GetUserDataFolder();
        Task RunAsync(string[] args);
    }
}