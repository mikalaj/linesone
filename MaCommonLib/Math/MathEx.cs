﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MaSoft.Framework.MathEx
{
    public enum Relation
    {
        START,
        END,
        INSIDE,
        FRONT,
        BACK,
        OUTSIDE,
        ON,
        INTERSECT,
        NOT_INTERSECT
    }

    public static class Extensions
    {         
        public static bool TestSphereCapsule(Sphere s, Capsule c)
        {
            var dist2 = SqDistPointSegment(c.a, c.b, s.c);
            var radius = s.r + c.r;
            return dist2 <= radius * radius;
        }

        public static bool TestSphereSphere(Sphere a, Sphere b)
        {
            var d = a.c - b.c;
            var dst = Dot(d, d);
            var radiusSum = a.r + b.r;
            return dst <= radiusSum * radiusSum;
        }

        public static bool TestAABBAABB(AABB a, AABB b)
        {
            if (a.Max.X < b.Min.X || a.Min.X > b.Max.X)
                return false;
            if (a.Max.Y < b.Min.Y || a.Min.Y > b.Max.Y)
                return false;
            if (a.Max.Z < b.Min.Z || a.Min.Z > b.Max.Z)
                return false;
            return true;
        }

        public static Plane ComputePlane(Vector3 a, Vector3 b, Vector3 c)
        {
            var normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));

            return new Plane
            {
                n = normal,
                d = Vector3.Dot(normal, a)
            };
        }

        public static Vector3 ClosestPtPointPlane(Vector3 q, Plane p)
        {
            var t = (Vector3.Dot(p.n, q) - p.d) / Vector3.Dot(p.n, p.n);
            return q - t * p.n;
        }

        public static float DistPointPlane(Vector3 q, Plane p)
        {
            return (Vector3.Dot(p.n, q) - p.d) / Vector3.Dot(p.n, p.n);
        }

        public static float Clamp(float n, float min, float max)
        {
            if (n < min) 
                return min;
            if (n > max)
                return max;
            return n;
        }

        public static float Dot(Vector3 a, Vector3 b)
        {
            return Vector3.Dot(a, b);
        }

           /// <summary>
           /// Find point such that line from p touches sphere with radius r
           /// in the direction of a to b
           /// </summary>
           /// <param name="a"></param>
           /// <param name="b"></param>
           /// <param name="p"></param>
           /// <param name="r"></param>
           /// <returns></returns>
        public static Vector3 TouchCapsule(Vector3 a, Vector3 b, Vector3 p, float r)
        {
            var t = b - a;
            var v = p - b;

            var n = Vector3.Normalize(Vector3.Cross(t, p - a));

            var alpha = (float)Math.Asin(r / v.Length());
            var k = Vector3.Normalize(Vector3.Cross(v, n));

            var x = Vector3.Transform(k, Quaternion.CreateFromAxisAngle(n, alpha));

            return b + x * r;
        }

        public static IEnumerable<Vector3> TouchPoints(List<Vector3> p, Vector3 start)
        {
            var bottom = 0;
            var top = 0;
            var n = new Vector3(0, 0, 1);            

            for (int i = 1; i < p.Count; ++i)
            {
                if (Vector3.Dot(n, Vector3.Cross(p[i] - start, p[top] - start)) > 0)
                    top = i;

                if (Vector3.Dot(n, Vector3.Cross(p[i] - start, p[bottom] - start)) < 0)
                    bottom = i;
            }

            yield return p[bottom];
            if (bottom != top)
                yield return p[top];
        }

        public static IEnumerable<Vector3> TouchSphere(Vector3 c, Vector3 n, Vector3 p, float r)
        {
            var v = p - c;

            var alpha = (float)Math.Asin(r / v.Length());
            var k = Vector3.Normalize(Vector3.Cross(v, n));
            var x = Vector3.Transform(k, Quaternion.CreateFromAxisAngle(n, alpha));

            yield return c + x * r;
            x = Vector3.Transform(k, Quaternion.CreateFromAxisAngle(n, -alpha));
            yield return c - x * r;
        }

        public static void Set(this Matrix4x4 m, int row, int col, float v)
        {
            switch (row)
            {
                case 0:
                    {
                        switch (col)
                        {
                            case 0:
                                m.M11 = v;
                                break;
                            case 1:
                                m.M12 = v;
                                break;
                            case 2:
                                m.M13 = v;
                                break;
                            case 3:
                                m.M14 = v;
                                break;
                        }
                    }
                    break;
                case 1:
                    {
                        switch (col)
                        {
                            case 0:
                                m.M21 = v;
                                break;
                            case 1:
                                m.M22 = v;
                                break;
                            case 2:
                                m.M23 = v;
                                break;
                            case 3:
                                m.M24 = v;
                                break;
                        }
                    }
                    break;
                case 2:
                    {
                        switch (col)
                        {
                            case 0:
                                m.M31 = v;
                                break;
                            case 1:
                                m.M32 = v;
                                break;
                            case 2:
                                m.M33 = v;
                                break;
                            case 3:
                                m.M34 = v;
                                break;
                        }
                    }
                    break;
                case 3:
                    {
                        switch (col)
                        {
                            case 0:
                                m.M41 = v;
                                break;
                            case 1:
                                m.M42 = v;
                                break;
                            case 2:
                                m.M43 = v;
                                break;
                            case 3:
                                m.M44 = v;
                                break;
                        }
                    }
                    break;
            }
            throw new Exception($"Index out of range {row}x{col}");
        }

        public static bool TestSegmentAABB(Vector3 p0, Vector3 p1, AABB b)
        {
            var c = (b.Min + b.Max) * 0.5f;
            var e = b.Max - c;
            var m = (p0 + p1) * 0.5f;
            var d = p1 - m;
            m = m - c;
            var adx = Math.Abs(d.X);
            if (Math.Abs(m.X) > e.X + adx)
                return false;
            var ady = Math.Abs(d.Y);
            if (Math.Abs(m.Y) > e.Y + ady)
                return false;
            var adz = Math.Abs(d.Z);
            if (Math.Abs(m.Z) > e.Z + adz)
                return false;

            adx += float.Epsilon;
            ady += float.Epsilon;
            adz += float.Epsilon;

            if (Math.Abs(m.Y * d.Z - m.Z * d.Y) > e.Y * adz + e.Z * ady)
                return false;
            if (Math.Abs(m.Z * d.X - m.X * d.Z) > e.X * adz + e.Z * adx)
                return false;
            if (Math.Abs(m.X * d.Y - m.Y * d.X) > e.X * ady + e.Y * adx)
                return false;

            return true;
        }

        public static float Get(this Vector3 m, int i)
        {
            switch (i)
            {
                case 0:
                    return m.X;
                case 1:
                    return m.Y;
                case 2:
                    return m.Z;
            }
            throw new Exception($"Index out of range {i}");
        }

        public static void Set(this Vector3 m, int i, float v)
        {
            switch (i)
            {
                case 0:
                    m.X = v;
                    break;
                case 1:
                    m.Y = v;
                    break;
                case 2:
                    m.Z = v;
                    break;
            }

            throw new Exception($"Index out of range {i}");
        }


        public static float Get(this Matrix4x4 m, int row, int col)
        {
            switch (row)
            {
                case 0:
                    {
                        switch (col)
                        {
                            case 0:
                                return m.M11;
                            case 1:
                                return m.M12;
                            case 2:
                                return m.M13;
                            case 3:
                                return m.M14;
                        }
                    }
                    break;
                case 1:
                    {
                        switch (col)
                        {
                            case 0:
                                return m.M21;
                            case 1:
                                return m.M22;
                            case 2:
                                return m.M23;
                            case 3:
                                return m.M24;
                        }
                    }
                    break;
                case 2:
                    {
                        switch (col)
                        {
                            case 0:
                                return m.M31;
                            case 1:
                                return m.M32;
                            case 2:
                                return m.M33;
                            case 3:
                                return m.M34;
                        }
                    }
                    break;
                case 3:
                    {
                        switch (col)
                        {
                            case 0:
                                return m.M41;
                            case 1:
                                return m.M42;
                            case 2:
                                return m.M43;
                            case 3:
                                return m.M44;
                        }
                    }
                    break;
            }
            throw new Exception($"Index out of range {row}x{col}");
        }

        public static Relation TestOBB(this OBB a, OBB b)
        {
            return TestOBBOBB(a, b);
        }

        public static Relation TestOBBOBB(OBB a, OBB b)
        {
            float ra, rb;
            Matrix4x4 R = Matrix4x4.Identity;
            Matrix4x4 AbsR = Matrix4x4.Identity;

            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    R.Set(i, j, Dot(a.u[i], b.u[j]));

            var t = b.c - a.c;
            t = new Vector3(Dot(t, a.u[0]), Dot(t, a.u[1]), Dot(t, a.u[2]));

            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    AbsR.Set(i, j, Math.Abs(R.Get(i, j)) + float.Epsilon);

            for (int i = 0; i < 3; ++i)
            {
                ra = a.e.Get(i);
                rb = b.e.Get(0) * AbsR.Get(i, 0) + b.e.Get(1) * AbsR.Get(i, 1) + b.e.Get(2) * AbsR.Get(i, 2);
                if (Math.Abs(t.Get(i)) > ra + rb)
                    return Relation.NOT_INTERSECT;
            }

            for (int i = 0; i < 3; ++i)
            {
                ra = a.e.Get(0) * AbsR.Get(0, i) + a.e.Get(1) * AbsR.Get(1, i) + a.e.Get(2) * AbsR.Get(2, i);
                rb = b.e.Get(i);
                if (Math.Abs(t.Get(0) * R.Get(0, i) + t.Get(1) * R.Get(1, i) + t.Get(2) * R.Get(2, i)) > ra + rb)
                    return Relation.NOT_INTERSECT;
            }

            // L = A0 x B0
            ra = a.e.Get(1) * AbsR.Get(2, 0) + a.e.Get(2) * AbsR.Get(1, 0);
            rb = b.e.Get(1) * AbsR.Get(0, 2) + b.e.Get(2) * AbsR.Get(0, 1);
            if (Math.Abs(t.Get(2) * R.Get(1, 0) - t.Get(1) * R.Get(2, 0)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A0 x B1
            ra = a.e.Get(1) * AbsR.Get(2, 1) + a.e.Get(2) * AbsR.Get(1, 1);
            rb = b.e.Get(0) * AbsR.Get(0, 2) + b.e.Get(2) * AbsR.Get(0, 0);
            if (Math.Abs(t.Get(2) * R.Get(1, 1) - t.Get(1) * R.Get(2, 1)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A0 x B2
            ra = a.e.Get(1) * AbsR.Get(2, 2) + a.e.Get(2) * AbsR.Get(1, 2);
            rb = b.e.Get(0) * AbsR.Get(0, 1) + b.e.Get(1) * AbsR.Get(0, 0);
            if (Math.Abs(t.Get(2) * R.Get(1, 2) - t.Get(1) * R.Get(2, 2)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A1 x B0
            ra = a.e.Get(0) * AbsR.Get(2, 0) + a.e.Get(2) * AbsR.Get(0, 0);
            rb = b.e.Get(1) * AbsR.Get(1, 2) + b.e.Get(2) * AbsR.Get(1, 1);
            if (Math.Abs(t.Get(0) * R.Get(2, 0) - t.Get(2) * R.Get(0, 0)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A1 x B1
            ra = a.e.Get(0) * AbsR.Get(2, 1) + a.e.Get(2) * AbsR.Get(0, 1);
            rb = b.e.Get(0) * AbsR.Get(1, 2) + b.e.Get(2) * AbsR.Get(1, 0);
            if (Math.Abs(t.Get(0) * R.Get(2, 1) - t.Get(2) * R.Get(0, 1)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A1 x B2
            ra = a.e.Get(0) * AbsR.Get(2, 2) + a.e.Get(2) * AbsR.Get(0, 2);
            rb = b.e.Get(0) * AbsR.Get(1, 1) + b.e.Get(1) * AbsR.Get(1, 0);
            if (Math.Abs(t.Get(0) * R.Get(2, 2) - t.Get(2) * R.Get(0, 2)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A2 x B0
            ra = a.e.Get(0) * AbsR.Get(1, 0) + a.e.Get(1) * AbsR.Get(0, 0);
            rb = b.e.Get(1) * AbsR.Get(2, 2) + b.e.Get(2) * AbsR.Get(2, 1);
            if (Math.Abs(t.Get(1) * R.Get(0, 0) - t.Get(0) * R.Get(1, 0)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A2 x B1
            ra = a.e.Get(0) * AbsR.Get(1, 1) + a.e.Get(1) * AbsR.Get(0, 1);
            rb = b.e.Get(0) * AbsR.Get(2, 2) + b.e.Get(2) * AbsR.Get(2, 0);
            if (Math.Abs(t.Get(1) * R.Get(0, 1) - t.Get(0) * R.Get(1, 1)) > ra + rb)
                return Relation.NOT_INTERSECT;

            // L = A2 x B1
            ra = a.e.Get(0) * AbsR.Get(1, 2) + a.e.Get(1) * AbsR.Get(0, 2);
            rb = b.e.Get(0) * AbsR.Get(2, 1) + b.e.Get(1) * AbsR.Get(2, 0);
            if (Math.Abs(t.Get(1) * R.Get(0, 2) - t.Get(0) * R.Get(1, 2)) > ra + rb)
                return Relation.NOT_INTERSECT;

            return Relation.INTERSECT;
        }

        public static float ClosestPtSegmentSegment(Vector3 p1, Vector3 q1, Vector3 p2, Vector3 q2, out float s, out float t, out Vector3 c1, out Vector3 c2)
        {
            var d1 = q1 - p1;
            var d2 = q2 - p2;
            var r = p1 - p2;
            var a = Dot(d1, d1);
            var e = Dot(d2, d2);
            var f = Dot(d2, r);

            if (a <= float.Epsilon && e <= float.Epsilon)
            {
                s = t = 0.0f;
                c1 = p1;
                c2 = p2;
                return Dot(c1 - c2, c1 - c2);
            }

            if (a <= float.Epsilon)
            {
                s = 0.0f;
                t = f / e;
                t = Clamp(t, 0.0f, 1.0f);
            }
            else
            {
                var c = Dot(d1, r);
                if (e <= float.Epsilon)
                {
                    t = 0.0f;
                    s = Clamp(-c / a, 0.0f, 1.0f);
                }
                else
                {
                    var b = Dot(d1, d2);
                    var denom = a * e - b * b;

                    if (denom != 0.0f)
                    {
                        s = Clamp((b * f - c * e) / denom, 0.0f, 1.0f);
                    }
                    else
                    {
                        s = 0.0f;
                    }

                    t = (b * s + f) / e;

                    if (t < 0.0f)
                    {
                        t = 0.0f;
                        s = Clamp(-c / a, 0.0f, 1.0f);
                    }
                    else if (t > 1.0f)
                    {
                        t = 1.0f;
                        s = Clamp((b - c) / a, 0.0f, 1.0f);
                    }
                }
            }

            c1 = p1 + d1 * s;
            c2 = p2 + d2 * t;

            return Dot(c1 - c2, c1 - c2);
        }

        public static float SqDistPointSegment(Vector3 a, Vector3 b, Vector3 c)
        {
            var ab = b - a;
            var ac = c - a;
            var bc = c - b;
            var e = Vector3.Dot(ac, ab);

            if (e <= 0.0f)
                return Vector3.Dot(ac, ac);

            var f = Vector3.Dot(ab, ab);

            if (e >= f)
                return Vector3.Dot(bc, bc);

            return Vector3.Dot(ac, ac) - e * e / f;
        }

        public static float GetDistanceToLine(Vector3 start, Vector3 end, Vector3 point)
        {
            var q = point;
            var s = start;
            var v = end - start;
            var qs = q - s;

            var qs_dot_v = Vector3.Dot(qs, v);
            var dst = (float)Math.Sqrt(Vector3.Dot(qs, qs) - qs_dot_v * qs_dot_v / v.LengthSquared());

            return dst;
        }

        public static Relation ClassifyLinePoint(Vector3 org, Vector3 dst, Vector3 p)
        {
            var dir = dst - org;
            var p_org = p - org;
            var p_dir = p - dst;

            if (p_org.LengthSquared() < float.Epsilon)
                return Relation.START;

            if (p_dir.LengthSquared() < float.Epsilon)
                return Relation.END;

            if (Vector3.Cross(p_org, p_dir).LengthSquared() < float.Epsilon)
            {
                if (Vector3.Dot(p_dir, p_org) < 0)
                    return Relation.INSIDE;

                if (Vector3.Dot(p_org, dir) < 0)
                    return Relation.FRONT;

                return Relation.BACK;
            }

            return Relation.OUTSIDE;
        }

        public static Relation CrossPlaneLine(Plane plane, Vector3 org, Vector3 dst, ref float t)
        {

            var dir = dst - org;
            var v = Vector4.Dot(new Vector4(plane.n, plane.d), new Vector4(dir, 0));
            var distance = Vector4.Dot(new Vector4(plane.n, plane.d), new Vector4(org, 1.0f));

            if (Math.Abs(v) < float.Epsilon)
            {
                if (Math.Abs(distance) < float.Epsilon)
                    return Relation.ON;

                if (distance < 0)
                    return Relation.BACK;

                if (distance > 0)
                    return Relation.FRONT;
            }

            t = -distance / v;
            return Relation.INTERSECT;
        }

        public static void GetBarycentric(Vector3 a, Vector3 b, Vector3 c, Vector3 p, ref float w0, ref float w1, ref float w2)
        {

            var r = p - a;
            var q1 = b - a;
            var q2 = c - a;
            var q1_q2 = Vector3.Dot(q1, q2);
            var r_q1 = Vector3.Dot(r, q1);
            var r_q2 = Vector3.Dot(r, q2);
            var q1_q1 = Vector3.Dot(q1, q1);
            var q2_q2 = Vector3.Dot(q2, q2);

            var inv_det = 1.0f / (q1_q1 * q2_q2 - q1_q2 * q1_q2);
            w1 = inv_det * (q2_q2 * r_q1 - q1_q2 * r_q2);
            w2 = inv_det * (-q1_q2 * r_q1 + q1_q1 * r_q2);
            w0 = 1.0f - w1 - w2;
        }

    }
}
