﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MaSoft.Framework.MathEx
{
    public struct Capsule
    {
        public Vector3 a;
        public Vector3 b;
        public float r;
    }

    public struct AABB
    {
        public Vector3 Min;
        public Vector3 Max;
    }

    public struct OBB
    {
        public Vector3 c;
        public Vector3[] u;        
        public Vector3 e;
    }

    public struct Sphere
    {
        public Vector3 c;
        public float r;
    }

    public struct Plane
    {
        public Vector3 n;
        public float d;
    }

    public abstract class BVTree
    {
        public BVTree Left;
        public BVTree Right;

        public bool IsLeaf { get => Left == null && Right == null; }
        public abstract bool Overlap(BVTree tree);
    }

    public class BVSphereTree : BVTree
    {
        public Sphere Shape;

        public override bool Overlap(BVTree tree)
        {
            if (tree is BVSphereTree sphereTree)
            {
                return Extensions.TestSphereSphere(Shape, sphereTree.Shape);
            }
            return false;
        }
    }
}
