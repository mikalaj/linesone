﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaSoft.Framework
{
    public class EnumModel<T> : NotifyPropertyChanged where T : struct
    {
        public EnumModel()
            : this(default, new DefaultEnumModelDescription<T>())
        {
        }

        public EnumModel(T value)
            : this(value, new DefaultEnumModelDescription<T>())
        {
        }

        IEnumModelDescription<T> Descriptor { get; }
        T? _value;

        public EnumModel(T value, IEnumModelDescription<T> desc)
        {
            Descriptor = desc;
            Value = value;            
        }

        private void V_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsChecked")
            {
                OnPropertyChanged(nameof(Value));
            }
        }

        public T Value
        {
            get
            {
                int res = 0;

                foreach(var v in Values)
                {
                    if (v.IsChecked)
                        res |= (int)(object)v.Value;
                }

                return (T)(object)res;
            }

            set
            {
                if (Equals(_value, value))
                    return;

                _value = value;
                Values = Enum.GetValues(typeof(T)).Cast<T>().Where(o => (int)(object)o != 0).Select(o => new EnumValue<T> { IsChecked = ((int)(object)o & (int)(object)value) == (int)(object)o, Value = o, Text = Descriptor.GetText(o) }).ToList();
                foreach (var v in Values)
                {
                    v.PropertyChanged += V_PropertyChanged;
                }
                OnPropertyChanged(nameof(Value), nameof(Values));
            }
        }

        public List<EnumValue<T>> Values { get; private set; }
    }
}
