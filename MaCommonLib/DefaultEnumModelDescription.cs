﻿namespace MaSoft.Framework
{
    public class DefaultEnumModelDescription<T> : IEnumModelDescription<T> where T : struct
    {
        public string GetText(T value)
        {
            return value.ToString();
        }
    }
}
