﻿namespace MaSoft.Framework
{
    public interface IEnumModelDescription<T> where T: struct
    {
        string GetText(T value);
    }
}
