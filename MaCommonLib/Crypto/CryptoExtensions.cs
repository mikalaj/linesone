﻿
using System.Security.Cryptography;

namespace MaSoft.Framework.Crypto
{
    public static class CryptoExtensions
    {
        public static RsaKeyParameters ToRsaKeyParameters(this RSAParameters p)
        {
            return new RsaKeyParameters
            {
                D = p.D,
                DP = p.DP,
                DQ = p.DQ,
                Exponent = p.Exponent,
                InverseQ = p.InverseQ,
                Modulus = p.Modulus,
                P = p.P,
                Q = p.Q
            };
        }

        public static RSAParameters ToRsaParameters(this RsaKeyParameters p)
        {
            return new RSAParameters
            {
                Q = p.Q,
                P = p.P,
                D = p.D,
                DP = p.DP,
                DQ = p.DQ,
                Exponent = p.Exponent,
                InverseQ = p.InverseQ,
                Modulus = p.Modulus
            };
        }
    }
}
