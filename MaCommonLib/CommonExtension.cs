﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaCommonLib
{
    public static class CommonExtension
    {
        public static string ToFormattedString(this byte[] buf)
        {
            var b = new StringBuilder();
            foreach (var v in buf)
            {
                b.AppendFormat($"{(int)v,2:x2} ");
            }

            return b.ToString();
        }
    }
}
