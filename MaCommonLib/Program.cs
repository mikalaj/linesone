﻿using MaSoft.Framework.Certificates;
using MaSoft.Framework.Crypto;
using NLog;
using NLog.Config;
using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MaSoft.Framework
{

    public abstract class Program : IProgram
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();

        public static Program Current { get; set; }

        protected Program(string company, string applicationName)
            : this(company, applicationName, true)
        {
        }

        protected Program(string company, string applicationName, bool isService)
        {
            Company = company;
            ApplicationName = applicationName;
            IsService = isService;
            Config = new Settings(GetSettingsPath(isService));
        }

        public bool IsService { get; }
        public async Task RunAsync(string[] args)
        {
            Log.Info($"Run application {string.Join(" ", args)}");

            LoadProgramIdentity();

            if (Config.IsChanged)
                Config.Save();

            LogHelper.SetLogLevel(Config.Get<LogLevel>("Default"));


            try
            {
                await OnRunAsync(args);
            }
            catch (Exception e)
            {
                Log.Error($"Run failed. {e.Message}");
            }

            Log.Info($"Complete...");
            Config.Save();
        }

        public Settings Config { get; }

        protected abstract Task OnRunAsync(string[] args);

        private void LoadProgramIdentity()
        {
            var privateKey = Config.Get(SettingNames.ProgramPrivateKey, "");
            var publicKey = Config.Get(SettingNames.ProgramPublicKey, "");

            if (!string.IsNullOrEmpty(privateKey) && !string.IsNullOrEmpty(publicKey))
            {
                Identity.ImportPrivateKeyFromPem(privateKey);
                return;
            }

            var privateKeyParameters = Config.Get(SettingNames.ProgramPrivateKeyParameters, "");
            if (!string.IsNullOrEmpty(privateKeyParameters))
            {
                var v = System.Text.Json.JsonSerializer.Deserialize<RsaKeyParameters>(privateKeyParameters);
                var p = new RSAParameters
                {
                    D = v.D,
                    DP = v.DP,
                    DQ = v.DQ,
                    Exponent = v.Exponent,
                    InverseQ = v.InverseQ,
                    Modulus = v.Modulus,
                    P = v.P,
                    Q = v.Q
                };

                Identity.ImportParameters(p);
                return;
            }

            try
            {
                try
                {
                    Config.Set(SettingNames.ProgramPrivateKey, Identity.ExportPrivateKeyToPEM());
                    Config.Set(SettingNames.ProgramPublicKey, Identity.ExportPublicKeyToPEM());
                }
                catch (Exception e)
                {
                    var p = Identity.ExportParameters(true);
                    var v = new RsaKeyParameters
                    {
                        D = p.D,
                        DP = p.DP,
                        DQ = p.DQ,
                        Exponent = p.Exponent,
                        InverseQ = p.InverseQ,
                        Modulus = p.Modulus,
                        P = p.P,
                        Q = p.Q
                    };

                    Config.Set(SettingNames.ProgramPrivateKeyParameters, System.Text.Json.JsonSerializer.Serialize(v));
                }
            }
            catch (Exception e)
            {
                Log.Error($"Failed to export identity {e.Message}");
            }
        }

        public string GetSettingsPath()
        {
            return GetSettingsPath(IsService);
        }

        public string GetSettingsPath(bool isService)
        {
            var root = isService ? GetApplicationDataFolder() : GetUserDataFolder();
            return Path.Combine(root, "config.json");
        }

        public string GetApplicationDataFolder()
        {
            string homePath = (Environment.OSVersion.Platform == PlatformID.Unix ||
                   Environment.OSVersion.Platform == PlatformID.MacOSX)
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

            var path = Path.Combine(homePath, Company, ApplicationName);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public string GetUserDataFolder()
        {
            string homePath = (Environment.OSVersion.Platform == PlatformID.Unix ||
                   Environment.OSVersion.Platform == PlatformID.MacOSX)
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            var path = Path.Combine(homePath, Company, ApplicationName);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public string GetLocation()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public RSA Identity { get; set; } = RSA.Create();
        public string ApplicationName { get; set; }
        public string Company { get; set; }
    }
}
