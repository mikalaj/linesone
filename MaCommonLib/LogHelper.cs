﻿using NLog;

namespace MaSoft.Framework
{
    public static class LogHelper
    {
        public static void SetLogLevel(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Trace:
                    SetLogLevelInternal(NLog.LogLevel.Trace);
                    break;
                case LogLevel.Debug:
                    SetLogLevelInternal(NLog.LogLevel.Debug);
                    break;
                case LogLevel.Info:
                    SetLogLevelInternal(NLog.LogLevel.Info);
                    break;
                case LogLevel.Warn:
                    SetLogLevelInternal(NLog.LogLevel.Warn);
                    break;
                case LogLevel.Error:
                    SetLogLevelInternal(NLog.LogLevel.Error);
                    break;
                case LogLevel.Fatal:
                    SetLogLevelInternal(NLog.LogLevel.Fatal);
                    break;                
            }
        }

        private static void SetLogLevelInternal(NLog.LogLevel level)
        {
            foreach (var rule in LogManager.Configuration.LoggingRules)
            {
                rule.EnableLoggingForLevel(level);
            }

            LogManager.ReconfigExistingLoggers();
        }
    }
}
