﻿using System;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;

namespace MaSoft.Framework.Certificates
{

    public class CertificateRequestWithECDsaKeyEx
    {
        public CertificateRequest Request { get; set; }
        public ECDsa Key { get; set; }
    }

    public class CertificateRequestWithRSAEx
    {
        public CertificateRequest Request { get; set; }
        public RSA Key { get; set; }
    }

    public class CertificateAuthority    
    {       

        private string SettingsPath { get => Path.Combine(Location, "settings.json"); }
        private string Location { get; }
        private string LocationClient { get => Path.Combine(Location, Oids.OidClientAuth); }
        private string LocationServer { get => Path.Combine(Location, Oids.OidServerAuth); }
        public X509Certificate2 Certificate { get; set; }
        public ECDsa KeyPair { get; set; }

        private byte[] NextSerial
        {
            get
            {
                var result = _nextSerialNumber.ToByteArray();
                _nextSerialNumber = BigInteger.Add(_nextSerialNumber, BigInteger.One);
                return result;
            }
        }

        private CertificateAuthority(string storageLocation)
        {
            Location = Path.Combine(storageLocation, "ca");

            if (!Directory.Exists(Location))
                Directory.CreateDirectory(Location);
        }

        public CertificateAuthority(string storageLocation, string name)
            : this(storageLocation)
        {
            Init(name);
        }

        void Init(string name)
        {
            _nextSerialNumber = new BigInteger(new byte[20]);
            _nextSerialNumber = BigInteger.Add(_nextSerialNumber, BigInteger.One);

            CreateCACertificate(name);

            Save();
        }

        public static CertificateAuthority GetOrCreate(string path, string name)
        {
            var auth = new CertificateAuthority(path);
            
            if (!auth.Load() || auth.Certificate.Issuer != name)
            {
                auth.Init(name);
            }

            return auth;
        }

        void Save()
        {
            var settings = new CertificateAuthoritySettings
            {
                NextSerialNumber = _nextSerialNumber.ToByteArray(),
                Certificate = Certificate.RawData,
                PrivateKey = KeyPair.ExportECPrivateKey(),
                PublicKey = KeyPair.ExportSubjectPublicKeyInfo()
            };
            
            File.WriteAllText(SettingsPath, System.Text.Json.JsonSerializer.Serialize(settings, new JsonSerializerOptions { WriteIndented = true }));
        }

        bool Load()
        {

            if (!File.Exists(SettingsPath))
                return false;

            try
            {
                var settings = JsonSerializer.Deserialize<CertificateAuthoritySettings>(File.ReadAllText(SettingsPath));
                _nextSerialNumber = new BigInteger(settings.NextSerialNumber);
                Certificate = new X509Certificate2(settings.Certificate);
                KeyPair = ECDsa.Create();
                KeyPair.ImportECPrivateKey(settings.PrivateKey, out int read);
                KeyPair.ImportSubjectPublicKeyInfo(settings.PublicKey, out read);
                

                return true;
            } 
            catch (Exception e)
            {
                return false;
            }
        }

        void CreateCACertificate(string name)
        {
            KeyPair = ECDsa.Create(); // generate asymmetric key pair            
            var req = new CertificateRequest($"cn={name}", KeyPair, HashAlgorithmName.SHA512);
            req.CertificateExtensions.Add(new X509BasicConstraintsExtension(true, false, 5, true));
            req.CertificateExtensions.Add(new X509KeyUsageExtension(X509KeyUsageFlags.KeyCertSign | X509KeyUsageFlags.CrlSign, true));
            Certificate = req.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(5));
            File.WriteAllText(Path.Combine(Location, "ca.pem"), Certificate.ExportToPEM());
        }        

        CertificateRequest Parse(byte[] data)
        {
            return Asn1Parser.Default.ParseCertificateRequest(data);
        }

        bool ValidateCertificate(X509Certificate2 cert)
        {
            return false;
        }

        public X509Certificate2 GetClientCertificate(byte[] data)
        {
            return GetClientCertificate(data, DateTimeOffset.Now, DateTimeOffset.Now.AddDays(100));
        }

        public X509Certificate2 GetServerCertificate(byte[] data)
        {
            return GetServerCertificate(data, DateTimeOffset.Now, DateTimeOffset.Now.AddDays(100));
        }

        public X509Certificate2 GetClientCertificate(byte[] data, DateTimeOffset start, DateTimeOffset end)
        {
            var certificateRequest = Parse(data);
            if (TryLoadClientCertificate(certificateRequest.SubjectName, out X509Certificate2 cert))
            {                
                if (ValidateCertificate(cert))
                    return cert;
            }
            
            var newCertificate = certificateRequest.Create(Certificate, start, end,  NextSerial);
            SaveClientCertificate(newCertificate);

            return newCertificate;
        }

        public X509Certificate2 GetServerCertificate(byte[] data, DateTimeOffset start, DateTimeOffset end)
        {
            var certificateRequest = Parse(data);
            if (TryLoadServerCertificate(certificateRequest.SubjectName, out X509Certificate2 cert))
            {
                if (ValidateCertificate(cert))
                    return cert;
            }

            if (certificateRequest.PublicKey.Oid.Value == Oids.OidRsaEncryption)
            {                                
                var newCertificate = certificateRequest.Create(Certificate.IssuerName, X509SignatureGenerator.CreateForECDsa(KeyPair), start, end, NextSerial);
                SaveServerCertificate(newCertificate);
                return newCertificate;
            }
            else
            {
                var newCertificate = certificateRequest.Create(Certificate, start, end, NextSerial);
                SaveServerCertificate(newCertificate);
                return newCertificate;
            }
        }

        private void SaveClientCertificate(X509Certificate2 newCertificate)
        {
            if (!Directory.Exists(LocationClient))
                Directory.CreateDirectory(LocationClient);

            var certpath = Path.Combine(LocationClient, newCertificate.SubjectName.Name.Substring(3) + ".pem");
            File.WriteAllText(certpath, newCertificate.ExportToPEM());
        }

        private void SaveServerCertificate(X509Certificate2 newCertificate)
        {
            if (!Directory.Exists(LocationServer))
                Directory.CreateDirectory(LocationServer);

            var certpath = Path.Combine(LocationServer, newCertificate.SubjectName.Name.Substring(3) + ".pem");
            File.WriteAllText(certpath, newCertificate.ExportToPEM());
        }

        private bool TryLoadClientCertificate(X500DistinguishedName subjectName, out X509Certificate2 cert)
        {
            cert = null;
            try
            {
                var certpath = Path.Combine(LocationClient, subjectName.Name.Substring(3) + ".pem");
                var keypath = Path.Combine(LocationClient, subjectName.Name.Substring(3) + "_privatekey.pem");

                if (File.Exists(certpath) && File.Exists(keypath))
                {
                    var certtext = File.ReadAllText(certpath);
                    var keytext = File.ReadAllText(keypath);
                    cert = CertificateExtension.CreateFromPem(certtext, keytext);
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool TryLoadServerCertificate(X500DistinguishedName subjectName, out X509Certificate2 cert)
        {
            cert = null;
            try
            {
                var certpath = Path.Combine(LocationServer, subjectName.Name.Substring(3) + ".pem");
                var keypath = Path.Combine(LocationServer, subjectName.Name.Substring(3) + "_privatekey.pem");

                if (File.Exists(certpath) && File.Exists(keypath))
                {
                    var certtext = File.ReadAllText(certpath);
                    var keytext = File.ReadAllText(keypath);
                    cert = CertificateExtension.CreateFromPem(certtext, keytext);
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public X509Certificate2 GetClientCertificate(string name, bool createIfNoExist)
        {
            throw new NotImplementedException();
            //var ecdsa = ECDsa.Create(); // generate asymmetric key pair       
            
            //var parameters = ecdsa.ExportParameters(true);
            ////var dd = new AsnEncodedData(parameters.Curve.Oid, new byte[0]);            
            ////var node = new Asn1ObjectIdentifier(parameters.Curve.Oid.Value);
            ////Console.WriteLine($"curveOid: {ByteArrayToString(node.GetBytes())}");
            ////{
            ////    var r = new Asn1Sequence();

            ////    var curvNode = new Asn1Sequence();
            ////    curvNode.Nodes.Add(new Asn1ObjectIdentifier("1.2.840.10045.2.1"));
            ////    curvNode.Nodes.Add(new Asn1ObjectIdentifier(parameters.Curve.Oid.Value));

            ////    r.Nodes.Add(curvNode);


            ////    var bits = new Asn1BitString(new byte[] { 4 }.Concat(parameters.Q.X).Concat(parameters.Q.Y).ToArray());
            ////    r.Nodes.Add(bits);

            ////    var b = r.GetBytes();
            ////    Console.WriteLine($"pubKeySelf: {ByteArrayToString(r.GetBytes())}");
            ////    var tmp = new TraverseContext();
            ////    TraverseNodes(r, tmp);
            ////}
            
            //// var pubKey = ecdsa.ExportSubjectPublicKeyInfo();            

            ////Console.WriteLine($"parameters.Q.X: {new AsnEncodedData(parameters.Q.X).Format(false)}");
            ////Console.WriteLine($"parameters.Q.Y: {new AsnEncodedData(parameters.Q.Y).Format(false)}");
            ////Console.WriteLine($"pubKey: {ByteArrayToString(pubKey)}");
            ////Console.WriteLine($"oid: {ByteArrayToString(dd.RawData)}");

            ////{
            ////    var tmp = new TraverseContext();
            ////    TraverseNodes(Asn1Node.ReadNode(pubKey), tmp);
            ////}

            //var req = new CertificateRequest($"cn={name}", ecdsa, HashAlgorithmName.SHA256);
            //var keyUsageExt = new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, false);

            //req.CertificateExtensions.Add(keyUsageExt);
            //var myOid = new Oid("1.3.6.1.4.1.1.4.1");
            //var v = new OidCollection();
            //v.Add(new Oid("1.3.6.1.5.5.7.3.2"));
            //v.Add(myOid);

            //var keyEncUsageExt = new X509EnhancedKeyUsageExtension(v, false);
            //req.CertificateExtensions.Add(keyEncUsageExt);
            //req.CertificateExtensions.Add(new X509Extension(myOid, System.Text.Encoding.UTF8.GetBytes("This is my data"), false));
            
            //var  bytes = req.CreateSigningRequest();
            //Console.WriteLine($"Raw Request: {ByteArrayToString(bytes)}");

            //var root = Asn1Node.ReadNode(bytes);            
            //var ctx = new TraverseContext { Display = true };
            //TraverseNodes(root, ctx);

            ////Console.WriteLine($"EncodedKeyValue: {ByteArrayToString(req.PublicKey.EncodedKeyValue.RawData)}");
            ////Console.WriteLine($"EncodedParameters: {ByteArrayToString(req.PublicKey.EncodedParameters.RawData)}");
            
            //{
                
            //   // var newecdsa = ECDsa.Create(); // generate asymmetric key pair   
            //    // var p = new ECParameters { Curve = new ECCurve(), Q = new ECPoint { X = ctx.X, Y = ctx.Y } };
            //    // newecdsa.ImportSubjectPublicKeyInfo(p);                
            //    var na = new X500DistinguishedName($"cn={ctx.CommonName}");                
            //    var newReq = new CertificateRequest(na, ctx.PublicKey.ToPublicKey(), ctx.HashAlgorithm);
            //    foreach (var ext in ctx.Extensions)
            //    {
            //        newReq.CertificateExtensions.Add(ext);
            //    }

            //    CreateCACertificate("ca");
            //    var newBytes = newReq.Create(Certificate, DateTimeOffset.Now, DateTimeOffset.Now.AddDays(100), NextSerial);                
            //    Console.WriteLine($"Raw Request: {ByteArrayToString(bytes)}");
            //}
            //return null;
            //return req.Create(ca, DateTime.Now, DateTime.Now.AddYears(5), CreateSerialNumber());
        }        

        private BigInteger _nextSerialNumber;        
    }
}
