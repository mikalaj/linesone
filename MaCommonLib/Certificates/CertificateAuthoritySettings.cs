﻿namespace MaSoft.Framework.Certificates
{
    public class CertificateAuthoritySettings
    {
        public byte[] NextSerialNumber { get; set; }
        public byte[] Certificate { get; set; }
        public byte[] PublicKey { get; set; }
        public byte[] PrivateKey { get; set; }
    }
}
