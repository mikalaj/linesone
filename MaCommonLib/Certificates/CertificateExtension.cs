﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace MaSoft.Framework.Certificates
{
    public static class CertificateExtension
    {
        //public static X509Certificate2 CreateFromPem(string cert, string key) 
        //{
        //    // loaded PEM file with labels stripped. Full value omitted for brevity
        //    //var certPem = "MIIB3zCCAYWgAwIBAgIUImttQCULqkHxYbDivb...FwT3WZO4S5JB5jvPg9hCnlXPjNwaC";

        //    //var cert = new X509Certificate2(Convert.FromBase64String(certPem));

        //    //// can be combined with the private key from the previous section 
        //    //var certWithKey = cert.CopyWithPrivateKey(key);

        //    return new X509Certificate2();
        //}

        public static string ExportPrivateKeyToPEM(this ECDsa key)
        {
            StringBuilder builder = new StringBuilder();

            var keyBlob = key.ExportECPrivateKey();
            builder.AppendLine("-----BEGIN EC PRIVATE KEY-----");
            builder.AppendLine(Convert.ToBase64String(keyBlob, Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END EC PRIVATE KEY-----");

            return builder.ToString();
        }

        public static string ExportPrivateKeyToPEM(this RSA key)
        {
            StringBuilder builder = new StringBuilder();

            var keyBlob = key.ExportRSAPrivateKey();
            builder.AppendLine("-----BEGIN RSA PRIVATE KEY-----");
            builder.AppendLine(Convert.ToBase64String(keyBlob, Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END RSA PRIVATE KEY-----");
            return builder.ToString();
        }

        public static string ExportPublicKeyToPEM(this ECDsa key)
        {
            StringBuilder builder = new StringBuilder();

            var keyBlob = key.ExportSubjectPublicKeyInfo();
            builder.AppendLine("-----BEGIN EC PUBLIC KEY-----");
            builder.AppendLine(Convert.ToBase64String(keyBlob, Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END EC PUBLIC KEY-----");

            return builder.ToString();
        }

        public static string ExportPublicKeyToPEM(this RSA key)
        {
            StringBuilder builder = new StringBuilder();

            var keyBlob = key.ExportRSAPublicKey();
            builder.AppendLine("-----BEGIN RSA PUBLIC KEY-----");
            builder.AppendLine(Convert.ToBase64String(keyBlob, Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END RSA PUBLIC KEY-----");

            return builder.ToString();
        }

        public static string ExportToPEM(this X509Certificate2 cert)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("-----BEGIN CERTIFICATE-----");
            builder.AppendLine(Convert.ToBase64String(cert.Export(X509ContentType.Pkcs12), Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END CERTIFICATE-----");

            return builder.ToString();
        }

        public static string ExportWithoutPrivateKeyToPEM(this X509Certificate2 cert)
        {
            StringBuilder builder = new StringBuilder();

            if (cert.HasPrivateKey)
            {
                cert = new X509Certificate2(cert.Export(X509ContentType.Cert));
            }

            builder.AppendLine("-----BEGIN CERTIFICATE-----");
            builder.AppendLine(Convert.ToBase64String(cert.Export(X509ContentType.Pkcs12), Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END CERTIFICATE-----");

            return builder.ToString();
        }

        public static X509Certificate2 ImportFromFilePem(string path)
        {
            var text = File.ReadAllText(path);
            return ImportFromPem(text);            
        }

        public static X509Certificate2 ImportFromFilePemWithECDsaPrivateKey(string certpath, string keypath)
        {
            var cert = ImportFromFilePem(certpath);

            var key = ECDsa.Create();
            key.ImportPrivateKeyFromPem(File.ReadAllText(keypath));

            return cert.CopyWithPrivateKey(key);            
        }

        public static X509Certificate2 ImportFromFilePemWithRsaPrivateKey(string certpath, string keypath)
        {
            var cert = ImportFromFilePem(certpath);

            var key = RSA.Create();
            key.ImportPrivateKeyFromPem(File.ReadAllText(keypath));

            return cert.CopyWithPrivateKey(key);
        }

        public static X509Certificate2 ImportFromPemWithECDsaPrivateKey(string certPem, string keyPem)
        {
            var cert = ImportFromPem(certPem);

            var key = ECDsa.Create();
            key.ImportPrivateKeyFromPem(keyPem);

            var certWithKey = cert.CopyWithPrivateKey(key);

            return new X509Certificate2(certWithKey.Export(X509ContentType.Pfx, "12345678"), "12345678");
        }

        public static X509Certificate2 ImportFromPemWithRsaPrivateKey(string certPem, string keyPem)
        {
            var cert = ImportFromPem(certPem);

            var key = RSA.Create();
            key.ImportPrivateKeyFromPem(keyPem);

            var certWithKey = cert.CopyWithPrivateKey(key);

            return new X509Certificate2(certWithKey.Export(X509ContentType.Pfx, "12345678"), "12345678");
        }

        public static X509Certificate2 ImportFromPem(string text)
        {
            var reg = new Regex("-----BEGIN.*?-----(.*)-----END.*?-----", RegexOptions.Singleline);

            var match = reg.Match(text);

            if (match.Success)
            {
                var value = match.Groups[1].Value.Trim();
                value = value.Replace("\r\n", "");
                var blob = Convert.FromBase64String(value);
                return new X509Certificate2(blob, string.Empty);
            }

            throw new Exception("Invalid PEM certificate");
        }

        public static X509Certificate ImportFromPem1(string text)
        {
            var reg = new Regex("-----BEGIN.*?-----(.*)-----END.*?-----", RegexOptions.Singleline);

            var match = reg.Match(text);

            if (match.Success)
            {
                var value = match.Groups[1].Value.Trim();
                value = value.Replace("\r\n", "");
                var blob = Convert.FromBase64String(value);
                return new X509Certificate(blob, string.Empty);
            }

            throw new Exception("Invalid PEM certificate");
        }

        public static void ImportPrivateKeyFromPem(this ECDsa self, string text)
        {
            var reg = new Regex("-----BEGIN EC PRIVATE KEY-----(.*)-----END EC PRIVATE KEY-----", RegexOptions.Singleline);

            var match = reg.Match(text);

            if (match.Success)
            {
                var blob = Convert.FromBase64String(match.Groups[1].Value);                
                self.ImportECPrivateKey(blob, out int _);
                return;
            }

            throw new Exception("Invalid ECDsa PEM private");
        }

        public static void ImportPrivateKeyFromPem(this RSA self, string text)
        {
            var reg = new Regex("-----BEGIN RSA PRIVATE KEY-----(.*)-----END RSA PRIVATE KEY-----", RegexOptions.Singleline);

            var match = reg.Match(text);

            if (match.Success)
            {
                var blob = Convert.FromBase64String(match.Groups[1].Value);
                self.ImportRSAPrivateKey(blob, out int _);
                return;
            }

            throw new Exception("Invalid RSA PEM private");
        }

        public static void ImportPublicKeyFromPem(this RSA self, string text)
        {
            var reg = new Regex("-----BEGIN RSA PUBLIC KEY-----(.*)-----END RSA PUBLIC KEY-----", RegexOptions.Singleline);

            var match = reg.Match(text);

            if (match.Success)
            {
                var blob = Convert.FromBase64String(match.Groups[1].Value);
                self.ImportRSAPublicKey(blob, out int _);
                return;
            }

            throw new Exception("Invalid RSA PEM public");
        }

        public static AsymmetricAlgorithm ImportPrivateKey(string keytext)
        {
            if (keytext.Contains("-----BEGIN RSA PRIVATE KEY-----"))
            {
                var key = RSA.Create();
                key.ImportPrivateKeyFromPem(keytext);
                return key;
            }

            if (keytext.Contains("-----BEGIN EC PRIVATE KEY-----"))
            {
                var key = ECDsa.Create();
                key.ImportPrivateKeyFromPem(keytext);
                return key;
            }

            throw new Exception($"Can't import private key from {keytext}");
        }

        internal static X509Certificate2 CreateFromPem(string certtext, string keytext)
        {
            var cert = ImportFromPem(certtext);
            var key = ImportPrivateKey(keytext);

            if (key is RSA rsa)
            {
                return new X509Certificate2(cert.CopyWithPrivateKey(rsa).Export(X509ContentType.Pfx, "1234"), "1234");
            }
            else if (key is ECDsa ecdsa)
            {
                return new X509Certificate2(cert.CopyWithPrivateKey(ecdsa).Export(X509ContentType.Pfx, "1234"), "1234");
            }

            throw new Exception("Can't create from pem");
        }
    }
}
