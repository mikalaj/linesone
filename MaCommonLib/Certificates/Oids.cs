﻿namespace MaSoft.Framework.Certificates
{
    public class Oids
    {
        public const string OidClientAuth = "1.3.6.1.5.5.7.3.2";
        public const string OidServerAuth = "1.3.6.1.5.5.7.3.1";
        public const string OidecPublicKey = "1.2.840.10045.2.1";
        public const string Oidsecp521r1 = "1.3.132.0.35";
        public const string OidExtensionReq = "1.2.840.113549.1.9.14";
        public const string OidCn = "2.5.4.3";
        public const string OidCeKeyUsage = "2.5.29.15";
        public const string OidCeKeyExtUsage = "2.5.29.37";
        public const string OidEcdsaWithSHA256 = "1.2.840.10045.4.3.2";
        public const string OidRsaEncryption = "1.2.840.113549.1.1.1";
        public const string OidSha256WithRSAEncryption = "1.2.840.113549.1.1.11";
        public const string OidRsassaPss = "1.2.840.113549.1.1.10";
        public const string OidSha512 = "2.16.840.1.101.3.4.2.3";
        public const string OidRsagf1 = "1.2.840.113549.1.1.8";
        public const string OidSha512WithRSAEncryption = "1.2.840.113549.1.1.13";
    }
}
