﻿using PeNet.Asn1;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MaSoft.Framework.Certificates
{
    public class Asn1Parser
    {
        static string ByteArrayToString(byte[] buf)
        {
            var b = new StringBuilder();
            foreach (var v in buf)
            {
                b.AppendFormat($"{(int)v,2:x2} ");
            }
            return b.ToString();
        }

        public static Asn1Parser Default { get; }

        static Asn1Parser()
        {
            Default = new Asn1Parser();
        }

        string GetOidFriendlyName(string oid)
        {
            switch (oid)
            {
                case Oids.OidCn:
                    return "cn";
                case Oids.OidecPublicKey:
                    return "id-ecPublicKey";
                case Oids.Oidsecp521r1:
                    return "secp521r1";
                case Oids.OidExtensionReq:
                    return "id-ExtensionReq";
                case Oids.OidCeKeyUsage:
                    return "id-ce-keyUsage";
                case Oids.OidCeKeyExtUsage:
                    return "id-ce-extKeyUsage";
                case Oids.OidEcdsaWithSHA256:
                    return "ecdsa-with-SHA256";
                case Oids.OidRsaEncryption:
                    return "rsaEncryption";
                case Oids.OidSha256WithRSAEncryption:
                    return "sha256WithRSAEncryption";
                case Oids.OidRsassaPss:
                    return "rsassa-pss";
                case Oids.OidSha512:
                    return "sha512";
                case Oids.OidRsagf1:
                    return "id-mgf1";
                case Oids.OidSha512WithRSAEncryption:
                    return "sha512WithRSAEncryption";
                default:
                    return oid;
            }
        }

        public CertificateRequest ParseCertificateRequest(byte[] data)
        {
            var root = Asn1Node.ReadNode(data);
            var ctx = new TraverseContext { Display = true };
            TraverseNodes(root, ctx);
            var distinguishedName = new X500DistinguishedName($"cn={ctx.CommonName}");
            CertificateRequest certificateRequest;
            if (ctx.PublicKey.Oid == Oids.OidRsaEncryption)
            {
                var rsa = RSA.Create();
                rsa.ImportRSAPublicKey(ctx.PublicKey.Key, out int _);
                certificateRequest = new CertificateRequest(distinguishedName, rsa, ctx.HashAlgorithm, RSASignaturePadding.Pkcs1);
            }
            else
            {
                certificateRequest = new CertificateRequest(distinguishedName, ctx.PublicKey.ToPublicKey(), ctx.HashAlgorithm);
            }
            
            foreach (var ext in ctx.Extensions)
            {
                certificateRequest.CertificateExtensions.Add(ext);
            }
            return certificateRequest;
        }

        internal class PublicKeyData
        {
            public string Oid { get; set; }
            public byte[] Parameters { get; set; }
            public byte[] Key { get; set; }

            public PublicKey ToPublicKey()
            {
                if (Oid == Oids.OidRsaEncryption)
                {
                    var key=  new PublicKey(new Oid(Oid), new AsnEncodedData(new byte[] { 5, 0 }), new AsnEncodedData(Key));
                    Console.WriteLine($"Oid {key.Oid.Value}");
                    Console.WriteLine($"EncodedKey {ByteArrayToString(key.EncodedKeyValue.RawData)}");
                    Console.WriteLine($"EncodedParams {ByteArrayToString(key.EncodedParameters.RawData)}");
                    return key;
                }
                else
                {
                    return new PublicKey(new Oid(Oid), new AsnEncodedData(Parameters), new AsnEncodedData(Key));
                }
            }
        }

        void TraverseNodes(Asn1Node root, TraverseContext ctx)
        {
            if (ctx.Display)
                Console.Write(new string(' ', ctx.Offset * 2));

            switch (root)
            {
                case Asn1Boolean boolNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Boolean: {boolNode.Value}");
                    break;
                case Asn1Integer intNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Boolean: {intNode.ToUInt64()}");
                    break;
                case Asn1BitString bitStringNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1BitString: {ByteArrayToString(bitStringNode.Data)}");

                    if (ctx.LastOid == Oids.Oidsecp521r1)
                    {
                        ctx.PublicKey.Key = bitStringNode.Data;

                        //if (bitStringNode.Data[0] != 4)
                        //    throw new Exception($"Unexpected {ctx.LastOid} curve parameters.");
                        //ctx.X = bitStringNode.Data.Skip(1).Take(66).ToArray();
                        //ctx.Y = bitStringNode.Data.Skip(67).ToArray();

                        //if (ctx.Display)
                        //{
                        //    Console.Write(new string(' ', ctx.Offset * 2));
                        //    Console.WriteLine($"{ctx.LastOid} X : {ByteArrayToString(ctx.X)}");

                        //    Console.Write(new string(' ', ctx.Offset * 2));
                        //    Console.WriteLine($"{ctx.LastOid} Y : {ByteArrayToString(ctx.Y)}");
                        //}
                    }
                    
                    if (ctx.LastOid == Oids.OidRsaEncryption)
                    {
                        ctx.PublicKey.Oid = Oids.OidRsaEncryption;
                        ctx.PublicKey.Key = bitStringNode.Data;
                    }

                    if (ctx.LastOid == Oids.OidEcdsaWithSHA256)
                    {
                        ctx.Hash = bitStringNode.Data;
                    }

                    if (ctx.LastOid == Oids.OidSha256WithRSAEncryption)
                    {
                        ctx.Hash = bitStringNode.Data;
                    }

                    if (ctx.LastOid == Oids.OidSha512WithRSAEncryption)
                    {
                        ctx.Hash = bitStringNode.Data;
                    }

                    if (ctx.LastOid == Oids.OidSha512)
                    {
                        ctx.Hash = bitStringNode.Data;
                    }

                    break;
                case Asn1OctetString octetStringNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1OctetString: {new AsnEncodedData(octetStringNode.Data).Format(true)} Text: {Encoding.UTF8.GetString(octetStringNode.Data)}");
                    if (ctx.ReadingExtensions)
                    {
                        if (ctx.LastOid == Oids.OidCeKeyUsage)
                        {
                            ctx.KeyUsageExtension = new X509KeyUsageExtension(new AsnEncodedData(octetStringNode.Data), false);
                            ctx.Extensions.Add(ctx.KeyUsageExtension);
                        }
                        else if (ctx.LastOid == Oids.OidCeKeyExtUsage)
                        {
                            ctx.EnhancedKeyUsageExtension = new X509EnhancedKeyUsageExtension(new AsnEncodedData(octetStringNode.Data), false);
                            ctx.Extensions.Add(ctx.EnhancedKeyUsageExtension);
                        }
                        else
                        {
                            ctx.Extensions.Add(new X509Extension(new Oid(ctx.LastOid), octetStringNode.Data, false));
                        }
                    }

                    break;
                case Asn1Null nullNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Null: {nullNode}");
                    break;
                case Asn1ObjectIdentifier oidNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1ObjectIdentifier: {GetOidFriendlyName(oidNode.Value)} ({oidNode.Value})");

                    if (oidNode.Value == Oids.OidecPublicKey)
                    {
                        ctx.PublicKey.Oid = oidNode.Value;
                    }

                    if (ctx.LastOid == Oids.OidecPublicKey)
                    {
                        ctx.Curve = oidNode.Value;
                        ctx.PublicKey.Parameters = oidNode.GetBytes();
                    }
                    if (oidNode.Value == Oids.OidEcdsaWithSHA256)
                    {
                        ctx.HashAlgorithm = HashAlgorithmName.SHA256;
                    }

                    if (oidNode.Value == Oids.OidSha256WithRSAEncryption)
                    {
                        ctx.HashAlgorithm = HashAlgorithmName.SHA256;
                    }

                    if (oidNode.Value == Oids.OidSha512WithRSAEncryption)
                    {
                        ctx.HashAlgorithm = HashAlgorithmName.SHA512;
                    }

                    if (oidNode.Value == Oids.OidSha512)
                    {
                        ctx.HashAlgorithm = HashAlgorithmName.SHA512;
                    }

                    ctx.LastOid = oidNode.Value;
                    break;
                case Asn1Utf8String utf8Node:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Utf8String: {utf8Node.Value}");
                    break;
                case Asn1NumericString numStringNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1NumericString: {numStringNode.Value}");
                    break;
                case Asn1PrintableString printNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1PrintableString: {printNode.Value}");

                    if (ctx.LastOid == Oids.OidCn)
                    {
                        ctx.CommonName = printNode.Value;
                    }

                    break;
                case Asn1Ia5String ia5Node:
                    Console.WriteLine($"Asn1Ia5String: {ia5Node.Value}");
                    break;
                case Asn1UtcTime timeNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1UtcTime: {timeNode.Value}");
                    break;
                case Asn1Sequence seqNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Sequence: {seqNode.Nodes.Count} nodes");
                    foreach (var node in seqNode.Nodes)
                    {
                        ctx.Offset++;
                        TraverseNodes(node, ctx);
                        ctx.Offset--;
                    }
                    break;
                case Asn1Set setNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Set: {setNode.Nodes.Count} nodes");
                    if (ctx.LastOid == Oids.OidExtensionReq)
                        ctx.ReadingExtensions = true;

                    foreach (var node in setNode.Nodes)
                    {
                        ctx.Offset++;
                        TraverseNodes(node, ctx);
                        ctx.Offset--;
                    }

                    ctx.ReadingExtensions = false;
                    break;
                case Asn1CustomNode customNode:
                    if (ctx.Display)
                        Console.WriteLine($"Asn1Set: {customNode.Nodes.Count} nodes");
                    foreach (var node in customNode.Nodes)
                    {
                        ctx.Offset++;
                        TraverseNodes(node, ctx);
                        ctx.Offset--;
                    }
                    break;
                default:
                    throw new Exception($"Unexpected node {root.GetType()}");
            }
        }

        class TraverseContext
        {
            public bool Display { get; set; } = false;
            public int Offset { get; set; }

            public string Curve { get; set; }
            public PublicKeyData PublicKey { get; set; } = new PublicKeyData();
            public string CommonName { get; set; }
            public bool ReadingExtensions { get; set; }
            public byte[] Hash { get; set; }
            public HashAlgorithmName HashAlgorithm { get; set; }
            public X509KeyUsageExtension KeyUsageExtension { get; set; }
            public X509EnhancedKeyUsageExtension EnhancedKeyUsageExtension { get; set; }
            public List<X509Extension> Extensions { get; set; } = new List<X509Extension>();
            public string LastOid { get; set; }
        }
    }
}
