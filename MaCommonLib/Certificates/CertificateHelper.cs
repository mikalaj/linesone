﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MaSoft.Framework.Certificates
{
    public static class CertificateHelper
    {
        public static string ExportCertificateToPEM(this X509Certificate cert)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("-----BEGIN CERTIFICATE-----");
            builder.AppendLine(Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END CERTIFICATE-----");

            return builder.ToString();
        }        

        public static CertificateRequestWithECDsaKeyEx CreateClientCertificateRequest(string name)
        {
            var ecdsa = ECDsa.Create();

            var req = new CertificateRequest($"cn={name}", ecdsa, HashAlgorithmName.SHA256);
            var keyUsageExt = new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, false);

            req.CertificateExtensions.Add(keyUsageExt);
            var myOid = new Oid("1.3.6.1.4.1.1.4.1");
            var v = new OidCollection();
            v.Add(new Oid("1.3.6.1.5.5.7.3.2"));
            v.Add(myOid);

            var keyEncUsageExt = new X509EnhancedKeyUsageExtension(v, false);
            req.CertificateExtensions.Add(keyEncUsageExt);
            req.CertificateExtensions.Add(new X509Extension(myOid, System.Text.Encoding.UTF8.GetBytes("This is my data"), false));

            return new CertificateRequestWithECDsaKeyEx
            {
                Key = ecdsa,
                Request = req
            };
        }

        public static CertificateRequestWithECDsaKeyEx CreateServerCertificateRequestWithECDsaPrivate(string name)
        {
            var ecdsa = ECDsa.Create();

            var req = new CertificateRequest($"cn={name}", ecdsa, HashAlgorithmName.SHA256);
            var keyUsageExt = new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, false);

            req.CertificateExtensions.Add(keyUsageExt);            
            var v = new OidCollection();
            v.Add(new Oid(Oids.OidServerAuth));            

            var keyEncUsageExt = new X509EnhancedKeyUsageExtension(v, false);
            req.CertificateExtensions.Add(keyEncUsageExt);

            return new CertificateRequestWithECDsaKeyEx
            {
                Key = ecdsa,
                Request = req
            };
        }

        public static CertificateRequestWithRSAEx CreateServerCertificateRequestWithRSA(string name)
        {
            return CreateServerCertificateRequestWithRSA(name, 2048);
        }

        public static CertificateRequestWithRSAEx CreateServerCertificateRequestWithRSA(string name, int keysize)
        {
            var ecdsa = RSA.Create(keysize);

            var req = new CertificateRequest($"cn={name}", ecdsa, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            var keyUsageExt = new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.KeyEncipherment, false);

            req.CertificateExtensions.Add(keyUsageExt);
            var v = new OidCollection();
            v.Add(new Oid(Oids.OidServerAuth));

            var keyEncUsageExt = new X509EnhancedKeyUsageExtension(v, false);
            req.CertificateExtensions.Add(keyEncUsageExt);

            var p = req.PublicKey;

            return new CertificateRequestWithRSAEx
            {
                Key = ecdsa,
                Request = req
            };
        }
    }
}
