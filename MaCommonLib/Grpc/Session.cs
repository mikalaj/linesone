﻿using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MaSoft.Framework.Grpc
{

    public interface ICreateSessionRequest
    {
        string FriendlyName { get; }
        string Password { get; }
    }

    public interface IListSessionsRequest
    {
        string Filter { get; }
    }

    public interface IUser
    {
        string Id { get; }
        IEnumerable<ISession> Sessions { get; }
        void AddSession(ISession session);
        void RemoveSession(ISession session);
        IAsyncEnumerable<ISessionCommand> GetOutput();
        Task<ISessionCommand> WaitOutput();
        void Execute(ISessionCommand sessionCommand);
        void Emit(ISessionCommand sessionCommand);
    }

    public abstract class User : IUser
    {
        public string Id { get; set; }

        public void Execute(ISessionCommand sessionCommand)
        {
            OnExecute(sessionCommand);
        }

        protected abstract void OnExecute(ISessionCommand cmd);

        public async IAsyncEnumerable<ISessionCommand> GetOutput()
        {
            while (true)
            {                       
                yield return await WaitOutput();
            }
        }        

        public Task<ISessionCommand> WaitOutput()
        {
            return Task.Run(() => _output.Take());
        }

        public void Emit(ISessionCommand sessionCommand)
        {
            if (sessionCommand == null)
            {
                _output.CompleteAdding();
            }
            else
            {
                _output.Add(sessionCommand);
            }
        }

        public void AddSession(ISession session)
        {
            _sessions.TryAdd(session.Id, session);
        }

        public void RemoveSession(ISession session)
        {
            _sessions.TryRemove(session.Id, out ISession _);
        }

        public ISession FindSession(string id)
        {
            if (_sessions.TryGetValue(id, out ISession session))
                return session;
            return null;
        }

        public IEnumerable<ISession> Sessions { get => _sessions.Values; }

        ConcurrentDictionary<string, ISession> _sessions = new ConcurrentDictionary<string, ISession>();
        BlockingCollection<ISessionCommand> _output = new BlockingCollection<ISessionCommand>();
    }

    public interface ISessionManager
    {
        ISession CreateSession(ICreateSessionRequest options);
        IEnumerable<ISession> ListSessions(IListSessionsRequest options);
        void JoinSession(string sessionName, string password, IUser user);
        void LeaveSession(string sessionName, IUser user);
        ISession FindSession(string sessionId);
        IUser FindUser(string id);
    }

    public abstract class SessionManager : ISessionManager
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        ConcurrentDictionary<string, ISession> _sessions = new ConcurrentDictionary<string, ISession>();
        ConcurrentDictionary<string, IUser> _users = new ConcurrentDictionary<string, IUser>();

        protected abstract ISession CreateSessionInternal(string id, ICreateSessionRequest options);

        public ISession CreateSession(ICreateSessionRequest options)
        {
            var id = Guid.NewGuid().ToString();
            return _sessions.GetOrAdd(id, (id) => CreateSessionInternal(id, options));
        }

        public void JoinSession(string sessionName, string password, IUser user)
        {
            if (_sessions.TryGetValue(sessionName, out ISession session))
            {
                if (session.Password != password)
                {
                    throw new Exception($"{session.FriendlyName}: Member {user.Id} password is not valid");
                }

                session.Add(user);
                user.AddSession(session);
            }
            else
            {
                throw new Exception($"{sessionName}: Session doesn't exist");
            }
        }

        public void LeaveSession(string sessionName, IUser member)
        {
            if (_sessions.TryGetValue(sessionName, out ISession session))
            {
                session.Leave(member);
                member.RemoveSession(session);
            }
        }

        public IEnumerable<ISession> ListSessions(IListSessionsRequest options)
        {
            return _sessions.Values.Where(o =>
            {
                return string.IsNullOrEmpty(o.FriendlyName)
                || string.IsNullOrEmpty(options.Filter)
                || o.FriendlyName.StartsWith(options.Filter, StringComparison.OrdinalIgnoreCase);
            });
        }

        public ISession FindSession(string sessionId)
        {
            if (_sessions.TryGetValue(sessionId, out ISession session))
                return session;
            return null;
        }

        public IUser FindUser(string id)
        {
            return _users.GetOrAdd(id, (id) => CreateUser(id));
        }

        protected abstract IUser CreateUser(string id);
    }    

    public interface ISessionCommand
    {
        DateTime Utc { get; }
        IUser Owner { get; }
        ISession Session { get; }
        bool Broadcast { get; }
    }    

    public interface ISession
    {
        string Id { get; }
        string FriendlyName { get; }
        string Password { get; }
        IEnumerable<IUser> Members { get; }

        void Add(IUser member);
        void Leave(IUser member);        

        IUser FindMember(string id);
        void Publish(ISessionCommand cmd);
    }

    public abstract class Session : ISession
    {
        public string Password { get; set; } = string.Empty;

        ConcurrentDictionary<string, IUser> _members = new ConcurrentDictionary<string, IUser>();
        public IEnumerable<IUser> Members => _members.Values;

        public string FriendlyName { get; set; }

        public string Id { get; } 


        public Session()
            : this(Guid.NewGuid().ToString())
        {
        }

        public Session(string id)
        {
            Id = id;
        }

        public void Add(IUser member)
        {
            if (_members.TryAdd(member.Id, member))
            {
                OnMemberAdded(member);
            }
        }

        public void Leave(IUser member)
        {
            if (_members.TryRemove(member.Id, out IUser oldMember))
            {
                OnMemberLeft(member);
            }
        }              

        protected abstract void OnCommand(ISessionCommand cmd);
        protected abstract void OnMemberAdded(IUser member);
        protected abstract void OnMemberLeft(IUser member);
        
        public IUser FindMember(string id)
        {
            if (_members.TryGetValue(id, out IUser member))
                return member;
            return null;
        }

        public void Publish(ISessionCommand cmd)
        {
            OnCommand(cmd);            
        }
    }
}
