﻿using Grpc.Core;
using Grpc.Net.Client;
using MaSoft.Framework;
using System;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MaSoft.Framework.Grpc
{

    public class ServiceClient<T> where T : class
    {
        public T Client
        {
            get
            {
                if (_client == null)
                {
                    Channel = CreateAuthenticatedChannel(RemoteAddress);
                    return _client = (T)Activator.CreateInstance(typeof(T), Channel);
                }

                return _client;
            }
        }

        T _client;

        protected GrpcChannel Channel { get; set; }

        protected ServiceClient(string remoteAddress)
        {
            RemoteAddress = remoteAddress;
        }

        public string RemoteAddress { get; }

        public RSA KeyPair { get; set; } = Program.Current.Identity;
        public RSA ServerPublicKey { get; set; } = RSA.Create();
        public Token Token { get; set; }

        private GrpcChannel CreateAuthenticatedChannel(string address)
        {
            var credentials = CallCredentials.FromInterceptor((context, metadata) =>
            {
                if (!string.IsNullOrEmpty(Token?.Value))
                {
                    metadata.Add("Authorization", $"Bearer {Token.Value}");
                }
                return Task.CompletedTask;
            });

            if (address.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
            {

                // SslCredentials is used here because this channel is using TLS.
                // CallCredentials can't be used with ChannelCredentials.Insecure on non-TLS channels.
                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient(),
                    Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
                });

                return channel;
            }
            else
            {
                // AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

                var channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    HttpClient = CreateHttpClient2()                    
                });

                return channel;
            }
        }

        private HttpClient CreateHttpClient()
        {
            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.CheckCertificateRevocationList = false;
            httpClientHandler.ClientCertificateOptions = ClientCertificateOption.Manual;
            httpClientHandler.ServerCertificateCustomValidationCallback = ValidateCertificate;
            httpClientHandler.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\ca.pem"));
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\1.3.6.1.5.5.7.3.1\localhost.pem"));
            return new HttpClient(httpClientHandler);
        }

        private HttpClient CreateHttpClient2()
        {
            var httpClientHandler = new HttpClientHandler();            
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\ca.pem"));
            //httpClientHandler.ClientCertificates.Add(CertificateExtension.ImportFromFilePem(@"I:\code\Certificates\bin\Debug\Server\ca\1.3.6.1.5.5.7.3.1\localhost.pem"));
            return new HttpClient(httpClientHandler);
        }

        private bool ValidateCertificate(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3, SslPolicyErrors arg4)
        {
            var value = Encoding.UTF8.GetString(arg2.PublicKey.EncodedKeyValue.RawData);
            var key = arg2.GetRSAPublicKey();
            ServerPublicKey = key; //.ImportRSAPublicKey(arg2.PublicKey.EncodedParameters.EncodedKeyValue.RawData, out int _);
            return true;
        }

        public async Task ShutdownAsync()
        {
            await Channel.ShutdownAsync();
            _client = null;
        }        

        public X509Certificate2 ServerCertificate { get; set; }

    }
}
