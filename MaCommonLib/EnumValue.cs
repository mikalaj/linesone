﻿namespace MaSoft.Framework
{
    public class EnumValue<T> : NotifyPropertyChanged 
    {
        private bool _isChecked;
        private T _value;
        private string _text;
        
        public T Value
        {
            get => _value; 
            set
            {
                if (Equals(_value, value))
                    return;
                _value = value;
                OnPropertyChanged();
            }
        }

        public string Text
        {
            get => _text;
            set
            {
                if (Equals(_text, value))
                    return;
                _text = value;
                OnPropertyChanged();
            }
        }        

        public bool IsChecked
        {
            get => _isChecked; 
            set
            {
                if (value == _isChecked)
                    return;
                _isChecked = value;
                OnPropertyChanged();
            }
        }
    }
}
