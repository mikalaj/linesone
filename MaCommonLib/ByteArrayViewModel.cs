﻿
using System;
using System.Text;

namespace MaSoft.Framework
{
    public class ByteArrayViewModel : NotifyPropertyChanged
    {
        private byte[] _buffer;

        public ByteArrayViewModel(byte[] v)
        {
            _buffer = v;
        }

        public byte[] Data
        {
            get => _buffer;
            set
            {
                if (Array.Equals(_buffer, value))
                    return;

                _buffer = value;
                OnPropertyChanged(nameof(HasValue), nameof(Data), nameof(Text));
            }
        }

        public string Text
        {
            get => _buffer != null ? Encoding.UTF8.GetString(_buffer) : "";
            set
            {
                var bytes = Encoding.UTF8.GetBytes(value);
                Data = bytes;
            }
        }

        public bool HasValue
        {
            get => _buffer != null && _buffer.Length != 0;
        }
    }
}
