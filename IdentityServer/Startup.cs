﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MaSoft.Framework.Grpc;
using NLog;
using MaSoft.Framework.Identity;
using MaSoft.Framework;

namespace Server
{   
    //public class AuthOptions
    //{
    //    public const string ISSUER = "MyAuthServer"; // издатель токена
    //    public const string AUDIENCE = "MyAuthClient"; // потребитель токена
    //    const string KEY = "mysupersecret_secretkey!123";   // ключ для шифрации
    //    public const int LIFETIME = 24 * 60 * 60; // время жизни токена - 1 минута
    //    public static SymmetricSecurityKey GetSymmetricSecurityKey()
    //    {
    //        return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
    //    }
    //}

    public class Startup
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddGrpc();
            services.AddControllers();
            services.AddSingleton(Module.CreateAccountProvider(AccountProviderType.FileProvider, IdentityServerApp.Current.GetApplicationDataFolder()));
            services.AddSingleton(IdentityServerApp.Current.IdentityService);            
        }       

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();
            
            //app.UseAuthentication();
            //app.UseAuthorization();            
           
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();

                Module.RegisterIdentityServer(endpoints);
                
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });            
        }

        private Task RequestDelegateFunc(HttpContext context)
        {
            context.Items["session-manager"] = "the session manager";
            return Task.CompletedTask;
        }
    }
}
