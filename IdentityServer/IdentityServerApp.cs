using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MaSoft.Framework;
using NLog;
using System.Net.Security;
using NLog.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using System.Security.Cryptography;
using MaSoft;
using System.Collections.Concurrent;
using System.Threading;
using Microsoft.IdentityModel.Logging;
using MaSoft.Framework.Certificates;
using MaSoft.Framework.Identity;

namespace Server
{
    public class IdentityServerApp : Program
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();

        public static new IdentityServerApp Current { get => Program.Current as IdentityServerApp; set => Program.Current = value; }

        public IAccountProvider AccountProvider { get; set; }
        public IIdentityServerInfo IdentityService { get; set; }

        static IdentityServerApp()
        {            
            Current = new IdentityServerApp("MaSoft", "IdentityServer");            
        }

        public IdentityServerApp(string company, string application)
            : base(company, application)
        {
            ServerCertificate = GetServerCertificate();
            IdentityService = Module.CreateServerInfo();
            IdentityService.Audience = Config.Get("IdentityAudience", "Users");
            IdentityService.Issuer = Config.Get("IdentityIssuer", company);
            IdentityService.LifeTime = Config.Get("TokenLifeTime", TimeSpan.FromDays(1));
            IdentityService.Certificate = ServerCertificate;
            AccountProvider = Module.CreateAccountProvider(Config.Get("AccountProviderType", AccountProviderType.FileProvider), Config.Get("AccountProviderConnectionString", GetApplicationDataFolder()));
        }

        public static async Task Main(string[] args)
        {
            NLog.Web.NLogBuilder.ConfigureNLog("NLog.config");
            IdentityModelEventSource.ShowPII = true;
            // AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            await Current.RunAsync(args);
        }

        // Additional configuration is required to successfully run gRPC on macOS.
        // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682
        public IHostBuilder CreateHostBuilder(string[] args) =>            
        Host.CreateDefaultBuilder(args)     
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.Sources.Clear();
                    var env = hostingContext.HostingEnvironment;

                    config.SetBasePath(GetApplicationDataFolder());
                    config.AddJsonFile(Path.GetFileName(GetSettingsPath()), optional: true, reloadOnChange: true);

                    config.AddEnvironmentVariables();

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddFilter("Grpc", Config.Get<MaSoft.Framework.LogLevel>("Grpc").ToLogLevel());
                })
                .UseNLog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .UseStartup((ctx) =>
                    {
                        return new Startup();
                    })
                    //.UseUrls($"https://{Config.Get(SettingNames.ListenAddress, SettingNames.ListenAddressDefault)}:{Config.GetInt(SettingNames.ListenPort, SettingNames.ListenPortDefault)}")
                    .ConfigureKestrel(v =>
                    {
                        var ip = Config.Get(SettingNames.ListenAddress, SettingNames.ListenAddressDefault);

                        //v.ConfigureHttpsDefaults(cfg =>
                        //{                        
                        //    cfg.AllowAnyClientCertificate();
                        //    cfg.ServerCertificate = GetServerCertificate();
                        //    // cfg.ServerCertificateSelector = (ctx, s) => SelectServerCertificate(ctx, s);
                        //});

                        if (ip == "localhost")
                            ip = "127.0.0.1";

                        if (ip == "*")
                        {
                            v.ListenAnyIP(Config.Get(SettingNames.ListenPort, SettingNames.ListenPortDefault), cfg =>
                            {
                                cfg.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2;
                            });

                            //v.ListenAnyIP(Config.Get(SettingNames.ListenPort, SettingNames.ListenPortDefault),
                            //         cfg =>
                            //         {
                            //             cfg.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2;

                            //             cfg.UseHttps(https =>
                            //             {
                            //                 https.ServerCertificate = GetServerCertificate();
                            //                 https.SslProtocols = System.Security.Authentication.SslProtocols.Tls12;
                            //                 // https.ServerCertificateSelector = (ctx, s) => SelectServerCertificate(ctx, s);
                            //                 // https.AllowAnyClientCertificate();
                            //                 https.ClientCertificateMode = ClientCertificateMode.NoCertificate;
                            //                 https.ClientCertificateValidation = ValidateClientCertificate;
                            //                 https.OnAuthenticate = OnAuthenticate;
                            //             });
                            //         });
                        }
                        else
                        {
                            v.Listen(System.Net.IPAddress.Parse(ip),
                                        Config.Get(SettingNames.ListenPort, SettingNames.ListenPortDefault), cfg =>
                            {
                                cfg.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2;
                            });

                            //v.Listen(System.Net.IPAddress.Parse(ip),
                            //         Config.Get(SettingNames.ListenPort, SettingNames.ListenPortDefault),
                            //         cfg =>
                            //         {
                            //             cfg.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2;
                            //             cfg.UseHttps(https =>
                            //             {
                            //                 https.SslProtocols = System.Security.Authentication.SslProtocols.Tls12
                            //                     | System.Security.Authentication.SslProtocols.Tls
                            //                     | System.Security.Authentication.SslProtocols.Tls11
                            //                     | System.Security.Authentication.SslProtocols.Tls13;
                                             
                            //                 https.ServerCertificateSelector = (ctx, s) => SelectServerCertificate(ctx, s);
                            //                 https.AllowAnyClientCertificate();
                            //             });
                            //         });
                        }
                    });
                });

        private void OnAuthenticate(ConnectionContext ctx, SslServerAuthenticationOptions v)
        {
            Log.Info($"OnAuthenticate {ctx.RemoteEndPoint}");
        }

        private bool ValidateClientCertificate(X509Certificate2 x509Certificate2, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors)
        {
            Log.Info($"Client certificate unconditionally valid");
            return true;
        }

        private X509Certificate2 SelectServerCertificate(ConnectionContext ctx, string s)
        {
            return GetServerCertificate();
        }

        private X509Certificate2 GetServerCertificate()
        {
            var certPem = Config.Get<string>(SettingNames.ServerCertificate);
            var privateKeyPem = Config.Get<string>(SettingNames.ServerPrivateCertificate);
            var publicKeyPem = Config.Get<string>(SettingNames.ServerPublicCertificate);

            if (!string.IsNullOrEmpty(certPem) && !string.IsNullOrEmpty(privateKeyPem) && !string.IsNullOrEmpty(publicKeyPem))
            {
                Identity = RSA.Create();
                Identity.ImportPrivateKeyFromPem(privateKeyPem);
                return ServerCertificate = CertificateExtension.ImportFromPemWithRsaPrivateKey(certPem, privateKeyPem);
            }

            var ca = new CertificateAuthority(GetLocation(), "localhost");

            var req = CertificateHelper.CreateServerCertificateRequestWithRSA("localhost");
            var serverCert = ca.GetServerCertificate(req.Request.CreateSigningRequest());

            Config.Set(SettingNames.ServerCertificate, serverCert.ExportToPEM());
            Config.Set(SettingNames.ServerPrivateCertificate, req.Key.ExportPrivateKeyToPEM());
            Config.Set(SettingNames.ServerPublicCertificate, req.Key.ExportPublicKeyToPEM());

            Identity = req.Key;

            return ServerCertificate = new X509Certificate2(serverCert.CopyWithPrivateKey(req.Key).Export(X509ContentType.Pfx, "12345678"), "12345678");
        }

        public X509Certificate2 ServerCertificate { get; set; }

        protected override async Task OnRunAsync(string[] args)
        {
            await CreateHostBuilder(args).Build().RunAsync();
        }
    }
}
