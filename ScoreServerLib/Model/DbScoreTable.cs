﻿using LiteDB;
using System.Collections.Generic;
using System.Linq;

namespace MaSoft.Framework.Score
{
    public class DbScoreTable: IScoreTable
    {
        private string _path;
        private object _sync;

        public DbScoreTable(string storagePath)
        {
            _path = storagePath;
            _sync = new object();

            AddScore("Kerman", 13);
        }

        public void AddScore(string name, int points)
        {
            lock (_sync)
            {
                AddScoreInternal(name, points);
            }
        }

        private void AddScoreInternal(string name, int points)
        {
            using (var db = new LiteDatabase(_path))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<ScoreRecord>("scores");

                // Create your new customer instance
                var customer = new ScoreRecord
                {                    
                    Name = name,
                    Points = points
                };

                // Insert new customer document (Id will be auto-incremented)
                col.Insert(customer);                

                // Index document using document Name property
                col.EnsureIndex(x => x.Name);
                col.EnsureIndex(x => x.Points);
            }
        }

        public void DeleteScore(string id)
        {
            lock (_sync)
            {
                DeleteScoreInternal(id);
            }
        }

        private void DeleteScoreInternal(string id)
        {
            using (var db = new LiteDatabase(_path))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<ScoreRecord>("scores");

                col.Delete(id);
                
                col.EnsureIndex(x => x.Name);
                col.EnsureIndex(x => x.Points);
            }
        }

        public IEnumerable<ScoreRecord> GetTop(int count)
        {
            lock (_sync)
            {
                return GetTopInternal(count);
            }
        }

        private IEnumerable<ScoreRecord> GetTopInternal(int count)
        {
            using (var db = new LiteDatabase(_path))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<ScoreRecord>("scores");

                return col.Find(Query.All(nameof(ScoreRecord.Points), Query.Descending), 0, count).ToList();
            }
        }
    }
}
