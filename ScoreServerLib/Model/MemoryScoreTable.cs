﻿using LiteDB;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MaSoft.Framework.Score
{
    public class MemoryScoreTable : IScoreTable
    {
        List<ScoreRecord> _records = new List<ScoreRecord>();

        public MemoryScoreTable()
        {
            _records.Add(new ScoreRecord { Name = "Mr. Smith", Points = 10 });
            _records.Add(new ScoreRecord { Name = "Mr. Dush", Points = 15 });
        }

        public void AddScore(string name, int points)
        {
            _records.Add(new ScoreRecord { Name = name, Points = points });
        }

        public void DeleteScore(string id)
        {
            _records.RemoveAll(o => o.Id == id);
        }

        public IEnumerable<ScoreRecord> GetTop(int count)
        {
            return _records.OrderByDescending(o => o.Points).Take(count);
        }
    }
}
