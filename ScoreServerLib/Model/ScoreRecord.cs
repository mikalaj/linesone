﻿using System;

namespace MaSoft.Framework.Score
{

    public class ScoreRecord
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public int Points { get; set; }
    }
}
