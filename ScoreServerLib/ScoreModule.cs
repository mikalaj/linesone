﻿using MaSoft.Framework.Score.Clients;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using System;

namespace MaSoft.Framework.Score
{
    public static class Module
    {
        public static IScoreClient CreateScoreClient()
        {
            return new ScoreClient();
        }

#if !NETSTANDARD

        public static void RegisterScoreServer(IEndpointRouteBuilder ep)
        {
            ep.MapGrpcService<ScoreService>();
        }

#endif

    }
}
