﻿#if !NETSTANDARD
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.IdentityModel.Tokens;
using MaSoft;
using MaSoft.Framework;
using Protocol;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using NLog;
using Microsoft.AspNetCore.Authorization;
using MaSoft.Framework.Identity;

namespace MaSoft.Framework.Score
{
    public class ScoreService : Protocol.ScoreService.ScoreServiceBase
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        IScoreTable _table;
        public ScoreService(IScoreTable table)
        {
            Log.Info("ScoreService created");
            _table = table;
        }

        public override Task<Empty> Connect(Empty request, ServerCallContext context)
        {
            return Task.FromResult(new Empty());
        }

        [Authorize(Roles = WellKnownRoles.Guest)]
        public override Task<ProtoScoreResponse> SendScore(ProtoScoreRequest request, ServerCallContext context)
        {
            return Task.Run(() =>
            {
                var name = request.Score.Name;
                if (string.IsNullOrEmpty(name))
                {
                    name = "UserX";
                }

                if (name.Length > 32)
                {
                    name = name.Substring(0, 32);
                }

                _table.AddScore(request.Score.Name, request.Score.Score);
                return new ProtoScoreResponse { GameId = request.GameId };
            });
        }

        [Authorize(Roles = WellKnownRoles.Guest)]
        public override Task<ProtoGetTopScoreResponse> GetTopScores(ProtoGetTopScoreRequest request, ServerCallContext context)
        {
            return Task.Run(() =>
            {
                var scores = _table.GetTop(request.Count);
                var result = new ProtoGetTopScoreResponse();
                result.Scores.AddRange(scores.Select(o => new ProtoScore
                {
                    Id = o.Id,
                    Name = o.Name,
                    Score = o.Points
                }));

                return result;
            });
        }

        [Authorize(Roles = WellKnownRoles.Admin)]
        public override Task<ProtoDeleteScoreResponse> DeleteScore(ProtoDeleteRequest request, ServerCallContext context)
        {
            return Task.Run(() =>
            {
                _table.DeleteScore(request.Id);
                return new ProtoDeleteScoreResponse { };
            });
        }
    }
}
#endif