﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace MaSoft.Framework.Score
{

    public interface IScoreTable
    {
        void AddScore(string name, int points);
        IEnumerable<ScoreRecord> GetTop(int count);
        void DeleteScore(string id);
    }
}
