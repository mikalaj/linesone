﻿using MaSoft.Framework.Grpc;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MaSoft.Framework.Score.Clients
{
    public class Record
    {
        public string Id { get; set; }
        public DateTime TimeUtc { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
    }

    public interface IScoreClient
    {
        Task SetConnectionInfoAsync(string scoreServer, string identityServer, RSA identity);
        Task<List<Record>> GetTopAsync(int count);
        Task DeleteRecordAsync(string id);
        Task AddNewScoreAsync(string name, int score);
    }
}
