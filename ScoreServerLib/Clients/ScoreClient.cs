﻿using Google.Protobuf;
using Grpc.Net.Client;
using MaSoft.Framework.Certificates;
using MaSoft.Framework.Grpc;
using MaSoft.Framework.Identity;
using MaSoft.Framework.Identity.Clients;
using NLog;
using Protocol;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MaSoft.Framework.Score.Clients
{
    public class ScoreClient : GrpcClient<Protocol.ScoreService.ScoreServiceClient>, IScoreClient
    {        
        public async Task AddNewScoreAsync(string name, int score)
        {
            var client = await GetClientAsync();
            try
            {
                await client.SendScoreAsync(new ProtoScoreRequest
                {
                    GameId = "",
                    Score = new ProtoScore
                    {
                        Id = "",
                        Name = name,
                        Score = score
                    }
                });
            }
            catch (Exception e)
            {
                Log.Error($"AddNewScoreAsync failed. {e.Message}");
                Restart();
                throw;
            }
        }

       

        public async Task DeleteRecordAsync(string id)
        {
            var client = await GetClientAsync();
            try
            {
                await client.DeleteScoreAsync(new ProtoDeleteRequest
                {
                    Id = id
                });
            }
            catch (Exception e)
            {
                Log.Error($"AddNewScoreAsync failed. {e.Message}");
                Restart();
                throw;
            }
        }        

        public async Task<List<Record>> GetTopAsync(int count)
        {
            var client = await GetClientAsync();
            try
            {
                var result = await client.GetTopScoresAsync(new ProtoGetTopScoreRequest
                {
                    Count = count
                });

                return result.Scores.Select(o => new Record
                {
                    Id = o.Id,
                    Name = o.Name,
                    Score = o.Score,
                }).ToList();
            }
            catch (Exception e)
            {
                Log.Error($"AddNewScoreAsync failed. {e.Message}");
                Restart();
                throw;
            }
        }

        private static Logger Log = LogManager.GetCurrentClassLogger();
    }
}
