﻿using Lines.Lib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Lines
{

    public class Game1 : Game, IRenderLib, IAssetsLib
    {
        Texture2D[] _balls = new Texture2D[Enum.GetValues(typeof(BallType)).Length];
        Texture2D[] _smallBballs = new Texture2D[Enum.GetValues(typeof(BallType)).Length];
        SpriteFont _font;
        private Texture2D ballTexture;
        private Vector2 ballPosition;
        private float ballSpeed;
        private GraphicsDeviceManager _graphics;
        private Field _field;

        struct Resizing
        {
            public bool Pending;
            public int Width;
            public int Height;
        }

        Resizing resizing = new Resizing();

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _field = new Field();

            OnResize += (width, height) => _field.on_resize(width, height);
            OnMouseMove += (state, dx, dy) => _field.on_mouse(state.X, state.Y);
            OnMouseLeftClick += (state) => _field.on_click(state);

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += Window_ClientSizeChanged;
        }

        public Texture2D get_ball(BallType ball)
        {
            return _balls[(int)ball];
        }

        public SpriteFont get_font()
        {
            return _font;
        }

        public Texture2D get_small_ball(BallType preview_ball)
        {
            return _balls[(int)preview_ball];
        }


        private void Window_ClientSizeChanged(object sender, EventArgs e)
        {
            resizing.Pending = true; // Resize is pending
            resizing.Width = Window.ClientBounds.Width;
            resizing.Height = Window.ClientBounds.Height;
        }

        protected override void Initialize()
        {
            ballPosition = new Vector2(_graphics.PreferredBackBufferWidth / 2,
            _graphics.PreferredBackBufferHeight / 2);
            ballSpeed = 100f;

            _lastState = Mouse.GetState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _basicEffect = new BasicEffect(_graphics.GraphicsDevice);
            _basicEffect.VertexColorEnabled = true;
            _basicEffect.Projection = Matrix.CreateOrthographicOffCenter
            (0,
            _graphics.GraphicsDevice.Viewport.Width,     // left, right
            _graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
            0, 1);

            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _balls[(int)BallType.blue] = Content.Load<Texture2D>("ball_blue64");
            _balls[(int)BallType.brown] = Content.Load<Texture2D>("ball_brown64");
            _balls[(int)BallType.cian] = Content.Load<Texture2D>("ball_cian64");
            _balls[(int)BallType.green] = Content.Load<Texture2D>("ball_green64");
            _balls[(int)BallType.pink] = Content.Load<Texture2D>("ball_pink64");
            _balls[(int)BallType.red] = Content.Load<Texture2D>("ball_red64");
            _balls[(int)BallType.yellow] = Content.Load<Texture2D>("ball_yellow64");

            _font = Content.Load<SpriteFont>("Fonts/Arial");


            // TODO: use this.Content to load your game content here
            ballTexture = Content.Load<Texture2D>("ball");
        }

        MouseState _lastState;
        bool _leftDown = false;

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (resizing.Pending)
            {
                _graphics.PreferredBackBufferWidth = resizing.Width;
                _graphics.PreferredBackBufferHeight = resizing.Height;
                _graphics.ApplyChanges();
                resizing.Pending = false;
                Resize(Window.ClientBounds.Width, Window.ClientBounds.Height);
            }

            MouseState state;

            if (TouchPanel.GetCapabilities().IsConnected)
            {
                var tc = TouchPanel.GetState();
                if (tc.Count == 1)
                {
                    state = new MouseState((int)tc[0].Position.X, (int)tc[0].Position.Y, 0, tc[0].State == TouchLocationState.Pressed ? ButtonState.Pressed : ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);
                }
                else
                {
                    state = new MouseState(_lastState.X, _lastState.Y, 0, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released, ButtonState.Released);
                }
            }
            else
            {
                state = Mouse.GetState();
            }

            if (state.LeftButton == ButtonState.Pressed && _lastState.LeftButton == ButtonState.Released)
            {
                _leftDown = true;
                OnMouseLeftButtonDown?.Invoke(state);
            }

            if (state.LeftButton == ButtonState.Released && _lastState.LeftButton == ButtonState.Pressed)
            {
                if (_leftDown)
                {
                    _leftDown = false;
                    OnMouseLeftClick?.Invoke(state);
                }

                OnMouseLeftButtonUp?.Invoke(state);
            }

            if (state.X != _lastState.X || state.Y != _lastState.Y)
            {
                if (Math.Abs(state.X - _lastState.X) >= 20 || Math.Abs(state.Y - _lastState.Y) >= 20)
                {
                    _leftDown = false;
                }
                OnMouseMove?.Invoke(state, state.X - _lastState.X, state.Y - _lastState.Y);
            }

            _lastState = state;
            

            // TODO: Add your update logic here
            var kstate = Keyboard.GetState();

            if (kstate.IsKeyDown(Keys.Up))
                ballPosition.Y -= ballSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (kstate.IsKeyDown(Keys.Down))
                ballPosition.Y += ballSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (kstate.IsKeyDown(Keys.Left))
                ballPosition.X -= ballSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (kstate.IsKeyDown(Keys.Right))
                ballPosition.X += ballSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (ballPosition.X > _graphics.PreferredBackBufferWidth - ballTexture.Width / 2)
                ballPosition.X = _graphics.PreferredBackBufferWidth - ballTexture.Width / 2;
            else if (ballPosition.X < ballTexture.Width / 2)
                ballPosition.X = ballTexture.Width / 2;

            if (ballPosition.Y > _graphics.PreferredBackBufferHeight - ballTexture.Height / 2)
                ballPosition.Y = _graphics.PreferredBackBufferHeight - ballTexture.Height / 2;
            else if (ballPosition.Y < ballTexture.Height / 2)
                ballPosition.Y = ballTexture.Height / 2;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _field.draw(this, this, gameTime.ElapsedGameTime);

            // TODO: Add your drawing code here
            // _spriteBatch.Begin();
            // _spriteBatch.Draw(ballTexture,
            //                  ballPosition,
            //                  null,
            //                  Color.White,
            //                  0f,
            //                  new Vector2(ballTexture.Width / 2, ballTexture.Height / 2),
            //                  Vector2.One,
            //                  SpriteEffects.None,
            //                  0f);                        
            // _spriteBatch.End();

            base.Draw(gameTime);
        }

        private BasicEffect _basicEffect;
        private SpriteBatch _spriteBatch;

        public int Width { get => _graphics.GraphicsDevice.DisplayMode.Width; }
        public int Height { get => _graphics.GraphicsDevice.DisplayMode.Height; }

        public event OnResize OnResize;
        public event OnMouseLeftButtonDown OnMouseLeftButtonDown;
        public event OnMouseMove OnMouseMove;
        public event OnMouseLeftButtonUp OnMouseLeftButtonUp;
        public event OnMouseLeftClick OnMouseLeftClick;


        public void draw_line(int x1, int y1, int x2, int y2, Color color)
        {
            _basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new VertexPositionColor[2];
            vertices[0].Position = new Vector3(x1, y1, 0);
            vertices[0].Color = color;
            vertices[1].Position = new Vector3(x2, y2, 0);
            vertices[1].Color = color;

            _graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);
        }

        public void draw_quad(int x, int y, int width, int height, Color color)
        {
            _basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new VertexPositionColor[4];
            vertices[0].Position = new Vector3(x, y, 0);
            vertices[0].Color = color;
            vertices[1].Position = new Vector3(x + width, y, 0);
            vertices[1].Color = color;
            vertices[2].Position = new Vector3(x, y + height, 0);
            vertices[2].Color = color;
            vertices[3].Position = new Vector3(x + width, y + height, 0);
            vertices[3].Color = color;

            _graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleStrip, vertices, 0, 2);
        }

        public void draw_rect(int x, int y, int width, int height, Color color)
        {
            _basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new VertexPositionColor[5];
            vertices[0].Position = new Vector3(x, y, 0);
            vertices[0].Color = color;
            vertices[1].Position = new Vector3(x + width, y, 0);
            vertices[1].Color = color;
            vertices[2].Position = new Vector3(x + width, y + height, 0);
            vertices[2].Color = color;
            vertices[3].Position = new Vector3(x, y + height, 0);
            vertices[3].Color = color;
            vertices[4].Position = new Vector3(x, y, 0);
            vertices[4].Color = color;

            _graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, 4);
        }

        public void draw_text(float v1, float v2, int font_size, SpriteFont spriteFont, Color color, string text)
        {
            _spriteBatch.Begin();
            // Finds the center of the string in coordinates inside the text rectangle
            Vector2 textMiddlePoint = spriteFont.MeasureString(text) / 2;
            _spriteBatch.DrawString(spriteFont, text, new Vector2 { X = v1, Y = v2 }, color, 0, textMiddlePoint, 1.0f, SpriteEffects.None, 0.5f);
            _spriteBatch.End();
        }

        public void draw_text_hcenter(int x, int y, int width, int size, SpriteFont spriteFont, Color color, string text_score)
        {
            Vector2 text_size = spriteFont.MeasureString(text_score) / 2;
            int off_x = x + width / 2 - (int)text_size.X / 2;
            draw_text(off_x, y, size, spriteFont, color, text_score);
        }

        public void draw_tile(int x, int y, Texture2D tile)
        {
            _spriteBatch.Begin();

            _spriteBatch.Draw(tile,
                             new Vector2 { X = x, Y = y },
                             null,
                             Color.White,
                             0f,
                             new Vector2(0, 0),
                             Vector2.One,
                             SpriteEffects.None,
                             0f);

            _spriteBatch.End();
        }

        public void draw_tile(int x, int y, float scale, Texture2D tile)
        {
            _spriteBatch.Begin();

            var newWidth = tile.Width * scale;
            var newHeight = tile.Height * scale;

            var diff = tile.Width - newWidth;

            _spriteBatch.Draw(tile,
                             // new Vector2(x - tile.Width / 2 + tile.Width * scale, y - tile.Height / 2 ),
                             new Vector2(x, y),
                             null,
                             Color.White,
                             0f,
                             Vector2.Zero,
                             scale,
                             SpriteEffects.None,
                             0f);

            _spriteBatch.End();
        }

        public void Resize(int width, int height)
        {
            _basicEffect.Projection = Matrix.CreateOrthographicOffCenter
            (0,
            _graphics.GraphicsDevice.Viewport.Width,     // left, right
            _graphics.GraphicsDevice.Viewport.Height, 0,    // bottom, top
            0, 1);

            OnResize?.Invoke(width, height);
        }
    }
}
