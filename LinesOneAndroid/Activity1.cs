using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using MaSoft.Framework.Graphics.UI;
using MaSoft.Lines.Client;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NLog;
using NLog.Config;
using System.Threading.Tasks;
using System.Xml;

namespace LinesAndroid
{
    [Activity(
        Label = "@string/app_name",
        MainLauncher = true,
        Icon = "@drawable/icon",
        AlwaysRetainTaskState = true,
        LaunchMode = LaunchMode.SingleInstance,
        ScreenOrientation = ScreenOrientation.Sensor,
        ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize
    )]
    public class Activity1 : AndroidGameActivity
    {
        private Game1 _game;
        private View _view;
        private Task<string> _task;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Bootstraping(); 

            _game = new Game1();
            _game.IsMobile = true;
            _game.ShowKeyboard = () =>
            {
                //_task = KeyboardInput.Show("Title", "Description", "Default", false);
                //_task.ContinueWith((name) =>
                //{
                //    var text = name.Result;
                //    foreach (var c in text)
                //    {
                //        _game.DoChar(Keys.Escape, c);
                //    }
                //});
                //TakeKeyEvents(true);
                //var pView = _game.Services.GetService<View>();
                //var inputMethodManager = Application.GetSystemService(Context.InputMethodService) as InputMethodManager;
                //inputMethodManager.ShowSoftInput(pView, ShowFlags.Forced);
                //inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);                
                //_game.IsKeyboardVisible = true;
            };

            Ui.ShowTextInput = (title, desc, def, pwd) =>
            {
                return KeyboardInput.Show(title, desc, def, pwd);
            };

            _game.HideKeyboard = () =>
            {
                //var text = _task.Result;                
                //var pView = _game.Services.GetService<View>();
                //InputMethodManager inputMethodManager = Application.GetSystemService(Context.InputMethodService) as InputMethodManager;
                //inputMethodManager.HideSoftInputFromWindow(pView.WindowToken, HideSoftInputFlags.None);
                //_game.IsKeyboardVisible = false;
                //TakeKeyEvents(false);
            };            

            _view = _game.Services.GetService(typeof(View)) as View;

            _view.OffsetLeftAndRight(0);
            _view.OffsetTopAndBottom(0);

            SetContentView(_view, new ViewGroup.MarginLayoutParams(-1, -1));
            _game.MainLoop();
        }       

        private void Bootstraping()
        {
            var assembly = this.GetType().Assembly;
            var assemblyName = assembly.GetName().Name;

            var location = $"{assemblyName}.NLog.config";
            foreach (var name in assembly.GetManifestResourceNames())
            {
                if (name.EndsWith("NLog.config", System.StringComparison.InvariantCultureIgnoreCase))
                {                    
                    var stream = assembly.GetManifestResourceStream(name);
                    LogManager.Configuration = new XmlLoggingConfiguration(XmlReader.Create(stream), null);
                    break;
                }
            }
        }

    }
}
