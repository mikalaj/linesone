﻿using MaSoft.Framework.Graphics;
using MaSoft.Framework.Score.Clients;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace MaSoft.Lines.Client
{   
    public class Game1 : MaGame<ProgramAdapter>
    {        
        internal enum GameState
        {
            Init = -1,
            Gameplay = 0,
            Settings,
            Score
        }

        public string Username { get; set; } = "Player";
        public IScoreClient ScoreService { get; set; }        
        
        State<Game1>[] _states = new State<Game1>[] { new LinesGameplayState(), new SettingsState(), new ScoreState()};

        GameState _state = GameState.Init;
        public LinesUI UI { get => base.Ui as LinesUI; }

        internal GameState State
        {
            get => _state;
            set
            {
                if (_state == value)
                    return;
                
                if (_state != GameState.Init)
                    _states[(int)_state].OnLeave(this);

                _state = value;
                _states[(int)_state].OnEnter(this);
            }
        }
        

        public State<Game1> CurrentState { get => _states[(int)_state]; }

        public IRenderLib Render { get; private set; }
        public ILinesAssetsLib Assets { get; private set; }
                

        public Action ShowKeyboard = () => { };
        public Action HideKeyboard = () => { };
        public bool IsKeyboardVisible { get; set; }
        public bool IsMobile { get; set; }

        public Game1()
            : base("MaSoft", "LinesOne", new LinesUI())
        {
            ScoreService = MaSoft.Framework.Score.Module.CreateScoreClient();
            ScoreService.SetConnectionInfoAsync("https://linesone.ma-shadow.tech", "https://identity.ma-shadow.tech", Program.Identity);            

            GraphicsManager.SynchronizeWithVerticalRetrace = true;                

            IsFixedTimeStep = false;            

            Content.RootDirectory = "Content";
            IsMouseVisible = true;            

            _model = FieldModel.Load();
            _table = ScoreTable.Load();

            if (_model == null)
            {
                _model = new FieldModel();
            }

            _model.OnNoWay += Model_OnNoWay;
            _model.OnFieldCleaned += Model_OnFieldCleaned;
            _model.OnGameOver += Model_OnGameOver;
            _model.OnRestart += UI._model_OnRestart;
            
            UI.Exit.OnClicked += (o, e) => DoExit();
            UI.Restart.OnClicked += (o, e) => _model.restart();

            UI.Settings.OnClicked += (o, e) => State = GameState.Settings;
            UI.ScoreTable.OnClicked += (o, e) => State = GameState.Score;

            UI.Back.OnClicked += (o, e) => State = GameState.Gameplay;                                        

            Window.AllowUserResizing = true;            
        }

        protected override void OnMouseLeftButtonDown(MouseState state)
        {
            base.OnMouseLeftButtonDown(state);
            CurrentState.OnMouseLeftButtonDown(this, state);
        }

        protected override void OnMouseLeftButtonUp(MouseState state)
        {
            base.OnMouseLeftButtonUp(state);
            CurrentState.OnMouseLeftButtonUp(this, state);
        }

        protected override void OnMouseLeftClick(MouseState state)
        {
            base.OnMouseLeftClick(state);
            CurrentState.OnMouseLeftButtonClick(this, state);
        }

        protected override void OnMouseMove(MouseState state, int dx, int dy)
        {
            base.OnMouseMove(state, dx, dy);
            CurrentState.OnMouseMove(this, state, dx, dy);
        }

        protected override void OnResize(int width, int height)
        {
            base.OnResize(width, height);

            _needNewBalls = true;

            foreach (var s in _states)
            {
                s.OnResize(this, width, height);
            }

            Render.Resize(width, height);
        }
       
        private object _cs = new object();
        private List<Record> _records = new List<Record>();

        public List<Record> GetRecords()
        {
            lock (_cs)
            {
                return _records.ToList();
            }
        }

        public void DoChar(Keys key, char c)
        {
            UI.UiChar(key, c);
        }        

        private void Model_OnGameOver(object sender, EventArgs args)
        {
            if (_model.score > _table.BestScore)
            {
                _table.BestScore = _model.score;
                _table.Save();                
            }

            ScoreService.AddNewScoreAsync(Username, _model.score);
        }

        private void Model_OnFieldCleaned(object sender, EventArgs args)
        {
            Assets.GetSound(SoundEffectType.Clean).Play();
        }

        private void Model_OnNoWay(object sender, EventArgs args)
        {
            Assets.GetSound(SoundEffectType.NoWay).Play();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {            
            base.OnExiting(sender, args);
        }

        protected void DoExit()
        {
            try
            {                
            }
            catch
            {

            }

            try
            { 
                _model.Save();
                _table.Save();
                Exit();
            }
            catch
            {

            }
        }        

        protected override void Initialize()
        {
            GraphicsManager.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            GraphicsManager.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            GraphicsManager.IsFullScreen = false;
            GraphicsManager.ApplyChanges();            

            base.Initialize();

            Render = new RenderLib(this);

            OnResize(Window.ClientBounds.Width, Window.ClientBounds.Height);

            State = GameState.Gameplay;

            UI.UserNameInput.Text = Username = Program.Config.Get("Username", string.Empty);
            if (string.IsNullOrEmpty(Username))
            {
                State = GameState.Settings;
            }
        }

        protected override void LoadContent()
        {
            Assets = new AssetsLib(this);
            
            UI.Init(this);            

            foreach(var s in _states)
            {
                s.Init(this);
            }            
        }        

        protected override void Update(GameTime gameTime)
        {            
            _model.Update(Render, Assets, gameTime.ElapsedGameTime);
            CurrentState.Update(this, gameTime);

            base.Update(gameTime);
        }

        FrameCounter _frameCounter = new FrameCounter();                
        
        protected override void Draw(GameTime gameTime)
        {
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _frameCounter.Update(deltaTime);

            if (_needNewBalls)
            {
                Assets.GenerateBalls(Render);
                _needNewBalls = false;
            }

            var fps = string.Format("FPS: {0}", _frameCounter.AverageFramesPerSecond);
            var size = Assets.GetFont(FontType.Text).MeasureString(fps);
            
            GraphicsDevice.SetRenderTarget(Render.BackBuffer);
            GraphicsDevice.DepthStencilState = new DepthStencilState() { DepthBufferEnable = true };
            
            Render.Sprite.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, SamplerState.PointClamp,
            DepthStencilState.None, RasterizerState.CullCounterClockwise);

            CurrentState.Draw(this, gameTime);            

            Render.Sprite.End();

            UI.UiDraw(Render, gameTime.ElapsedGameTime);

            GraphicsDevice.SetRenderTarget(null);
            
            Render.SwapBuffer();           

            base.Draw(gameTime);
        }        

        private bool _needNewBalls;
        public FieldModel _model { get; set; }
        public ScoreTable _table { get; set; }

        private static Logger Log = LogManager.GetCurrentClassLogger();
    }
}
