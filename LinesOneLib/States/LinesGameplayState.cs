﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MaSoft.Lines.Client
{
    public class LinesGameplayState : State<Game1>
    {
        private FieldView _field;
        private bool need_draw;

        public override void Draw(Game1 app, GameTime gameTime)
        {
            _field.draw(app.Render, app.Assets, gameTime.ElapsedGameTime);
        }

        public override void Init(Game1 app)
        {
            _field = new FieldView(app._model, app._table);
            _field._no_button.OnClicked += (o, e) => app.Exit();
        }        

        public override void OnEnter(Game1 app)
        {            
            _field.invalidate();            
            app.UI.Back.Visible = false;
            app.UI.ScoreTable.Visible = true;
            app.UI.Settings.Visible = true;
            app.UI.Restart.Visible = true;
            app.UI.Exit.Visible = true;            
        }

        public override void OnExit(Game1 app)
        {            
        }

        public override void OnLeave(Game1 app)
        {
            app.UI.Back.Visible = true;
            app.UI.ScoreTable.Visible = false;
            app.UI.Settings.Visible = false;
            app.UI.Restart.Visible = false;
            app.UI.Exit.Visible = false;
        }

        public override void OnMouseLeftButtonClick(Game1 app, MouseState state)
        {
            _field.on_click(state.X, state.Y);
        }

        public override void OnMouseLeftButtonDown(Game1 app, MouseState state)
        {
        }

        public override void OnMouseLeftButtonUp(Game1 app, MouseState state)
        {
        }

        public override void OnMouseMove(Game1 app, MouseState state, int dx, int dy)
        {
            _field.on_mouse(state.X, state.Y);
        }

        public override void OnResize(Game1 app, int width, int height)
        {
            _field.on_resize(width, height);
        }

        public override void Update(Game1 app, GameTime gameTime)
        {
            _field.Model.Update(app.Render, app.Assets, gameTime.ElapsedGameTime);
        }
    }
}
