﻿using MaSoft.Framework.Score.Clients;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaSoft.Lines.Client
{
    public class ScoreState : State<Game1>
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private bool need_draw;
        private Task<List<Record>> _task;
        private List<Record> _records = new List<Record>();
        private string _error = string.Empty;

        public override void Draw(Game1 app, GameTime gameTime)
        {
            if (need_draw)
            {
                app.UI.Back.Invalidate();

                if (!string.IsNullOrEmpty(_error))
                {
                    app.GraphicsDevice.Clear(new Color(200, 200, 200, 255));
                    app.Render.DrawTextLeft(100, 100, 48, app.Assets.GetFont(FontType.Score), Color.White, _error);
                }
                else if (_task != null)
                {
                    app.GraphicsDevice.Clear(new Color(200, 200, 200, 255));
                    app.Render.DrawTextLeft(100, 64, 48, app.Assets.GetFont(FontType.Score), Color.White, Resources.Resources.Score);
                    app.Render.draw_text_hcenter(0, app.Render.Height / 2 - 100, app.Render.Width, 48, app.Assets.GetFont(FontType.Score), Color.Red, Resources.Resources.Loading);
                }
                else
                {
                    app.GraphicsDevice.Clear(new Color(200, 200, 200, 255));
                    app.Render.DrawTextLeft(100, 64, 48, app.Assets.GetFont(FontType.Score), Color.White, Resources.Resources.Score);

                    int x0 = 10;
                    int x1 = 200;
                    int x2 = 800;
                    int y = 150;
                    int index = 1;

                    foreach (var score in _records.OrderByDescending(o => o.Score))
                    {
                        var color = Color.Gray;
                        if (score.Name == app.Username)
                            color = Color.Red;

                        app.Render.DrawTextLeft(x0, y, 48, app.Assets.GetFont(FontType.Score), color, $"{index}");
                        app.Render.DrawTextLeft(x1, y, 48, app.Assets.GetFont(FontType.Score), color, $"{score.Name}");
                        app.Render.DrawTextLeft(x2, y, 48, app.Assets.GetFont(FontType.Score), color, $"{score.Score}");
                        index++;
                        y += 100;

                        if (y > app.Render.Height - 200)
                            break;
                    }
                }

                need_draw = false;
            }
        }

        public override void Init(Game1 app)
        {
        }

        public override void OnEnter(Game1 app)
        {
            Log.Info("Enter score state...");
            _task = app.ScoreService.GetTopAsync(100);

            need_draw = true;
            app.UI.Back.Visible = true;
            app.UI.ScoreTable.Visible = false;
            app.UI.Settings.Visible = false;
            app.UI.Restart.Visible = false;
            app.UI.Exit.Visible = false;
        }

        public override void OnExit(Game1 app)
        {
        }

        public override void OnLeave(Game1 app)
        {
            app.UI.Back.Visible = false;
            app.UI.ScoreTable.Visible = true;
            app.UI.Settings.Visible = true;
            app.UI.Restart.Visible = true;
            app.UI.Exit.Visible = true;
            Log.Info("Leave score state...");
        }

        public override void OnMouseLeftButtonClick(Game1 app, MouseState state)
        {
        }

        public override void OnMouseLeftButtonDown(Game1 app, MouseState state)
        {
        }

        public override void OnMouseLeftButtonUp(Game1 app, MouseState state)
        {
        }

        public override void OnMouseMove(Game1 app, MouseState state, int dx, int dy)
        {
        }

        public override void OnResize(Game1 app, int width, int height)
        {
            need_draw = true;
        }

        public override void Update(Game1 app, GameTime gameTime)
        {
            if (_task != null && _task.IsCompleted)
            {
                try
                {
                    if (_task.IsCompletedSuccessfully)
                    {
                        _records = _task.Result;
                        _error = string.Empty;
                    }
                    else
                    {
                        _error = _task.Exception.Message;
                        Log.Error(_task.Exception.Message);
                    }
                }
                catch (Exception e)
                {
                    Log.Error($"Failed to get top scores {e.Message}");
                    _error = "Connection to score server is not available";
                }
                need_draw = true;
                _task = null;
            }
        }
    }
}
