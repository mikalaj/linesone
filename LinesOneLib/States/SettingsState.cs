﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MaSoft.Lines.Client
{
    public class SettingsState : State<Game1>
    {
        private bool need_draw;
        
        public override void Draw(Game1 app, GameTime gameTime)
        {
            if (need_draw)
            {
                app.GraphicsDevice.Clear(new Color(200, 200, 200, 255));
                app.Render.DrawTextLeft(100, 64, 48, app.Assets.GetFont(FontType.Score), Color.White, Resources.Resources.Settings);
                app.UI.Back.Invalidate();
                app.UI.UserNameInput.Invalidate();
                app.UI.UserNameLabel.Invalidate();
                need_draw = false;
            }
        }

        public override void Init(Game1 app)
        {
        }

        public override void OnEnter(Game1 app)
        {            
            need_draw = true;
            app.UI.Back.Visible = true;            
            app.UI.UserNameInput.Visible = true;
            app.UI.UserNameLabel.Visible = true;
        }

        public override void OnExit(Game1 app)
        {            
        }

        public override void OnLeave(Game1 app)
        {
            app.UI.Back.Visible = false;            
            app.UI.UserNameInput.Visible = false;
            app.UI.UserNameLabel.Visible = false;
        }

        public override void OnMouseLeftButtonClick(Game1 app, MouseState state)
        {            
        }

        public override void OnMouseLeftButtonDown(Game1 app, MouseState state)
        {
        }

        public override void OnMouseLeftButtonUp(Game1 app, MouseState state)
        {
        }

        public override void OnMouseMove(Game1 app, MouseState state, int dx, int dy)
        {
        }

        public override void OnResize(Game1 app,  int width, int height)
        {
            need_draw = true;            
        }

        public override void Update(Game1 app, GameTime gameTime)
        {
        }
    }
}
