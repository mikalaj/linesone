﻿using MaSoft.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace MaSoft.Lines.Client
{
    public delegate void OnRestartDelegate(object sender, EventArgs args);
    public delegate void OnFieldCleanedDelegate(object sender, EventArgs args);
    public delegate void OnBallsSpawnedDelegate(object sender, EventArgs args);
    public delegate void OnGameOverDelegate(object sender, EventArgs args);
    public delegate void OnNoWayDelegate(object sender, EventArgs args);

    public class FieldModel
    {
        public event OnRestartDelegate OnRestart;
        public event OnFieldCleanedDelegate OnFieldCleaned;
        public event OnBallsSpawnedDelegate OnBallsSpawned;
        public event OnGameOverDelegate OnGameOver;
        public event OnNoWayDelegate OnNoWay;

        public enum state_t
        {
            idle,
            ball_selected,
            ball_move,
            clean,
            spawn,
            end,
            enter_name,
            play_again,
            wait_yes_no,
        };

        private Random _rnd = new Random();
        private CellModel[] _field = new CellModel[LinesGame.field_width * LinesGame.field_height];
        
        public int target_cell { get; set; }        
        public int cur_cell { get; set; }        
        public int score { get; set; }        
        public int clean_id { get; set; }
        public CellModel[] Field { get => _field; set => _field = value; }
        public int free_cells { get; set; } = 0;
        public BallCell[] next_balls { get; set; } = new BallCell[3];        
        public int search { get; set; } = 0;
        public state_t state { get; set; } = state_t.idle;
        public Stack<int> _movePath { get; set; } = new Stack<int>();
        public long _moveDelay { get; set; }

        public FieldModel()
        {
            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    _field[y * LinesGame.field_width + x] = new CellModel();
                }
            }

            for (int i = 0; i < next_balls.Length; ++i)
            {
                next_balls[i] = new BallCell();
            }

            restart();
        }


        public void SendCommand(FieldCommand cmd)
        {
            var cx = cmd.C.X;
            var cy = cmd.C.Y;
            // out_cout("Cell[" << cx << "][" << cy << "] was clicked" << "                     ");

            switch (state)
            {
                case state_t.idle:
                    {
                        var cell = at(cx, cy);
                        if (cell.Ball != BallType.no)
                        {
                            cur_cell = cell.Id;
                            Field[cur_cell].select(true);
                            state = state_t.ball_selected;
                        }
                    }
                    break;
                case state_t.ball_selected:
                    {
                        var cell = at(cx, cy);
                        if (cell.Ball == BallType.no)
                        {
                            target_cell = cell.Id;
                            if (find_path(_movePath))
                            {
                                state = state_t.ball_move;
                            }
                            else
                            {
                                OnNoWay(this, new EventArgs());
                                target_cell = -1;
                            }
                        }
                        else
                        {
                            Field[cur_cell].select(false);
                            cur_cell = cell.Id;
                            Field[cur_cell].select(true);
                        }
                    }
                    break;

            }
        }        

        public void restart()
        {
            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    _field[y * LinesGame.field_width + x].Id = y * LinesGame.field_width + x;
                    _field[y * LinesGame.field_width + x].c = new FieldPoint { X = x, Y = y };
                    _field[y * LinesGame.field_width + x].Reset();
                }
            }

            for (int i = 0; i < 3; ++i)
            {
                next_balls[i].Key = next_ball();
                next_balls[i].Value = GetNextPoint();
            }

            free_cells = LinesGame.field_width * LinesGame.field_height;
            spawn();
            state = state_t.idle;
            // need_draw = true;
            // _restart.invalidate();
            // Exit.invalidate();
            score = 0;
            OnRestart?.Invoke(this, new EventArgs());
        }

        BallType next_ball()
        {
            return (BallType)_rnd.Next(Enum.GetValues(typeof(BallType)).Length - 1);
        }


        FieldPoint GetNextPoint()
        {
            int x = _rnd.Next() % LinesGame.field_width;
            int y = _rnd.Next() % LinesGame.field_height;

            return new FieldPoint { X = x, Y = y };
        }

        public bool find_path(Stack<int> path)
        {
            ++search;
            _movePath.Clear();

            Queue<int> s = new Queue<int>();
            Field[cur_cell].search_rev = search;
            Field[cur_cell].search_step = 0;
            s.Enqueue(cur_cell);

            while (s.Count != 0)
            {
                var cell = s.Peek();

                if (cell == target_cell)
                {
                    path.Push(target_cell);

                    var cur = cell;
                    while (cur != cur_cell)
                    {
                        var offsets = new Point[] { new Point(-1, 0), new Point(1, 0), new Point(0, 1), new Point(0, -1) };
                        foreach (var off in offsets)
                        {
                            if (Field[cur].c.X + off.X >= 0 && Field[cur].c.X + off.X < LinesGame.field_width && Field[cur].c.Y + off.Y >= 0 && Field[cur].c.Y + off.Y < LinesGame.field_height)
                            {
                                var path_cell = at(Field[cur].c.X + off.X, Field[cur].c.Y + off.Y);
                                if (path_cell.search_step + 1 == Field[cur].search_step && path_cell.search_rev == search)
                                {
                                    cur = path_cell.Id;
                                    path.Push(cur);
                                    break;
                                }
                            }
                        }
                    }

                    // path.Reverse();
                    // out_cout("Path found with " << cell.search_step << " steps");
                    return true;
                }

                s.Dequeue();

                int[] b = new int[4];

                if (Field[cell].c.X - 1 >= 0)
                    b[0] = at(Field[cell].c.X - 1, Field[cell].c.Y).Id;
                else
                    b[0] = -1;

                if (Field[cell].c.X + 1 < LinesGame.field_width)
                    b[1] = at(Field[cell].c.X + 1, Field[cell].c.Y).Id;
                else
                    b[1] = -1;

                if (Field[cell].c.Y - 1 >= 0)
                    b[2] = at(Field[cell].c.X, Field[cell].c.Y - 1).Id;
                else
                    b[2] = -1;

                if (Field[cell].c.Y + 1 < LinesGame.field_height)
                    b[3] = at(Field[cell].c.X, Field[cell].c.Y + 1).Id;
                else
                    b[3] = -1;

                foreach (var n in b)
                {
                    if (n == -1)
                        continue;

                    if (Field[n].Ball == BallType.no && (Field[n].search_rev != search || Field[n].search_step > Field[cell].search_step + 1))
                    {
                        Field[n].search_rev = search;
                        Field[n].search_step = Field[cell].search_step + 1;
                        s.Enqueue(n);
                    }
                }
            }
            // out_cout("Path was not found");
            return false;
        }

        public bool Update(IRenderLib c, ILinesAssetsLib a, TimeSpan dt)
        {
            bool flag = false;
            switch (state)
            {
                case state_t.ball_move:
                    {
                        if (_movePath.Count != 0)
                        {
                            _moveDelay -= dt.Ticks;

                            if (_moveDelay < 0)
                            {
                                _moveDelay = LinesGame.move_delay;
                                var cell = _movePath.Pop();

                                if (_movePath.Count != 0)
                                    a.GetSound(SoundEffectType.Move).Play();

                                var b = Field[cur_cell].Ball;
                                Field[cur_cell].Ball = Field[target_cell].Ball;
                                Field[cur_cell].select(false);
                                Field[cell].Ball = b;
                                cur_cell = cell;
                                // target_cell.set_ball(b);
                                // flag |= draw_field(c, a, dt);
                            }
                        }
                        else
                        {
                            state = state_t.clean;
                        }

                        // flag |= _restart.draw(c, a);
                        // flag |= Exit.draw(c, a);
                    }
                    break;

                case state_t.clean:
                    {
                        if (clean_field(target_cell))
                        {
                            OnFieldCleaned?.Invoke(this, new EventArgs());
                            state = state_t.idle;
                            // need_draw = true;
                            //_restart.invalidate();
                            // Exit.invalidate();
                            // a.GetSound(SoundEffectType.Clean).Play();
                        }
                        else
                        {
                            state = state_t.spawn;
                        }
                        target_cell = -1;
                        cur_cell = -1;
                        // flag |= draw_field(c, a, dt);
                        // flag |= _restart.draw(c, a);
                        // flag |= Exit.draw(c, a);
                    }
                    break;

                case state_t.spawn:
                    {
                        spawn();
                        if (free_cells == 0)
                            state = state_t.end;
                        else
                            state = state_t.idle;
                        //flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a);
                        //flag |= Exit.draw(c, a);
                    }
                    break;
                case state_t.idle:
                    {
                        //flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a);
                        //flag |= Exit.draw(c, a);
                    }
                    break;
                case state_t.end:
                    {
                        //if (Model.score > best_score)
                        //{
                        //    SaveRecord();
                        //    best_score = score;
                        //}
                        OnGameOver?.Invoke(this, new EventArgs());
                        state = state_t.play_again;
                    }
                    break;
                case state_t.play_again:
                    {
                        //DrawGameOver(c, a);
                        //DrawPlayAgain(c, a);

                        //if (LinesGame.horizontal_view)
                        //{
                        //    _yes_button.set_rect(window_width / 3, window_height / 2, 64, 64);
                        //    _no_button.set_rect(window_width / 3 * 2, window_height / 2, 64, 64);
                        //}
                        //else
                        //{
                        //    _yes_button.set_rect(window_width / 3, window_height / 2, 64, 64);
                        //    _no_button.set_rect(window_width / 3 * 2, window_height / 2, 64, 64);
                        //}

                        //_yes_button.set_text("Yes");


                        //_no_button.set_text("No");

                        //_yes_button.draw(c, a);
                        //_no_button.draw(c, a);

                        state = state_t.wait_yes_no;
                        // flag |= true;
                    }
                    break;
                case state_t.wait_yes_no:
                    //flag |= _yes_button.draw(c, a);
                    //flag |= _no_button.draw(c, a);
                    break;
                default:
                    //flag |= draw_field(c, a, dt);
                    //flag |= _restart.draw(c, a);
                    //flag |= Exit.draw(c, a);
                    break;
            }

            return flag;
        }


        CellModel find_random_free_cell()
        {
            int count = 100;
            int spawned = 0;

            for (int i = 0; i < count; ++i)
            {
                var p = GetNextPoint();
                int x = p.X;
                int y = p.Y;
                if (at(x, y).Ball != BallType.no || at(x, y).Preview_ball != BallType.no)
                {
                    continue;
                }

                return at(x, y);
            }

            for (int y = 0; y < LinesGame.field_height; ++y)
            {
                for (int x = 0; x < LinesGame.field_width; ++x)
                {
                    if (at(x, y).Ball == BallType.no && at(x, y).Preview_ball == BallType.no)
                    {
                        return at(x, y);
                    }
                }
            }

            throw new System.Exception("Should not try to find cell if no free cells");
        }

        void spawn()
        {
            int count = 100;
            int spawned = 0;

            for (int i = 0; i < 3 && free_cells != 0; ++i)
            {

                if (next_balls[i].Key == BallType.no)
                    continue;

                target_cell = at(next_balls[i].Value.X, next_balls[i].Value.Y).Id;
                if (Field[target_cell].Ball == BallType.no)
                {
                    Field[target_cell].Ball = next_balls[i].Key;
                    Field[target_cell].Preview_ball = BallType.no;
                }
                else
                {
                    Field[target_cell].Preview_ball = BallType.no;
                    var cell = find_random_free_cell();
                    target_cell = cell.Id;                    
                    next_balls[i].Value = cell.c;
                    Field[target_cell].Ball = next_balls[i].Key;
                    Field[target_cell].Preview_ball = BallType.no;
                }

                free_cells--;

                next_balls[i].Key = BallType.no;
            }

            for (int i = 0; i < 3; ++i)
            {
                var c = at(next_balls[i].Value.X, next_balls[i].Value.Y);
                if (c.Ball == BallType.no)
                    continue;
                if (clean_field(c.Id))
                {
                    OnFieldCleaned?.Invoke(this, new EventArgs());
                }
                // need_draw_score = need_draw_next |= clean_field(c);
            }

            int next_free_cells = free_cells;
            for (int i = 0; i < 3 && next_free_cells != 0; ++i)
            {
                var c = find_random_free_cell();
                c.Preview_ball = next_balls[i].Key = next_ball();
                next_balls[i].Value = c.c;
                --next_free_cells;
            }

            OnBallsSpawned?.Invoke(this, new EventArgs());
            // need_draw_next = true;
        }

        public void Save()
        {

            var options = new JsonSerializerOptions { WriteIndented = true };
            options.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
            var text = JsonSerializer.Serialize(this, options);
            File.WriteAllText(LinesGame.GetLastGame(), text);
        }

        public static FieldModel Load()
        {
            var file = LinesGame.GetLastGame();

            if (!File.Exists(file))
                return new FieldModel();

            try
            {
                var text = File.ReadAllText(file);
                var options = new JsonSerializerOptions { WriteIndented = true };
                options.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
                var field = System.Text.Json.JsonSerializer.Deserialize<FieldModel>(text, options);
                return field;
            }
            catch
            {
                return new FieldModel();
            }
        }

        public bool clean_field(int target)
        {
            // assert(target != null);
            // assert(target.Ball != ball_type_t.no);
            clean_id++;
            int count = 0;
            int bonus = -5;

            var b = Field[target].Ball;
            int cx = Field[target].c.X;
            int cy = Field[target].c.Y;
            bool result = false;

            int x1 = cx;
            while (true)
            {
                if (x1 > 0 && at(x1 - 1, cy).Ball == b)
                {
                    --x1;
                }
                else
                {
                    break;
                }
            }

            int x2 = cx;
            while (true)
            {
                if (x2 < LinesGame.field_width - 1 && at(x2 + 1, cy).Ball == b)
                {
                    ++x2;
                }
                else
                {
                    break;
                }
            }

            int y1 = cy;
            while (true)
            {
                if (y1 > 0 && at(cx, y1 - 1).Ball == b)
                {
                    --y1;
                }
                else
                {
                    break;
                }
            }

            int y2 = cy;
            while (true)
            {
                if (y2 < LinesGame.field_height - 1 && at(cx, y2 + 1).Ball == b)
                {
                    ++y2;
                }
                else
                {
                    break;
                }
            }

            int dx11 = cx;
            int dy11 = cy;
            while (true)
            {
                if (dx11 < LinesGame.field_width - 1 && dy11 < LinesGame.field_height - 1 && at(dx11 + 1, dy11 + 1).Ball == b)
                {
                    ++dx11;
                    ++dy11;
                }
                else
                {
                    break;
                }
            }

            int dx12 = cx;
            int dy12 = cy;
            while (true)
            {
                if (dx12 > 0 && dy12 > 0 && at(dx12 - 1, dy12 - 1).Ball == b)
                {
                    --dx12;
                    --dy12;
                }
                else
                {
                    break;
                }
            }

            int dx21 = cx;
            int dy21 = cy;
            while (true)
            {
                if (dx21 > 0 && dy21 < LinesGame.field_height - 1 && at(dx21 - 1, dy21 + 1).Ball == b)
                {
                    --dx21;
                    ++dy21;
                }
                else
                {
                    break;
                }
            }

            int dx22 = cx;
            int dy22 = cy;
            while (true)
            {
                if (dx22 < LinesGame.field_width - 1 && dy22 > 0 && at(dx22 + 1, dy22 - 1).Ball == b)
                {
                    ++dx22;
                    --dy22;
                }
                else
                {
                    break;
                }
            }

            if (x2 - x1 >= LinesGame.min_clean - 1)
            {
                while (x1 <= x2)
                {
                    at(x1, cy).Ball = BallType.no;
                    ++x1;
                    free_cells++;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (y2 - y1 >= LinesGame.min_clean - 1)
            {
                while (y1 <= y2)
                {
                    at(cx, y1).Ball = BallType.no;
                    ++y1;
                    free_cells++;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (dx11 - dx12 >= LinesGame.min_clean - 1)
            {
                while (dx12 <= dx11)
                {
                    at(dx12, dy12).Ball = BallType.no;
                    free_cells++;
                    ++dx12;
                    ++dy12;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            if (dx22 - dx21 >= LinesGame.min_clean - 1)
            {
                while (dx21 <= dx22)
                {
                    at(dx21, dy21).Ball = BallType.no;
                    free_cells++;
                    ++dx21;
                    --dy21;
                    count += 1 + ((++bonus > 0) ? bonus : 0);
                }
                result |= true;
            }

            score += count;
            // need_draw_score = true;
            return result;
        }        

        public CellModel at(int x, int y)
        {
            return _field[y * LinesGame.field_width + x];
        }
    }
}
