﻿using MaSoft.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace MaSoft.Lines.Client
{
    public class AssetsLib : ILinesAssetsLib
    {
        private SpriteFont _font;
        private SpriteFont _scoreFont;
        private Texture2D[] _balls = new Texture2D[Enum.GetValues(typeof(BallType)).Length];
        private SoundEffect _moveEffect;
        private SoundEffect _cleanEffect;
        private SoundEffect _noEffect;

        private Texture2D _gearTexture;
        private Texture2D _doorTexture;
        private Texture2D _restartTexture;
        private Texture2D _cupTexture;
        private Texture2D _backTexture;

        public AssetsLib(Game g)
        {
            App = g;
            var Content = App.Content;

            _balls[(int)BallType.blue] = Content.Load<Texture2D>("ball_blue64");
            _balls[(int)BallType.brown] = Content.Load<Texture2D>("ball_brown64");
            _balls[(int)BallType.cian] = Content.Load<Texture2D>("ball_cian64");
            _balls[(int)BallType.green] = Content.Load<Texture2D>("ball_green64");
            _balls[(int)BallType.pink] = Content.Load<Texture2D>("ball_pink64");
            _balls[(int)BallType.red] = Content.Load<Texture2D>("ball_red64");
            _balls[(int)BallType.yellow] = Content.Load<Texture2D>("ball_yellow64");

            _moveEffect = Content.Load<SoundEffect>("Sounds/move");
            _cleanEffect = Content.Load<SoundEffect>("Sounds/clean");
            _noEffect = Content.Load<SoundEffect>("Sounds/no");


            _font = Content.Load<SpriteFont>("Fonts/Arial");
            _scoreFont = Content.Load<SpriteFont>("Fonts/Score");

            _gearTexture = Content.Load<Texture2D>("Icons/gear");
            _doorTexture = Content.Load<Texture2D>("Icons/door");
            _restartTexture = Content.Load<Texture2D>("Icons/circle_arrow");
            _cupTexture = Content.Load<Texture2D>("Icons/cup");
            _backTexture = Content.Load<Texture2D>("Icons/arrow");
        }

        public Texture2D GetRestartIcon()
        {
            return _restartTexture;
        }

        public Texture2D GetExitIcon()
        {
            return _doorTexture;
        }

        public Texture2D GetGearIcon()
        {
            return _gearTexture;
        }

        //public static Texture2D GetTexture2DFromBitmap(GraphicsDevice device, System.Drawing.Bitmap bitmap)
        //{
        //    Texture2D tex = new Texture2D(device, bitmap.Width, bitmap.Height, false, SurfaceFormat.Color);

        //    var data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);

        //    int bufferSize = data.Height * data.Stride;

        //    //create data buffer 
        //    byte[] bytes = new byte[bufferSize];

        //    // copy bitmap data into buffer
        //    Marshal.Copy(data.Scan0, bytes, 0, bytes.Length);

        //    // copy our buffer to the texture
        //    tex.SetData(bytes);

        //    // unlock the bitmap data
        //    bitmap.UnlockBits(data);

        //    return tex;
        //}

        public Game App { get; }

        Color GetBallColor(BallType type)
        {
            switch (type)
            {
                case BallType.blue:
                    return Color.Blue;
                case BallType.brown:
                    return Color.SaddleBrown;
                case BallType.cian:
                    return Color.Cyan;
                case BallType.green:
                    return Color.Green;
                case BallType.pink:
                    return Color.HotPink;
                case BallType.red:
                    return Color.Red;
                case BallType.yellow:
                    return Color.Yellow;
                default:
                    return Color.Black;
            }
        }

        public void GenerateBalls(IRenderLib render)
        {
            var balls = new BallType[] { BallType.blue, BallType.brown, BallType.cian, BallType.green, BallType.pink, BallType.red, BallType.yellow };

            foreach (var ball in balls)
            {
                using (var _ballRenderTarget = new RenderTarget2D(render.Device, LinesGame.ball_size, LinesGame.ball_size, false,
                    render.Device.PresentationParameters.BackBufferFormat, DepthFormat.Depth24, 0, RenderTargetUsage.PreserveContents))
                {

                    render.Device.SetRenderTarget(_ballRenderTarget);
                    render.Device.DepthStencilState = new DepthStencilState() { DepthBufferEnable = false };

                    render.Device.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.Transparent, 1.0f, 0);
                    render.DrawSphere(Matrix.CreateTranslation(0, 0, 0), Matrix.CreateTranslation(0, 0, 0), Matrix.CreateOrthographic(1, 1, -1.0f, 1.0f), GetBallColor(ball));

                    render.Device.SetRenderTarget(null);

                    if (_balls[(int)ball] != null)
                        _balls[(int)ball].Dispose();

                    _balls[(int)ball] = new Texture2D(render.Device, LinesGame.ball_size, LinesGame.ball_size, false, SurfaceFormat.Color);
                    var data = new Color[LinesGame.ball_size * LinesGame.ball_size];
                    _ballRenderTarget.GetData(data);
                    _balls[(int)ball].SetData(data);

                }
            }
        }

        public SpriteFont GetFont(FontType type)
        {
            switch (type)
            {
                case FontType.Text:
                    return _font;
                case FontType.Score:
                    return _scoreFont;
                default:
                    return _font;
            }
        }

        public Texture2D get_ball(BallType ball)
        {
            return _balls[(int)ball];
        }

        public Texture2D get_small_ball(BallType preview_ball)
        {
            return _balls[(int)preview_ball];
        }

        public SoundEffect GetSound(SoundEffectType type)
        {
            switch (type)
            {
                case SoundEffectType.Move:
                    return _moveEffect;
                case SoundEffectType.Clean:
                    return _cleanEffect;
                case SoundEffectType.NoWay:
                    return _noEffect;
                default:
                    throw new Exception($"Sound effect {type} not found.");
            }
        }

        public Texture2D GetCupIcon()
        {
            return _cupTexture;
        }

        public Texture2D GetBackIcon()
        {
            return _backTexture;
        }
    }
}
