﻿using MaSoft.Framework.Graphics;
using MaSoft.Framework.Graphics.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Xml.Linq;

namespace MaSoft.Lines.Client
{    

    public class FieldView
    {        

        public FieldModel Model { get; set; }
        public ScoreTable Score { get; set; }
        public FieldView(FieldModel model, ScoreTable score)
        {
            Model = model;
            Score = score;

            Model.OnRestart += Model_OnRestart;
            Model.OnFieldCleaned += Model_OnFieldCleaned;
            Model.OnBallsSpawned += Model_OnBallsSpawned;

            _field = Model.Field.Select(o => new CellView(o)).ToArray();
            // LoadRecord();            
        }

        private void Model_OnBallsSpawned(object sender, EventArgs args)
        {
            need_draw_next = true;
        }

        private void Model_OnFieldCleaned(object sender, EventArgs args)
        {
            need_draw_score = need_draw_next = true;
        }

        private void Model_OnRestart(object sender, EventArgs args)
        {
            need_draw = true;
            //_restart.invalidate();
            //_setting.invalidate();
            //Exit.invalidate();
        }

        internal void invalidate()
        {
            need_draw = true;
        }


        //void SaveRecord()
        //{
        //    var path = GetRecordFile();           
        //    File.WriteAllText(path, score.ToString());
        //}

        //public string GetRecordFile()
        //{
        //    var dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MaSoft", "Lines");
        //    if (!Directory.Exists(dir))
        //        Directory.CreateDirectory(dir);

        //    return Path.Combine(dir, "score.dat");
        //}        

        //void LoadRecord()
        //{
        //    var path = GetRecordFile();

        //    if (File.Exists(path))
        //    {
        //        var text = File.ReadAllText(path);
        //        best_score = int.Parse(text);
        //    }
        //}


        void draw_next(IRenderLib c, ILinesAssetsLib a)
        {            
            int cs = LinesGame.cell_size;
            int w = cs * 3;
            int off_x = c.Width / 2 - w / 2;
            int off_y = 0;


            for (int i = 0; i < 3; ++i)
            {
                if (LinesGame.horizontal_view)
                {
                    var width = (c.Width - LinesGame.field_size) / 2;
                    
                    var cell_size = Math.Min(width / 3, LinesGame.cell_size);
                    var left = cell_size;
                    var height = cell_size * 3;
                    var top = (c.Height - height) / 2;

                    c.DrawRectangle(cell_size, top + i * cell_size, cell_size, cell_size, new Color(0, 0, 0, 255));

                    if (Model.next_balls[i].Key != BallType.no)
                    {
                        int b_off_x = left + cell_size / 2 - (int)(LinesGame.ball_size / 2 * 0.3f);
                        int b_off_y = top + cell_size / 2 + cell_size * i - (int)(LinesGame.ball_size / 2 * 0.3f);
                        c.draw_tile(b_off_x, b_off_y, 0.3f, a.get_small_ball(Model.next_balls[i].Key));
                    }
                }
                else
                {
                    c.DrawRectangle(off_x + i * cs, off_y, cs, LinesGame.top_offset, new Color(0, 0, 0, 255));
                    if (Model.next_balls[i].Key != BallType.no)
                    {
                        int b_off_x = off_x + i * cs + cs / 2 - (int)(LinesGame.ball_size / 2 * 0.3f);
                        int b_off_y = off_y + LinesGame.top_offset / 2 - (int)(LinesGame.ball_size / 2 * 0.3f);
                        c.draw_tile(b_off_x, b_off_y, 0.3f, a.get_small_ball(Model.next_balls[i].Key));
                    }
                }
            }
        }

        bool draw_field(IRenderLib c, ILinesAssetsLib a, TimeSpan dt)
        {
            bool flag = need_draw || need_draw_next;

            var field_width = LinesGame.field_width;
            var field_height = LinesGame.field_height;
            var cell_size = LinesGame.cell_size;


            if (need_draw)
            {
                c.DrawQuad(0, 0, x_off, c.Height, new Color(200, 200, 200, 255));
                c.DrawQuad(x_off, 0, field_width * cell_size, y_off, new Color(200, 200, 200, 255));
                c.DrawQuad(x_off + field_width * cell_size, 0, c.Width - x_off + field_width * cell_size, c.Height, new Color(200, 200, 200, 255));
                c.DrawQuad(0, y_off + field_height * cell_size, c.Width, c.Height - y_off - field_height * cell_size, new Color(200, 200, 200, 255));
            }

            for (int y = 0; y < field_height; ++y)
            {
                for (int x = 0; x < field_width; ++x)
                {
                    flag |= _field[y * field_width + x].draw(x_off + x * cell_size, y_off + y * cell_size, c, a, need_draw, dt);
                }
            }

            if (need_draw_next || need_draw)
            {
                draw_next(c, a);
                need_draw_next = false;
            }

            if (need_draw_score || need_draw)
            {
                DrawChampionScore(c, a);
                DrawChallengerScore(c, a);
                need_draw_score = false;
            }

            need_draw = false;

            return flag;
        }

        void DrawChampionScore(IRenderLib c, ILinesAssetsLib a)
        {
            if (LinesGame.horizontal_view)
            {
                var width = (c.Width - LinesGame.field_size) / 2;
                var text_score = Score.BestScore.ToString();
                var y = (int)(150 + a.GetFont(FontType.Score).MeasureString(text_score).Y / 2);
                c.draw_text_hcenter(0, 150, width, 48, a.GetFont(FontType.Score), new Color(0, 127, 0, 255), text_score);
                c.DrawLine(30, y, width - 30, y, Color.Black);
            }
            else
            {
                int cs = LinesGame.cell_size;
                int w = cs * 3;
                int right = c.Width / 2 - w / 2;
                var text_score = Score.BestScore.ToString();
                string text_label = "Champion";

                int bottom = 10 + LinesGame.cell_size;                
                // c.DrawQuad(0, 0, right - 1, LinesGame.top_offset - 1, new Color(200, 200, 200, 240));
                c.draw_text_hcenter(0, 60, right, 48, a.GetFont(FontType.Score), new Color(0, 127, 0, 255), text_score);
            }
        }

        void DrawChallengerScore(IRenderLib c, ILinesAssetsLib a)
        {
            if (LinesGame.horizontal_view)
            {
                var width = (c.Width - LinesGame.field_size) / 2;
                var left = width + LinesGame.field_size;
                var text_score = Model.score.ToString();
                var y = (int)(150 + a.GetFont(FontType.Score).MeasureString(text_score).Y / 2);
                c.DrawQuad(left + 1, 0, width, y, new Color(200, 200, 200, 255));
                c.draw_text_hcenter(left, 150, width, 48, a.GetFont(FontType.Score), new Color(0, 127, 0, 255), text_score);
                c.DrawLine(c.Width - width + 30, y, c.Width - 30, y, Color.Black);
            }
            else
            {
                int cs = LinesGame.cell_size;
                int w = cs * 3;
                int left = c.Width / 2 + w / 2;
                int width = c.Width - left;
                var text_score = Model.score.ToString();

                int bottom = 10 + LinesGame.cell_size;
                c.DrawQuad(left + 1, 0, width, LinesGame.top_offset, new Color(200, 200, 200, 255));
                c.draw_text_hcenter(left, 60, width, 48, a.GetFont(FontType.Score), new Color(0, 127, 0, 255), text_score);
            }
        }

        public bool draw(IRenderLib c, ILinesAssetsLib a, TimeSpan dt)
        {
            if (_yes_button.Font == null)
                _yes_button.Font = a.GetFont(FontType.Score);
            if (_no_button.Font == null)
                _no_button.Font = a.GetFont(FontType.Score);

            bool flag = false;
            switch (Model.state)
            {
                case FieldModel.state_t.ball_move:
                    {
                        //if (_movePath.Count != 0)
                        //{
                        //    _moveDelay -= dt.Ticks;

                        //    if (_moveDelay < 0)                            
                        //    {                                
                        //        _moveDelay = LinesGame.move_delay;
                        //        var cell = _movePath.Pop();

                        //        if (_movePath.Count != 0)
                        //            a.GetSound(SoundEffectType.Move).Play();

                        //        var b = cur_cell.Ball;
                        //        cur_cell.set_ball(target_cell.Ball);
                        //        cur_cell.select(false);
                        //        cell.set_ball(b);
                        //        cur_cell = cell;                                
                        //        // target_cell.set_ball(b);
                                flag |= draw_field(c, a, dt);
                        //    } 
                        //}
                        //else
                        //{
                        //    state = state_t.clean;
                        //}

                        //flag |= _restart.draw(c, a.GetRestartIcon());
                        //flag |= _setting.draw(c, a.GetGearIcon());
                        //flag |= Exit.draw(c, a.GetExitIcon());
                    }
                    break;

                case FieldModel.state_t.clean:
                    {
                        //if (clean_field(target_cell))
                        //{
                        //    state = state_t.idle;
                        //    need_draw = true;
                        //    _restart.invalidate();
                        //    Exit.invalidate();
                        //    a.GetSound(SoundEffectType.Clean).Play();
                        //}
                        //else
                        //{                            
                        //    state = state_t.spawn;
                        //}
                        //target_cell = null;
                        //cur_cell = null;
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a.GetRestartIcon());
                        //flag |= _setting.draw(c, a.GetGearIcon());
                        //flag |= Exit.draw(c, a.GetExitIcon());
                    }
                    break;
                case FieldModel.state_t.spawn:
                    {
                        //spawn();
                        //if (free_cells == 0)
                        //    state = state_t.end;
                        //else
                        //    state = state_t.idle;
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a.GetRestartIcon());
                        //flag |= _setting.draw(c, a.GetGearIcon());
                        //flag |= Exit.draw(c, a.GetExitIcon());
                    }
                    break;
                case FieldModel.state_t.idle:
                    {
                        flag |= draw_field(c, a, dt);
                        //flag |= _restart.draw(c, a.GetRestartIcon());
                        //flag |= _setting.draw(c, a.GetGearIcon());
                        //flag |= Exit.draw(c, a.GetExitIcon());
                    }
                    break;
                case FieldModel.state_t.end:
                    {
                        //if (Model.score > best_score)
                        //{
                        //    SaveRecord();
                        //    best_score = score;
                        //}
                        //state = state_t.play_again;
                    }
                    break;
                case FieldModel.state_t.play_again:
                    {
                        DrawGameOver(c, a);                        

                        if (LinesGame.horizontal_view)
                        {
                            _yes_button.Resize(window_width / 3, window_height / 2, 64, 64);
                            _no_button.Resize(window_width / 3 * 2, window_height / 2, 64, 64);
                        }
                        else
                        {
                            _yes_button.Resize(window_width / 3, window_height / 2, 64, 64);
                            _no_button.Resize(window_width / 3 * 2, window_height / 2, 64, 64);
                        }

                        _yes_button.Text = Resources.Resources.Yes;
                        _no_button.Text = Resources.Resources.No;

                        _yes_button.draw(c, dt);
                        _no_button.draw(c, dt);

                        // state = state_t.wait_yes_no;
                        flag |= true;
                    }
                    break;
                case FieldModel.state_t.wait_yes_no:
                    flag |= _yes_button.draw(c, dt);
                    flag |= _no_button.draw(c, dt);
                    break;
                default:
                    flag |= draw_field(c, a, dt);
                    //flag |= _restart.draw(c, a.GetRestartIcon());
                    //flag |= _setting.draw(c, a.GetGearIcon());
                    //flag |= Exit.draw(c, a.GetExitIcon());
                    break;
            }

            return flag;
        }
        

        void DrawGameOver(IRenderLib c, ILinesAssetsLib a)
        {
            {
                var size = a.GetFont(FontType.Score).MeasureString(Resources.Resources.GameOver);

                c.DrawQuad(0, 0, window_width, window_height, new Color(50, 50, 50, 240));
                c.draw_text_hcenter(0, window_height / 2 - (int)size.Y - 220, window_width, 48, a.GetFont(FontType.Score), new Color(255, 0, 0, 255), Resources.Resources.GameOver);
            }

            {
                var size = a.GetFont(FontType.Score).MeasureString(Resources.Resources.YourScore);

                c.DrawQuad(0, 0, window_width, window_height, new Color(50, 50, 50, 240));
                c.draw_text_hcenter(0, window_height / 2 - (int)size.Y - 120, window_width, 48, a.GetFont(FontType.Score), new Color(255, 255, 255, 255), string.Format(Resources.Resources.YourScore, Model.score));
            }
         
            {
                var size = a.GetFont(FontType.Score).MeasureString(Resources.Resources.PlayAgain);
                c.draw_text_hcenter(0, window_height / 2 - (int)size.Y - 40, window_width, 48, a.GetFont(FontType.Score), new Color(255, 255, 555, 255), Resources.Resources.PlayAgain);
            }
        }

        void exit_game()
        {
            _done = true;
        }

        int count = 0;

        public void on_click(int px, int py)
        {

            // console::get().set_position(0, 5);
            // out_cout(count << ": " << e.to_string() << "            ");

            count++;

            //if (_restart.is_point_inside(e.X, e.Y))
            //{
            //    Model.restart();
            //    return;
            //}

            //if (Exit.is_point_inside(e.X, e.Y))
            //{
            //    Exit.DoClick(e.X, e.Y);                
            //    return;
            //}

            //if (_setting.is_point_inside(e.X, e.Y))
            //{
            //    _setting.DoClick(e.X, e.Y);
            //    return;
            //}


            if (Model.state == FieldModel.state_t.wait_yes_no)
            {
                if (_yes_button.IsPointInside(px, py))
                {
                    Model.restart();
                }

                if (_no_button.IsPointInside(px, py))
                {
                    _no_button.DoClick(px, py);
                }
            }
            else
            {
                int x = px - x_off;
                int y = py - y_off;

                int cx = x / LinesGame.cell_size;
                int cy = y / LinesGame.cell_size;

                if (cx >= 0 && cx < LinesGame.field_width && cy >= 0 && cy < LinesGame.field_height)
                {
                    Model.SendCommand(new FieldCommand { C = new FieldPoint { X = cx, Y = cy } });                    
                }
            }
        }        

        public void on_resize(int width, int height)
        {
            if (window_width == width && window_height == height)
                return;

            window_width = width;
            window_height = height;
            var fw = LinesGame.field_height * LinesGame.cell_size;
            var fh = LinesGame.field_height * LinesGame.cell_size;
            x_off = (window_width - fw) / 2;

            if (LinesGame.horizontal_view)
            {
                y_off = 0;
                var size = x_off / 3;
                //_setting.set_rect(0, window_height - size, size, size);
                //_restart.set_rect(size, window_height - size, size, size);
                //Exit.set_rect(2 * size, window_height - size, size, size);
            }
            else
            {
                y_off = LinesGame.top_offset;
                var h = window_height - LinesGame.top_offset - LinesGame.field_size;
                var size = Math.Min(h, window_width / 3);
                var offset = window_width / 3 - size;

                //_setting.set_rect(offset / 2, window_height - size, size, size);
                //_restart.set_rect(offset / 2 + window_width / 3, window_height - size, size, size);
                //Exit.set_rect(offset / 2 + 2 * window_width / 3, window_height - size, size, size);
            }

            need_draw = true;

            
            //_restart.set_text("Restart");
            //Exit.set_text("Exit");
        }


        public void on_mouse(int X, int Y)
        {
            switch (Model.state)
            {
                case FieldModel.state_t.ball_selected:
                case FieldModel.state_t.idle:
                    {
                        
                    }
                    break;
                case FieldModel.state_t.wait_yes_no:
                    {
                        if (_yes_button.IsPointInside(X, Y))
                        {
                            _yes_button.set_text_color(new Color(255, 255, 255, 255));
                        }
                        else
                        {
                            _yes_button.set_text_color(new Color(0, 0, 0, 255));
                        }

                        if (_no_button.IsPointInside(X, Y))
                        {
                            _no_button.set_text_color(new Color(255, 255, 255, 255));
                        }
                        else
                        {
                            _no_button.set_text_color(new Color(0, 0, 0, 255));
                        }
                    }
                    break;
                default:
                    break;

            }
        }
                  

        bool done()
        {
            return _done;

        }
        

        int window_width = 0;
        int window_height = 0;
        int x_off = 0;
        int y_off = 0;

        CellView[] _field;
        public TextControl _yes_button { get; } = new TextControl();
        public TextControl _no_button { get; } = new TextControl();        

        bool _done = false;        
        private bool need_draw_next = true;
        private bool need_draw = true;
        private bool need_draw_score = true;                
    }
}
