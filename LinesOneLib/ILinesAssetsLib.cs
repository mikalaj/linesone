﻿using MaSoft.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace MaSoft.Lines.Client
{
    public interface ILinesAssetsLib : IAssetsLib
    {
        Texture2D get_ball(BallType ball);
        Texture2D get_small_ball(BallType preview_ball);
        SpriteFont GetFont(FontType type);
        void GenerateBalls(IRenderLib render);
        SoundEffect GetSound(SoundEffectType type);
        Texture2D GetGearIcon();
        Texture2D GetExitIcon();
        Texture2D GetRestartIcon();
        Texture2D GetCupIcon();
        Texture2D GetBackIcon();
    }
}