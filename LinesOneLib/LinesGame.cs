﻿using System;
using System.IO;

namespace MaSoft.Lines.Client
{
    public class LinesGame
    {
        public static int top_offset = 100;
        public static int field_size { get; set; } = 600;
        public static int field_width { get; set; } = 9;
        public static int field_height { get; set; } = 9;
        public static int ball_size { get => cell_size - 2 * margin; }
        public static int margin { get; set; } = 8;
        public static int cell_size { get => field_size / field_width; }
        public static int min_clean { get; set; } = 5;
        public static long bounce_delay { get; } = TimeSpan.TicksPerSecond / margin / 10;
        public static long move_delay { get; } = (int)(0.06 * (double)TimeSpan.TicksPerSecond);
        public static bool horizontal_view { get; set; } = false;
        public static string AppName { get; } = "Lines One";
        public static string Company { get; } = "MaSoft";

        public static string GetTableRecordFile()
        {
            var dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MaSoft", "Lines");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            return Path.Combine(dir, "score.dat");
        }

        public static string GetLastGame()
        {
            var dir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MaSoft", "Lines");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            return Path.Combine(dir, "last_game.dat");
        }
    }
}
