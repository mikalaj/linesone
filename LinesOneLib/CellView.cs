﻿using MaSoft.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace MaSoft.Lines.Client
{
    
    public class CellView
    {
        public CellModel Model { get; private set; }

        public CellView(CellModel model)
        {
            Model = model;

            Model.OnBallChanged += Model_OnBallChanged;
            Model.OnPreviewBallChanged += Model_OnPreviewBallChanged;
        }

        private void Model_OnPreviewBallChanged(object sender, EventArgs args)
        {
            need_draw = true;
        }

        private void Model_OnBallChanged(object sender, EventArgs args)
        {
            need_draw = true;
        }

        public bool draw(int x, int y, IRenderLib c, ILinesAssetsLib a, bool force, TimeSpan dt)
        {
            bool flag = need_draw || force;
            
            if (!need_draw && !force && !Model.selected)
                return flag;

            var cell_size = LinesGame.cell_size;
            var margin = LinesGame.margin;
            var bounce_delay = LinesGame.bounce_delay;            

            //for (int i = 0; i < cell_size; ++i)
            //{
            //    c.DrawLine(x, y + i, x + cell_size, y + i, new Color(125, 125, 125, 255));
            //}
            
            c.DrawQuad(x, y, cell_size, cell_size, Color.DarkGray);
            c.DrawQuad(x + 2, y + 2, cell_size - 4, cell_size - 4, Color.Gray);

            //c.DrawLine(x, y, x + cell_size, y, new Color(125, 125, 125, 255));
            //c.DrawLine(x + cell_size, y, x + cell_size, y + cell_size, new Color(125, 125, 125, 255));
            //c.DrawLine(x + cell_size, y + cell_size, x, y + cell_size, Color.Black /*new Color(125, 125, 125, 255)*/);
            //c.DrawLine(x, y + cell_size, x, y, new Color(125, 125, 125, 255));

            if (Model.Ball != BallType.no)
            {
                if (Model.selected)
                {
                    Model._delay -= dt.Ticks;
                    if (Model._delay < 0)
                    {
                        Model.dy += Model.dir;
                        if (Model.dy > margin - 2 || Model.dy < -(margin - 2))
                        {
                            Model.dir = -Model.dir;
                            if (Model.dir > 0)
                            {
                                a.GetSound(SoundEffectType.Move).Play();
                            }
                        }
                        Model._delay = bounce_delay;
                    }
                }
                else
                {
                    Model.dy = 0;
                }

                var tile = a.get_ball(Model.Ball);

                var scale = (float)LinesGame.ball_size / (float)tile.Width;
                c.draw_tile(x + margin, y + margin + Model.dy, scale, tile);
            }
            else if (Model.Preview_ball != BallType.no)
            {
                var tile = a.get_small_ball(Model.Preview_ball);
                c.draw_tile(x + LinesGame.cell_size / 2 - (int)(LinesGame.ball_size / 2 * 0.3f), y + LinesGame.cell_size / 2 - (int)(LinesGame.ball_size / 2 * 0.3f), 0.3f, tile);
            }

            if (Model.selected)
            {
                need_draw = true;
            }
            else
            {
                need_draw = false;
            }

            return flag;
        }

        public void select(bool flag)
        {
            Model.selected = flag;
            need_draw = true;
            Model._delay = LinesGame.bounce_delay;
        }        
        
        private bool need_draw = true;
    }
}