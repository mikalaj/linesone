﻿using System;

namespace MaSoft.Lines.Client
{
    public delegate void OnBallChangedDelegate(object sender, EventArgs args);
    public delegate void OnPreviewBallChangedDelegate(object sender, EventArgs args);
    public class CellModel
    {
        public event OnBallChangedDelegate OnBallChanged;
        public event OnPreviewBallChangedDelegate OnPreviewBallChanged;        

        public int Id { get; set; }
        public FieldPoint c { get; set; }
        public int dir { get; set; } = 1;
        public int dy { get; set; } = 0;
        public int search_rev { get; set; } = 0;
        public int search_step { get; set; } = 0;
        // bool need_draw =true ;
        public bool selected { get; set; } = false;

        private BallType _ball = BallType.no;

        public BallType Ball
        {
            get => _ball;
            set
            {
                if (_ball == value)
                    return;
                _ball = value;
                OnBallChanged?.Invoke(this, new EventArgs());
            }
        }

        private BallType _preview_ball = BallType.no;

        public BallType Preview_ball
        {
            get => _preview_ball;
            set
            {
                if (_preview_ball == value)
                    return;
                _preview_ball = value;
                OnPreviewBallChanged?.Invoke(this, new EventArgs());
            }

        }
        public long _delay { get; set; } = LinesGame.bounce_delay;

        public void Update(TimeSpan dt)
        {

        }

        public void select(bool flag)
        {
            selected = flag;
            _delay = LinesGame.bounce_delay;
        }        

        public void Reset()
        {
            search_rev = 0;
            search_step = 0;
            selected = false;
            _ball = BallType.no;
            _preview_ball = BallType.no;
            dir = 1;
            dy = 0;
        }
    }
}