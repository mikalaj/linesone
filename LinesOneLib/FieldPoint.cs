﻿namespace MaSoft.Lines.Client
{
    public struct FieldPoint
    { 
        public int X { get; set; }
        public int Y { get; set; }        
    }
}
