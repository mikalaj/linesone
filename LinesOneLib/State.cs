﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MaSoft.Lines.Client
{
    public abstract class State<TGame>
    {
        public abstract void Init(TGame app);
        public abstract void Update(TGame app, GameTime gameTime);
        public abstract void Draw(TGame app, GameTime gameTime);
        public abstract void OnMouseMove(TGame app, MouseState state, int dx, int dy);

        public abstract void OnMouseLeftButtonDown(TGame app, MouseState state);
        public abstract void OnMouseLeftButtonUp(TGame app, MouseState state);
        public abstract void OnMouseLeftButtonClick(TGame app, MouseState state);
        public abstract void OnResize(TGame app, int width, int height);
        public abstract void OnExit(TGame app);
        public abstract void OnLeave(TGame app);
        public abstract void OnEnter(TGame app);
    }
}
