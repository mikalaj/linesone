﻿using System.IO;

namespace MaSoft.Lines.Client
{
    public class ScoreTable
    {
        public int BestScore { get; set; }
        public string BestPlayer { get; set; } = "Unknown";

        public void Save()
        {
            var text = System.Text.Json.JsonSerializer.Serialize(this, new System.Text.Json.JsonSerializerOptions { WriteIndented = true });
            File.WriteAllText(LinesGame.GetTableRecordFile(), text);
        }

        public static ScoreTable Load()
        {
            var file = LinesGame.GetTableRecordFile();
            if (!File.Exists(file))
                return new ScoreTable();

            try
            {
                var text = File.ReadAllText(file);
                return System.Text.Json.JsonSerializer.Deserialize<ScoreTable>(text);
            }
            catch
            {
                return new ScoreTable();
            }
        }
    }
}
