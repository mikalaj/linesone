﻿namespace MaSoft.Lines.Client
{
    public class BallCell
    {
        public BallType Key { get; set; } = BallType.no;
        public FieldPoint Value { get; set; }
    }
}
