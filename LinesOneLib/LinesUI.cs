﻿using MaSoft.Framework.Graphics.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NLog;
using System;

namespace MaSoft.Lines.Client
{
    public class LinesUI : UiImpl<Game1>
    {
        private static Logger Log = LogManager.GetCurrentClassLogger();
        public IconControl Exit { get; }
        public IconControl Restart { get; }
        public IconControl Settings { get; }
        public IconControl ScoreTable { get; }
        public IconControl Back { get; }
        public TextInput UserNameInput { get; }
        public TextControl UserNameLabel { get; }

        public LinesUI()
        {
            AddControl(Exit = new IconControl());
            AddControl(Restart = new IconControl());
            AddControl(Settings = new IconControl());
            AddControl(ScoreTable = new IconControl());
            AddControl(Back = new IconControl());
            AddControl(UserNameInput = new TextInput());
            AddControl(UserNameLabel = new TextControl());
        }

        public void _model_OnRestart(object sender, EventArgs args)
        {
            Invalidate();
        }

        protected override void ShowKeyboard(Game1 app)
        {
            app.ShowKeyboard();
        }

        protected override void HideKeyboard(Game1 app)
        {
            app.HideKeyboard();
        }

        protected override void OnInit(Game1 app)
        {
            base.OnInit(app);

            IsMobile = app.IsMobile;
            UserNameInput.Font = app.Assets.GetFont(FontType.Score);
            UserNameInput.Resize(100, 100, 200, 64);
            UserNameInput.Text = string.Empty;
            UserNameInput.Visible = false;
            UserNameInput.Title = Resources.Resources.PlayerNameLabel;
            UserNameInput.OnTextEntered += (o) =>
            {
                try
                {
                    app.Program.Config.Set("Username", app.Username = UserNameInput.Text);
                    app.Program.Config.Save();
                }
                catch (Exception e)
                {
                    Log.Error("Failed to update config file");
                }
            };

            UserNameLabel.Font = app.Assets.GetFont(FontType.Score);
            UserNameLabel.Resize(100, 100, 200, 64);
            UserNameLabel.Text = Resources.Resources.PlayerNameLabel;
            UserNameLabel.Visible = false;
            UserNameLabel.set_text_color(Color.Black);

            Exit.SetIcon(app.Assets.GetExitIcon());
            Restart.SetIcon(app.Assets.GetRestartIcon());
            Settings.SetIcon(app.Assets.GetGearIcon());
            ScoreTable.SetIcon(app.Assets.GetCupIcon());
            Back.SetIcon(app.Assets.GetBackIcon());
        }

        protected override void OnResize(Game1 app, int width, int height)
        {
            base.OnResize(app, width, height);

            var minSize = Math.Min(app.Window.ClientBounds.Width, app.Window.ClientBounds.Height);
            LinesGame.field_size = Math.Min(app.Window.ClientBounds.Width, app.Window.ClientBounds.Height);
            LinesGame.horizontal_view = app.Window.ClientBounds.Width > app.Window.ClientBounds.Height;

            var window_width = width;
            var window_height = height;
            var fw = LinesGame.field_height * LinesGame.cell_size;
            var fh = LinesGame.field_height * LinesGame.cell_size;
            var x_off = (window_width - fw) / 2;

            if (LinesGame.horizontal_view)
            {
                var y_off = 0;
                var size = x_off / 4;
                Settings.Resize(0, window_height - size, size, size);
                ScoreTable.Resize(size, window_height - size, size, size);
                Restart.Resize(2 * size, window_height - size, size, size);
                Exit.Resize(3 * size, window_height - size, size, size);
                Back.Resize(0, window_height - size, size * 4, size);
                UserNameInput.Resize(window_width / 2, 200, window_width / 2, 64);
                UserNameLabel.Resize(10, 200, window_width / 2 - 10, 64);
            }
            else
            {
                var y_off = LinesGame.top_offset;
                var h = window_height - LinesGame.top_offset - LinesGame.field_size;
                var size = Math.Min(h, window_width / 4);
                var offset = window_width / 4 - size;

                Settings.Resize(offset / 2, window_height - size, size, size);
                ScoreTable.Resize(offset / 2 + window_width / 4, window_height - size, size, size);
                Restart.Resize(offset / 2 + 2 * window_width / 4, window_height - size, size, size);
                Exit.Resize(offset / 2 + 3 * window_width / 4, window_height - size, size, size);
                Back.Resize(0, window_height - size, window_width / 4, size);                
                UserNameInput.Resize(window_width / 2, 200, window_width / 2, 64);
                UserNameLabel.Resize(10, 200, window_width / 2 - 10, 64);
            }

            // need_draw = true;


            // _restart.set_text("Restart");
            // Exit.set_text("Exit");
        }        
    }
}
