﻿//using MaSoft.Framework;
//using MaSoft.Framework.Score.Clients;
//using MaSoft.Lines.Service;
//using NLog;
//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;

//namespace MaSoft.Lines.Client
//{
//    public class Command
//    {
//    }

//    public class CallbackCommand<T> : Command
//    {
//        public Action<T> OnDone { get; set; }
//    }


//    public class SendScore : Command
//    {
//        public string Name { get; set; }
//        public int Score { get; set; }
//    }

//    public class Record
//    {
//        public string Id { get; set; }
//        public string Name { get; set; }
//        public int Score { get; set; }
//    }

//    public class GetTopScores : CallbackCommand<List<Record>>
//    {
//        public int Count { get; set; }
//    }

//    public class Client : Program
//    {
//        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
//        private IScoreClient _scoreService;
//        private string _address;
//        private CancellationTokenSource _ct = new CancellationTokenSource();

//        BlockingCollection<Command> _command = new BlockingCollection<Command>();

//        public Client(string address)
//            : base(LinesGame.Company, LinesGame.AppName)
//        {
//            _address = address;
//            Current = this;
//        }

//        public void Stop()
//        {
//            _ct.Cancel();
//        }

//        public void GetTop(int count, Action<List<Record>> onDone)
//        {
//            _command.Add(new GetTopScores { Count = count, OnDone = onDone });
//        }

//        public async Task<List<Record>> GetTop(int count)
//        {
//            return await Task.Run(() =>
//            {
//                List<Record> r = new List<Record>();

//                using (var evt = new ManualResetEvent(false))
//                {
//                    GetTop(count, (result) =>
//                    {
//                        r = result;
//                        evt.Set();
//                    });

//                    evt.WaitOne();
//                }

//                return r;
//            });
//        }

//        public async Task DeleteRecordAsync(string id)
//        {
//            try
//            {
//                await _scoreService.DeleteRecordAsync(id);
//            }
//            catch (Exception e)
//            {
//                Log.Error($"Delete score failed. {e.Message}");
//            }
//        }

//        public async Task AddNewScoreAsync(string name, int score)
//        {
//            try
//            {
//                await _scoreService.AddNewScoreAsync(name, score);
//            }
//            catch (Exception e)
//            {
//                Log.Error($"Add new score failed. {e.Message}");
//            }
//        }

//        protected override async Task OnRunAsync(string[] args)
//        {
//            _scoreService = MaSoft.Framework.Score.Module.CreateScoreClient();
//            await _scoreService.SetConnectionInfoAsync("https://linesone.ma-shadow.tech", "https://identity.ma-shadow.tech", Identity)
//            var cancelToken = _ct.Token;

//            try
//            {
//                while (!cancelToken.IsCancellationRequested)
//                {
//                    try
//                    {                        
//                        while (!cancelToken.IsCancellationRequested)
//                        {
//                            var cmd = _command.Take(cancelToken);

//                            if (cmd == null)
//                                break;

//                            switch (cmd)
//                            {
//                                case SendScore sendScore:
//                                    await _scoreService.Client.SendScoreAsync(new Protocol.ProtoLinesOneScoreRequest
//                                    {
//                                        GameId = "Lines One",
//                                        Score = new Protocol.ProtoLinesScore
//                                        {                                            
//                                            Name = sendScore.Name,
//                                            Score = sendScore.Score
//                                        }
//                                    });
//                                    break;

//                                case GetTopScores getTopScores:
//                                    var scores = await _scoreService.Client.GetTopScoresAsync(new Protocol.ProtoLinesOneGetTopScoreRequest
//                                    {
//                                        Count = getTopScores.Count
//                                    });

//                                    getTopScores.OnDone(scores.Scores.Select(o => new Record
//                                    {
//                                        Id = o.Id,
//                                        Name = o.Name,
//                                        Score = o.Score
//                                    }).ToList());

//                                    break;
//                                default:
//                                    break;
//                            }
//                        }
//                    }
//                    catch (Exception e)
//                    {
//                        await _tokenService.ShutdownAsync();                        
//                        Console.WriteLine($"Error {e.Message}");
//                        cancelToken.WaitHandle.WaitOne(1000);
//                    }
//                }
//            }
//            finally
//            {

//                await _tokenService.ShutdownAsync();
//                await _scoreService.ShutdownAsync();
//            }
//        }
//    }
//}
