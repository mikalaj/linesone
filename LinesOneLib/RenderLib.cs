﻿using MaSoft.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace MaSoft.Lines.Client
{
    public class RenderLib : IRenderLib
    {
        Game App { get; set; }

        public int Width { get => _windowWidth; }
        public int Height { get => _windowHeight; }

        MaSoft.Framework.Graphics.Geometry.SpherePrimitive _sphere;

        public RenderLib(Game g)
        {
            App = g;

            _sphere = new MaSoft.Framework.Graphics.Geometry.SpherePrimitive(App.GraphicsDevice, 1, 16);

            _basicEffect = new BasicEffect(App.GraphicsDevice);
            _basicEffect.VertexColorEnabled = true;
            _basicEffect.Projection = Matrix.CreateOrthographicOffCenter
            (0,
            App.GraphicsDevice.Viewport.Width,     // left, right
            App.GraphicsDevice.Viewport.Height, 0,    // bottom, top
            0, 1);

            _spriteBatch = new SpriteBatch(App.GraphicsDevice);

            Resize(g.GraphicsDevice.PresentationParameters.BackBufferWidth, g.GraphicsDevice.PresentationParameters.BackBufferHeight);
        }

        public event OnResize OnResize;
        public event OnMouseLeftButtonDown OnMouseLeftButtonDown;
        public event OnMouseMove OnMouseMove;
        public event OnMouseLeftButtonUp OnMouseLeftButtonUp;
        public event OnMouseLeftClick OnMouseLeftClick;
        public event OnKeyDown OnKeyDown;
        public event OnKeyUp OnKeyUp;


        public void DrawLine(int x1, int y1, int x2, int y2, Color color)
        {
            foreach (var pass in _basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                var vertices = new VertexPositionColor[2];
                vertices[0].Position = new Vector3(x1, y1, 0);
                vertices[0].Color = color;
                vertices[1].Position = new Vector3(x2, y2, 0);
                vertices[1].Color = color;                
                App.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, 1);
            }
        }

        public void DrawLine(int x1, int y1, int x2, int y2, int size, Color color)
        {
            float fsize = (float)size;
            var p1 = new Vector3(x1, y1, 0);
            var p2 = new Vector3(x2, y2, 0);
            var d = p2 - p1;
            d.Normalize();
            var n = new Vector3(d.Y, -d.X, 0);            

            foreach (var pass in _basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();

                var vertices = new VertexPositionColor[4];
                vertices[0].Position = p1 - n * fsize * 0.5f;
                vertices[0].Color = color;
                vertices[1].Position = p1 + n * fsize * 0.5f;
                vertices[1].Color = color;
                vertices[2].Position = p2 - n * fsize * 0.5f;
                vertices[2].Color = color;
                vertices[3].Position = p2 + n * fsize * 0.5f;
                vertices[3].Color = color;                
                App.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleStrip, vertices, 0, 2);
            }
        }

        public void DrawQuad(Rectangle rect, Color color)
        {
            DrawQuad(rect.X, rect.Y, rect.Width, rect.Height, color);
        }

        public void DrawQuad(int x, int y, int width, int height, Color color)
        {
            _basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new VertexPositionColor[4];
            vertices[0].Position = new Vector3(x, y, 0);
            vertices[0].Color = color;
            vertices[1].Position = new Vector3(x + width, y, 0);
            vertices[1].Color = color;
            vertices[2].Position = new Vector3(x, y + height, 0);
            vertices[2].Color = color;
            vertices[3].Position = new Vector3(x + width, y + height, 0);
            vertices[3].Color = color;

            App.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleStrip, vertices, 0, 2);
        }

        public void DrawRectangle(Rectangle rect, Color color)
        {
            DrawRectangle(rect.X, rect.Y, rect.Width, rect.Height, color);
        }

        public void DrawRectangle(int x, int y, int width, int height, Color color)
        {
            _basicEffect.CurrentTechnique.Passes[0].Apply();
            var vertices = new VertexPositionColor[5];
            vertices[0].Position = new Vector3(x, y, 0);
            vertices[0].Color = color;
            vertices[1].Position = new Vector3(x + width, y, 0);
            vertices[1].Color = color;
            vertices[2].Position = new Vector3(x + width, y + height, 0);
            vertices[2].Color = color;
            vertices[3].Position = new Vector3(x, y + height, 0);
            vertices[3].Color = color;
            vertices[4].Position = new Vector3(x, y, 0);
            vertices[4].Color = color;

            App.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, vertices, 0, 4);
        }

        public void DrawText(float v1, float v2, int font_size, SpriteFont spriteFont, Color color, string text)
        {
            // Finds the center of the string in coordinates inside the text rectangle
            Vector2 textMiddlePoint = spriteFont.MeasureString(text) / 2;
            _spriteBatch.DrawString(spriteFont, text, new Vector2 { X = v1, Y = v2 }, color, 0, textMiddlePoint, 1.0f, SpriteEffects.None, 0.5f);
        }

        public void DrawTextLeft(float v1, float v2, int font_size, SpriteFont spriteFont, Color color, string text)
        {
            // Finds the center of the string in coordinates inside the text rectangle
            // Vector2 textMiddlePoint = spriteFont.MeasureString(text) / 2;
            _spriteBatch.DrawString(spriteFont, text, new Vector2 { X = v1, Y = v2 }, color, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.5f);
        }

        public void draw_text_hcenter(int x, int y, int width, int size, SpriteFont spriteFont, Color color, string text_score)
        {
            Vector2 text_size = spriteFont.MeasureString(text_score);
            int off_x = x + width / 2 /*- (int)text_size.X / 2*/;
            DrawText(off_x, y, size, spriteFont, color, text_score);
        }

        public void draw_tile(int x, int y, Texture2D tile)
        {
            _spriteBatch.Draw(tile,
                             new Vector2 { X = x, Y = y },
                             null,
                             Color.White,
                             0f,
                             new Vector2(0, 0),
                             Vector2.One,
                             SpriteEffects.None,
                             0f);
        }

        public void draw_tile(int x, int y, float scale, Texture2D tile)
        {
            var newWidth = tile.Width * scale;
            var newHeight = tile.Height * scale;

            var diff = tile.Width - newWidth;

            _spriteBatch.Draw(tile,
                             // new Vector2(x - tile.Width / 2 + tile.Width * scale, y - tile.Height / 2 ),
                             new Vector2(x, y),
                             null,
                             Color.White,
                             0f,
                             Vector2.Zero,
                             scale,
                             SpriteEffects.None,
                             0f);
        }

        public void draw_tile(int x, int y, float scalex, float scaley, Texture2D tile)
        {

            _spriteBatch.Draw(tile,
                             new Vector2(x, y),
                             null,
                             Color.White,
                             0f,
                             Vector2.Zero,
                             new Vector2(scalex, scaley),
                             SpriteEffects.None,
                             0f);
        }

        public void Resize(int width, int height)
        {
            if (_windowWidth == width && _windowHeight == height)
                return;

            if (_renderTarget != null)
                _renderTarget.Dispose();

            _renderTarget = new RenderTarget2D(App.GraphicsDevice, width, height, false,
                App.GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24, 0, RenderTargetUsage.PreserveContents);

            _windowWidth = width;
            _windowHeight = height;

            _basicEffect.Projection = Matrix.CreateOrthographicOffCenter
            (0,
            App.GraphicsDevice.Viewport.Width,     // left, right
            App.GraphicsDevice.Viewport.Height, 0,    // bottom, top
            0, 1);

            OnResize?.Invoke(width, height);
        }

        public void DrawSphere(Matrix world, Matrix view, Matrix proj, Color color)
        {
            _sphere.Draw(world, view, proj, color);
        }

        public void SwapBuffer()
        {
            _spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.Default, RasterizerState.CullNone);
            _spriteBatch.Draw(_renderTarget, new Rectangle(0, 0, _windowWidth, _windowHeight), Color.White);
            _spriteBatch.End();
        }

        public void Clear()
        {
            Device.Clear(new Color(200, 200, 200, 255));
        }        

        public SpriteBatch Sprite => _spriteBatch;

        public RenderTarget2D BackBuffer => _renderTarget;

        public GraphicsDevice Device => App.GraphicsDevice;

        private BasicEffect _basicEffect;
        private SpriteBatch _spriteBatch;

        private int _windowWidth;
        private int _windowHeight;

        RenderTarget2D _renderTarget;
    }
}
