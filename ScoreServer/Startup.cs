﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using MaSoft.Framework.Certificates;
using MaSoft.Framework.Score;

namespace ScoreServer
{
    public class Startup
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    Log.Info($"Server certificate {ScoreServerApp.Current.ServerCertificate.ExportToPEM()}");
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = ScoreServerApp.Current.Config.Get("IdentityIssuer", "MaSoft"),
                        ValidateAudience = true,
                        ValidAudience = ScoreServerApp.Current.Config.Get("IdentityAudience", "IdentityIssuer"),
                        ValidateLifetime = true,
                        IssuerSigningKey = new X509SecurityKey(ScoreServerApp.Current.ServerCertificate, SecurityAlgorithms.RsaSha512Signature),
                        ValidateIssuerSigningKey = true,
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = OnAuthenticationFailed,
                        OnForbidden = OnForbidden,
                        OnMessageReceived = OnMessageReceived,
                        OnTokenValidated = OnTokenValidated,
                        OnChallenge = OnChallenge,
                    };
                });
            services.AddControllersWithViews();
            services.AddGrpc();
            services.AddSingleton<IScoreTable, MemoryScoreTable>();
        }

        private Task OnChallenge(JwtBearerChallengeContext arg)
        {
            Log.Info($"OnChallenge");
            return Task.CompletedTask;
        }

        private Task OnTokenValidated(TokenValidatedContext arg)
        {
            Log.Info($"OnTokenValidated {arg.Principal}");
            return Task.CompletedTask;
        }

        private Task OnMessageReceived(MessageReceivedContext arg)
        {
            Log.Info($"OnMessageReceived");
            return Task.CompletedTask;
        }

        private Task OnForbidden(ForbiddenContext arg)
        {
            Log.Info($"OnForbidden");
            return Task.CompletedTask;
        }

        private Task OnAuthenticationFailed(AuthenticationFailedContext arg)
        {
            Log.Info($"OnAuthenticationFailed");
            return Task.CompletedTask;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                MaSoft.Framework.Score.Module.RegisterScoreServer(endpoints);

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
